# Makefile for compilation of code

# C compiler
CC = gcc
# Source files location
SRC_DIR = src
# Source files
SRC_FILES = inout.c geometry.c misc.c common.c jst.c uw.c jst_solvers.c uw_solvers.c main.c
# Compiler flags (do not link)
CFLAGS = -c -Wall -O3 -fopenmp
# Linker flags (-lm needs to be used in case of using math.h)
LDFLAGS = -lm -Wall -O3 -fopenmp
# Get full path of source files
SRC = $(addprefix $(SRC_DIR)/,$(SRC_FILES))
# Get objects names
OBJ = $(SRC:.c=.o)
# Executable name
EXE = SpacetimeCFD

# Actual compilation

all: $(EXE) clean

$(EXE): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) -o $@ $(CFLAGS) $<

.PHONY: clean
clean: $(OBJ)
	rm $^
