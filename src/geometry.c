/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "geometry.h"

/**
 * Normalizes vector so that module = 1.0
 */
static void normalize_vec(
  int numberOfElements,
  double *x
  )
{
  int i;
  double mod;

  mod = 0.0;
  for (i = 0; i < numberOfElements; i++){
    mod += pow(x[i], 2.0);
  }
  mod = sqrt(mod);
  for (i = 0; i < numberOfElements; i++){
    x[i] /= mod;
  }
}

/**
 * Calculates the vectorial product of x and y.
 */
static void vecprod_3d(
  double x[3],
  double y[3],
  double z[3]
  )
{
  z[0] = x[1] * y[2] - x[2] * y[1];
  z[1] = x[2] * y[0] - x[0] * y[2];
  z[2] = x[0] * y[1] - x[1] * y[0];
}


/**
 * Gets rotation matrix for plane in 3D.
 */
static double **get_plane_rotation_matrix(
  double x1,
  double x2,
  double x3,
  double y1,
  double y2,
  double y3,
  double z1,
  double z2,
  double z3,
  double *normal
  )
{
  int i;
  double u[3], v[3], **matrix;

  /* work out in-plane base (u,v) */
  u[0] = x2 - x1;
  u[1] = y2 - y1;
  u[2] = z2 - z1;
  v[0] = x3 - x1;
  v[1] = y3 - y1;
  v[2] = z3 - z1;

  vecprod_3d(u, v, normal);
  vecprod_3d(normal, u, v);
  normalize_vec(3, u);
  normalize_vec(3, v);
  normalize_vec(3, normal);

  /* create rotation matrix */
  matrix = (double**)malloc(sizeof(double*)*2);
  matrix[0] = (double*)malloc(sizeof(double)*3);
  matrix[1] = (double*)malloc(sizeof(double)*3);
  for (i = 0; i < 3; i++){
    matrix[0][i] = u[i];
    matrix[1][i] = v[i];
  }

  return matrix;
}


/**
 * Projects point in 3D into a 2D space.
 */
static void project_coordinate_in_plane(
  double x0,
  double y0,
  double z0,
  double x,
  double y,
  double z,
  double **matrix,
  double *x_proj,
  double *y_proj
  )
{
  int j;
  double u[3];

  u[0] = x - x0;
  u[1] = y - y0;
  u[2] = z - z0;
  (*x_proj) = (*y_proj) = 0.0;

  for (j = 0; j < 3; j++){
    (*x_proj) += matrix[0][j]*u[j];
    (*y_proj) += matrix[1][j]*u[j];
  }
}


/**
 * Repair the heap whose root element is at index 'start', assuming the heaps
 * rooted at its children are valid.
 */
static void shift_down(
  double *arr,
  int *ind,
  const int start,
  const int end
  )
{
  int root, child, itemp;
  double dtemp;
  int itemp2;

  root = start;
  while (2 * root + 1 <= end){   /* While the root has at least one child */
    child = 2 * root + 1;        /* Left child of root */
    itemp = root;                /* Keeps track of child to swap with */

    if (arr[itemp] < arr[child]){
      itemp = child;
    }
    /* If there is a right child and that child is greater */
    if (child + 1 <= end && arr[itemp] < arr[child + 1]){
      itemp = child + 1;
    }
    if (itemp == root){
      /* The root holds the largest element. Since we assume the heaps rooted at the
      children are valid, this means that we are done */
      break;
    }
    else {
      dtemp = arr[itemp];
      arr[itemp] = arr[root];
      arr[root] = dtemp;
      itemp2 = ind[itemp];
      ind[itemp] = ind[root];
      ind[root] = itemp2;
      root = itemp;   /* repeat to continue shifting down the child now */
    }
  }
}


/**
 * Put elements of 'arr' in heap order, in-place.
 */
static void heapify(
  double *arr,
  int *ind,
  const int count
  )
{
  int start;

  /* 'start' is assigned the index in 'arr' of the last parent node */
  /* The last element in a 0-based array is at index count-1; find the parent of that element */
  start = (int)floor((count - 2) / 2);
  while (start >= 0){
    /* shift down the node at index 'start' to the proper place such that all nodes below
    the start index are in heap order */
    shift_down(arr, ind, start, count - 1);
    /* go to the next parent node */
    start -= 1;
    /* after sifting down the root all nodes/elements are in heap order */
  }
}


/**
 * Sorts a set of numbers from lowest to highest
 */
static void heapsort(
  double *arr,
  int *ind,
  const int len
  )
{
  int end, itemp;
  double dtemp;

  /* Build the heap in array arr so that largest value is at the root */
  heapify(arr, ind, len);

  /* The following loop maintains the invariants that arr[0:end] is a heap and every element
  beyond end is greater than everything before it (so arr[end:count] is in sorted order) */
  end = len - 1;
  while (end > 0){
    /* arr[0] is the root and largest value. The following moves it in front of the sorted elements */
    dtemp = arr[end];
    arr[end] = arr[0];
    arr[0] = dtemp;
    itemp = ind[end];
    ind[end] = ind[0];
    ind[0] = itemp;
    /* the heap size is reduced by one */
    end = end - 1;
    /* Restore the heap property */
    shift_down(arr, ind, 0, end);
  }
}


/**
 * Sorts 2D points counter-clockwise. Returns a vector of new indices (based on
 * the Graham scan algorithm)
 */
static int *sort_counterClockwise(
  const int numberOfPoints,
  const double *x,
  const double *y
  )
{
  int j, n;
  double *arr;
  int *ind;

  /* allocate dynamic memory */
  arr = (double*)malloc(sizeof(double)*numberOfPoints);
  ind = (int*)malloc(sizeof(int)*numberOfPoints);

  for (j = 0; j < numberOfPoints; j++){
    ind[j] = j;
  }

  /* find vertex with the smallest y-coordinate; in case of equal y, find the smallest x-coordinate */
  n = 0;
  for (j = 1; j < numberOfPoints; j++){
    if (y[n] > y[j]){
      n = j;
    }
    else if (fabs(y[n] - y[j]) < 1.0e-15){
      if (x[n] > x[j]){
        n = j;
      }
    }
  }

  /* sort vertices by increasing angle formed by the vector from the vertex to point n and the x-axis */
  for (j = 0; j < numberOfPoints; j++){
    if (j == n){
      arr[j] = -1.0;  // cos(180.0)
      continue;
    }
    else {
      arr[j] = (x[n] - x[j]) / sqrt(pow(x[n] - x[j], 2.0) + pow(y[n] - y[j], 2.0));
    }
  }

  /* sort arr[] from lowest to highest value; the elements of ind[] result after moving its elements the same as arr[] */
  /* in other words, after heapsort() new_arr[N] would be equal to arr[ind[N]]   */
  heapsort(arr, ind, numberOfPoints);

  free(arr);

  return ind;
}


/**
 * Works out face area.
 */
static double get_face_area(
  const int numberOfVertices,
  const double *x,
  const double *y
  )
{
  int i;
  double area = 0.0;

  for (i = 0; i < numberOfVertices - 1; i++){
    area += x[i] * y[i + 1] - x[i + 1] * y[i];
  }
  area += x[numberOfVertices - 1] * y[0] - x[0] * y[numberOfVertices - 1];
  area = fabs(area / 2.0);

  return area;
}


/**
 * Works out the scalar dot product of two vectors
 */
static double dot_product(
  double *vector_0,
  double *vector_1,
  int dim
  )
{
  int i;
  double answer;

  answer = 0.0;
  for (i = 0; i < dim; i++){
    answer += vector_0[i] * vector_1[i];
  }

  return answer;
}


/**
 * Determines whether a cell is left neighbour.
 */
static int is_left_cell_3d(
  double *cellCentre,
  double *faceCentre,
  double *normalVec
  )
{
  int i, answer;
  double auxVec[3];

  for (i = 0; i < 3; i++){
    auxVec[i] = cellCentre[i] - faceCentre[i];
  }

  if (dot_product(normalVec, auxVec, 3) > 0.0){
    answer = YES;
  }
  else {
    answer = NO;
  }

  return answer;
}



/**
 * Finds the other neighbour face, if exists
 */
static int find_other_face(
  const int currentFace,
  const int size0,
  const int size1,
  const int *faceAtNode0,
  const int *faceAtNode1
  )
{
  int i, j;
  int answer;

  /* remember that faceAtNode are in order from low to high value (by definition of how they are obtained) */
  answer = -1;
  for (i = 0; i < size0; i++){
    if (faceAtNode0[i] == currentFace){
      continue;
    }
    for (j = 0; j < size1; j++){
      if (faceAtNode0[i] == faceAtNode1[j]){
        answer = faceAtNode0[i];
        break;
      }
      else if (faceAtNode0[i] < faceAtNode1[j]){
        break;
      }
    }
    if (answer > -1){
      break;
    }
  }

  if (answer < 0){
    for (i = 0; i < size0; i++)
      printf(" %d,", faceAtNode0[i]);
    printf("\b\n");
    for (i = 0; i < size1; i++)
      printf(" %d,", faceAtNode1[i]);
    printf("\b\n");

    error("failed to find other neighbour face");
  }

  return answer;
}



/**
 * Get local edge index in face.
 */
static int find_edge_index_in_face(
  const int node0,
  const int node1,
  const int numberOfFaceVertices,
  const int *faceVert
  )
{
  int i;
  int answer;

  answer = -1;
  for (i = 0; i < numberOfFaceVertices - 1; i++){
    if ((faceVert[i] == node0 && faceVert[i + 1] == node1) || (faceVert[i] == node1 && faceVert[i + 1] == node0)){
      answer = i;
      break;
    }
  }
  if (answer < 0){
    if ((faceVert[numberOfFaceVertices - 1] == node0 && faceVert[0] == node1) || (faceVert[numberOfFaceVertices - 1] == node1 && faceVert[0] == node0)){
      answer = numberOfFaceVertices - 1;
    }
  }
  if (answer < 0)
    error("failed to retrieve local edge index in face");

  return answer;
}


/**
 * Calculates area, normalVector and faceCentre.
 *
 * @note It sorts face vertices counter-clockwise.
 */
void calculate_faces_3d(
  const int numberOfFaces,
  const int *numberOfFaceVertices,
  int **faceVert,
  double **normalVec,
  double ***faceCentre,
  double *area,
  const double *x,
  const double *y,
  const double *z
  )
{
  int i = 3, j, *ind, *vert;
  double *x0, *y0, **M;

  /* allocate heap memory */
  (*faceCentre) = (double**)malloc(sizeof(double*)*numberOfFaces);
  x0 = (double*)malloc(sizeof(double));
  y0 = (double*)malloc(sizeof(double));
  vert = (int*)malloc(sizeof(int));
  for (i = 0; i < numberOfFaces; i++){
    (*faceCentre)[i] = (double*)calloc(3, sizeof(double));
    x0 = (double*)realloc(x0,sizeof(double)*numberOfFaceVertices[i]);
    y0 = (double*)realloc(y0,sizeof(double)*numberOfFaceVertices[i]);
    vert = (int*)realloc(vert,sizeof(int)*numberOfFaceVertices[i]);
    M = (double**)get_plane_rotation_matrix(x[faceVert[i][0]], x[faceVert[i][1]], x[faceVert[i][2]],
        y[faceVert[i][0]], y[faceVert[i][1]], y[faceVert[i][2]], z[faceVert[i][0]], z[faceVert[i][1]],
        z[faceVert[i][2]], normalVec[i]);
    /* get faceCentre and project vertices */
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      (*faceCentre)[i][0] += x[faceVert[i][j]];
      (*faceCentre)[i][1] += y[faceVert[i][j]];
      (*faceCentre)[i][2] += z[faceVert[i][j]];
      project_coordinate_in_plane(x[faceVert[i][0]], y[faceVert[i][0]], z[faceVert[i][0]],
          x[faceVert[i][j]], y[faceVert[i][j]], z[faceVert[i][j]], M, &x0[j], &y0[j]);
      vert[j] = faceVert[i][j];
    }
    free(M[0]);
    free(M[1]);
    free(M);
    for (j = 0; j < 3; j++){
      (*faceCentre)[i][j] /= (double)numberOfFaceVertices[i];
    }
    /* sort face vertices counter-clockwise */
    ind = sort_counterClockwise(numberOfFaceVertices[i], x0, y0);
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      faceVert[i][j] = vert[ind[j]];
    }
    free(ind);
    area[i] = get_face_area(numberOfFaceVertices[i],x0,y0);
  }

  /* free heap memory */
  free(x0);
  free(y0);
  free(vert);
}


/**
 * Work out cell centres.
 */
double **get_cell_centres_3d(
  const face_map *indx_f,
  const int numberOfNodes,
  const int numberOfCells,
  const int *numberOfFaceVertices,
  int **neighbourCell,
  int **faceVert,
  const double *x,
  const double *y,
  const double *z
  )
{
  int i, j, k, numDims = 3, numberOfCellNodes, *nodeInCell, **cellFaces;
  double **cellCentre;

  /* alloc dyanmic memory */
  cellFaces = (int**)malloc(sizeof(int*)*numberOfCells);
  nodeInCell = (int*)calloc(numberOfNodes, sizeof(int));
  cellCentre = (double**)malloc(sizeof(double*)*numberOfCells);
  for (i = 0; i < numberOfCells; i++)
  {
    cellFaces[i] = (int*)calloc(5, sizeof(int));
    cellCentre[i] = (double*)calloc(numDims, sizeof(double));
  }

  /* get cellFaces per cell */
  for (i = indx_f->inner; i < indx_f->initial; i++){
    for (j = 0; j < 2; j++){
      if (cellFaces[neighbourCell[i][j]][0] > 3){
        cellFaces[neighbourCell[i][j]] = (int*)realloc(cellFaces[neighbourCell[i][j]], sizeof(int)*(cellFaces[neighbourCell[i][j]][0] + 2));
      }
      cellFaces[neighbourCell[i][j]][0]++;
      cellFaces[neighbourCell[i][j]][cellFaces[neighbourCell[i][j]][0]] = i;
    }
  }
  j = 0;
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    if (cellFaces[neighbourCell[i][j]][0] > 3){
      cellFaces[neighbourCell[i][j]] = (int*)realloc(cellFaces[neighbourCell[i][j]], sizeof(int)*(cellFaces[neighbourCell[i][j]][0] + 2));
    }
    cellFaces[neighbourCell[i][j]][0]++;
    cellFaces[neighbourCell[i][j]][cellFaces[neighbourCell[i][j]][0]] = i;
  }

  /* calculate cell centres: all elements in cellCentre[] and nodeInCell[] need to be zero */
  for (i = 0; i < numberOfCells; i++){
    numberOfCellNodes = 0;
    for (j = 1; j < cellFaces[i][0] + 1; j++){
      for (k = 0; k < numberOfFaceVertices[cellFaces[i][j]]; k++){
        if (nodeInCell[faceVert[cellFaces[i][j]][k]] == 0){
          nodeInCell[faceVert[cellFaces[i][j]][k]]++;
          numberOfCellNodes++;
        }
      }
    }
    for (j = 1; j < cellFaces[i][0] + 1; j++){
      for (k = 0; k < numberOfFaceVertices[cellFaces[i][j]]; k++){
        if (nodeInCell[faceVert[cellFaces[i][j]][k]] > 0){
          cellCentre[i][0] += x[faceVert[cellFaces[i][j]][k]]/(double)numberOfCellNodes;
          cellCentre[i][1] += y[faceVert[cellFaces[i][j]][k]]/(double)numberOfCellNodes;
          cellCentre[i][2] += z[faceVert[cellFaces[i][j]][k]]/(double)numberOfCellNodes;
          nodeInCell[faceVert[cellFaces[i][j]][k]] = 0;
        }
      }
    }
  }

  /* free memory */
  free(nodeInCell);
  for (i = 0; i < numberOfCells; i++)
    free(cellFaces[i]);
  free(cellFaces);

  return cellCentre;
}



/**
 * Works out cell volumes.
 */
double *get_cell_volumes_3d(
  const face_map *indx_f,
  const int numberOfCells,
  int **neighbourCell,
  double ****connecVec,
  double **faceCentre,
  double **cellCentre,
  double **normalVec,
  const double *faceArea
  )
{
  int i, j, k, numDims = 3;
  double *volume;

  /* allocate heap memory */
  volume = (double*)calloc(numberOfCells, sizeof(double));
  (*connecVec) = (double***)malloc(sizeof(double**)*(indx_f->numberOfFaces-indx_f->inner));

  /* calculate cell volumes through Green-Gauss Theorem: volume[] elements are all zero */
  for (i = indx_f->inner; i < indx_f->initial; i++){
    (*connecVec)[i] = (double**)malloc(sizeof(double*)*2);
    for (j = 0; j < 2; j++){
      (*connecVec)[i][j] = (double*)malloc(sizeof(double)*numDims);
      for (k = 0; k < numDims; k++){
        (*connecVec)[i][j][k] = faceCentre[i][k] - cellCentre[neighbourCell[i][j]][k];
      }
      volume[neighbourCell[i][j]] += fabs(dot_product((*connecVec)[i][j], normalVec[i], 3))*faceArea[i]/3.0;
    }
  }
  j = 0;
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    (*connecVec)[i] = (double**)malloc(sizeof(double*));
    (*connecVec)[i][j] = (double*)malloc(sizeof(double)*numDims);
    for (k = 0; k < numDims; k++){
      (*connecVec)[i][j][k] = faceCentre[i][k] - cellCentre[neighbourCell[i][j]][k];
    }
    volume[neighbourCell[i][j]] += fabs(dot_product((*connecVec)[i][j], normalVec[i], 3))*faceArea[i]/3.0;
  }

  return volume;
}



/**
 * Normal vectors must point towards left cell
 */
void orient_normal_vectors_3d(
  const face_map *indx_f,
  int **neighbourCell,
  double **cellCentre,
  double **faceCentre,
  double **normalVec
  )
{
  int i, j;
  int counter;

  counter = 0;

  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    if (is_left_cell_3d(cellCentre[neighbourCell[i][RIGHT]], faceCentre[i], normalVec[i])){
      for (j = 0; j < 3; j++){
        normalVec[i][j] *= -1.0;
      }
      counter++;
    }
  }

  if (counter > 1){
    verbose("Successfully re-oriented %d normal vectors", counter);
  }
  else if (counter > 0) {
    verbose("Successfully re-oriented %d normal vector", counter);
  }
  else {
    verbose("All normal vectors are already oriented");
  }
}



/**
 * Gets initial 2D geometry from 3D mesh. In particular it calculates the following:
 *     (1) numberOfEdges
 *     (2) indx_f0
 *     (3) neighbourFace[][]
 *     (4) edgeVert[][]
 */
void get_initial_2d_geometry(
  const face_map *indx_f,
  face_map *indx_f0,
  const int numberOfNodes,
  int *numberOfEdges,
  const int *numberOfFaceVertices,
  int **faceVert,
  int ***neighbourFace,
  int ***edgeVert
  )
{
  int i, ii, j, k, numSolid, numFar, numIn, numInitialFaces;
  int *solidAtNode, *farfieldAtNode, **typeOfEdge, **faceAtNode;

  /* find out which nodes correspond to solid and boundary faces */
  numInitialFaces = indx_f->solid - indx_f->initial;
  solidAtNode = (int*)calloc(numberOfNodes, sizeof(int));
  farfieldAtNode = (int*)calloc(numberOfNodes, sizeof(int));
  faceAtNode = (int**)malloc(sizeof(int*)*numberOfNodes);
  typeOfEdge = (int**)malloc(sizeof(int*)*numInitialFaces);
  for (i = 0; i < numberOfNodes; i++)
    faceAtNode[i] = (int*)calloc(1, sizeof(int));
  for (i = indx_f->solid; i < indx_f->farfield; i++){
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      solidAtNode[faceVert[i][j]]++;
    }
  }
  for (i = indx_f->farfield; i < indx_f->final; i++){
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      farfieldAtNode[faceVert[i][j]]++;
    }
  }

  for (i = indx_f->initial; i < indx_f->solid; i++){
    ii = i - indx_f->initial;
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      faceAtNode[faceVert[i][j]][0]++;
      faceAtNode[faceVert[i][j]] = (int*)realloc(faceAtNode[faceVert[i][j]], sizeof(int)*(faceAtNode[faceVert[i][j]][0] + 1));
      faceAtNode[faceVert[i][j]][faceAtNode[faceVert[i][j]][0]] = ii;
    }
  }

  /* find out number of edges of each type at initial 2d geometry */
  numSolid = numFar = numIn = 0;
  for (i = indx_f->initial; i < indx_f->solid; i++){
    ii = i - indx_f->initial;
    typeOfEdge[ii] = (int*)malloc(sizeof(int)*numberOfFaceVertices[i]);
    for (j = 0; j < numberOfFaceVertices[i] - 1; j++)
    {
      if (solidAtNode[faceVert[i][j]] > 0 && solidAtNode[faceVert[i][j + 1]] > 0){
        typeOfEdge[ii][j] = BMK_SOLID;
        numSolid++;
      }
      else if (farfieldAtNode[faceVert[i][j]] > 0 && farfieldAtNode[faceVert[i][j + 1]] > 0){
        typeOfEdge[ii][j] = BMK_FARFIELD;
        numFar++;
      }
      else {
        typeOfEdge[ii][j] = BMK_INNER;
        numIn++;
      }
    }
    if (solidAtNode[faceVert[i][numberOfFaceVertices[i] - 1]] > 0 && solidAtNode[faceVert[i][0]] > 0){
      typeOfEdge[ii][numberOfFaceVertices[i] - 1] = BMK_SOLID;
      numSolid++;
    }
    else if (farfieldAtNode[faceVert[i][numberOfFaceVertices[i] - 1]] > 0 && farfieldAtNode[faceVert[i][0]] > 0){
      typeOfEdge[ii][numberOfFaceVertices[i] - 1] = BMK_FARFIELD;
      numFar++;
    }
    else {
      typeOfEdge[ii][numberOfFaceVertices[i] - 1] = BMK_INNER;
      numIn++;
    }
  }
  if ((numIn + 1) / 2 != numIn / 2){
    error("failed to work out number of edges in initial geometry");
  }
  numIn /= 2;

  /* allocate heap memory */
  (*numberOfEdges) = numFar + numSolid + numIn;
  indx_f0->inner = 0;
  indx_f0->solid = indx_f0->initial = indx_f0->inner + numIn;
  indx_f0->farfield = indx_f0->solid + numSolid;
  indx_f0->final = indx_f0->numberOfFaces = indx_f0->farfield + numFar;
  (*neighbourFace) = (int**)malloc(sizeof(int*)*(*numberOfEdges));
  (*edgeVert) = (int**)malloc(sizeof(int*)*(*numberOfEdges));
  for (i = indx_f0->inner; i < indx_f0->initial; i++){
    (*neighbourFace)[i] = (int*)malloc(sizeof(int)* 2);
    (*edgeVert)[i] = (int*)malloc(sizeof(int)* 2);
    (*neighbourFace)[i][0] = (*neighbourFace)[i][1] = -1;
  }
  for (i = indx_f0->solid; i < indx_f0->numberOfFaces; i++){
    (*neighbourFace)[i] = (int*)malloc(sizeof(int));
    (*edgeVert)[i] = (int*)malloc(sizeof(int)* 2);
    (*neighbourFace)[i][0] = -1;
  }

  /* get initial edges */
  numIn = indx_f0->inner;
  numSolid = indx_f0->solid;
  numFar = indx_f0->farfield;
  for (i = indx_f->initial; i < indx_f->solid; i++)
  {
    ii = i - indx_f->initial;
    for (j = 0; j < numberOfFaceVertices[i] - 1; j++)
    {
      switch (typeOfEdge[ii][j])
      {
      case BMK_SOLID:
        (*edgeVert)[numSolid][0] = faceVert[i][j];
        (*edgeVert)[numSolid][1] = faceVert[i][j + 1];
        (*neighbourFace)[numSolid][RIGHT] = ii;
        numSolid++;
        break;
      case BMK_FARFIELD:
        (*edgeVert)[numFar][0] = faceVert[i][j];
        (*edgeVert)[numFar][1] = faceVert[i][j + 1];
        (*neighbourFace)[numFar][RIGHT] = ii;
        numFar++;
        break;
      case BMK_INNER:
        (*edgeVert)[numIn][0] = faceVert[i][j];
        (*edgeVert)[numIn][1] = faceVert[i][j + 1];
        (*neighbourFace)[numIn][RIGHT] = ii;
        (*neighbourFace)[numIn][LEFT] = find_other_face(ii, faceAtNode[(*edgeVert)[numIn][0]][0], faceAtNode[(*edgeVert)[numIn][1]][0], &faceAtNode[(*edgeVert)[numIn][0]][1], &faceAtNode[(*edgeVert)[numIn][1]][1]);
        k = (*neighbourFace)[numIn][LEFT] + indx_f->initial;
        typeOfEdge[(*neighbourFace)[numIn][LEFT]][find_edge_index_in_face((*edgeVert)[numIn][0], (*edgeVert)[numIn][1], numberOfFaceVertices[k], faceVert[k])] = -1;
        numIn++;
        break;
      default:
        break;
      }
    }
    switch (typeOfEdge[ii][numberOfFaceVertices[i] - 1])
    {
    case BMK_SOLID:
      (*edgeVert)[numSolid][0] = faceVert[i][numberOfFaceVertices[i] - 1];
      (*edgeVert)[numSolid][1] = faceVert[i][0];
      (*neighbourFace)[numSolid][RIGHT] = ii;
      numSolid++;
      break;
    case BMK_FARFIELD:
      (*edgeVert)[numFar][0] = faceVert[i][numberOfFaceVertices[i] - 1];
      (*edgeVert)[numFar][1] = faceVert[i][0];
      (*neighbourFace)[numFar][RIGHT] = ii;
      numFar++;
      break;
    case BMK_INNER:
      (*edgeVert)[numIn][0] = faceVert[i][numberOfFaceVertices[i] - 1];
      (*edgeVert)[numIn][1] = faceVert[i][0];
      (*neighbourFace)[numIn][RIGHT] = ii;
      (*neighbourFace)[numIn][LEFT] = find_other_face(ii, faceAtNode[(*edgeVert)[numIn][0]][0], faceAtNode[(*edgeVert)[numIn][1]][0], &faceAtNode[(*edgeVert)[numIn][0]][1], &faceAtNode[(*edgeVert)[numIn][1]][1]);
      k = (*neighbourFace)[numIn][LEFT] + indx_f->initial;
      typeOfEdge[(*neighbourFace)[numIn][LEFT]][find_edge_index_in_face((*edgeVert)[numIn][0], (*edgeVert)[numIn][1], numberOfFaceVertices[k], faceVert[k])] = -1;
      numIn++;
      break;
    default:
      break;
    }
  }

  free(solidAtNode);
  free(farfieldAtNode);
  for (i = 0; i < indx_f->solid - indx_f->initial; i++){
    free(typeOfEdge[i]);
  }
  free(typeOfEdge);
  for (i = 0; i < numberOfNodes; i++)
    free(faceAtNode[i]);
  free(faceAtNode);
}
