/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "common.h"
#include "stdarg.h"

#define temperature(rho,p,Minf) (GAMMA*pow((Minf), 2.0)*(p) / (rho))
#define pressure(rho,u,v,E) ((rho)*(GAMMA - 1.0)*((E) - (pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define energy(rho,u,v,p) (((p) / ((GAMMA - 1.0)*(rho))) + ((pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define suderland(Tref) (110.0/(Tref))
#define viscosity(Tref,T) (pow((T), 1.5)*(1.0 + suderland(Tref)) / ((T) + suderland(Tref)))
#define sound_speed(p,rho) sqrt(GAMMA*(p)/(rho))


/**
 * Allocates dynamic memory for Euler (common to JST & UW) solver.
 *
 * @warning This function allocates dynamic memory. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void allocate_common_memory_Euler(const int numberOfCells, const face_map *indx_f, double **rho0,
    double **dt, double **specRad, double **specRadFace, double ***w0, double ***invflux)
{
  int i;
  const int num_eqs = 4;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;

  /* 1D arrays */
  (*rho0) = (double*)malloc(sizeof(double)*numberOfCells);
  (*dt) = (double*)malloc(sizeof(double)*numberOfCells);
  (*specRad) = (double*)malloc(sizeof(double)*numberOfCells);
  (*specRadFace) = (double*)malloc(sizeof(double)*numberOfFaces);

  /* 2D arrays */
  (*invflux) = (double**)malloc(sizeof(double*)*numberOfFaces);
  (*w0) = (double**)malloc(sizeof(double*)*numberOfCells);

  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++)
    (*invflux)[i] = (double*) calloc(num_eqs, sizeof(double));

  for (i = 0; i < numberOfCells; i++)
    (*w0)[i] = (double*) calloc(num_eqs, sizeof(double));
}


/**
 * Allocates memory for RANS (common to JST & UW) solver.
 *
 * @warning This function allocates dynamic memory. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void allocate_common_memory_RANS(const int numberOfCells, const face_map *indx_f, double **mubar0,
    double **tauxx, double **tauxy, double **tauyy, double **qx, double **qy, double **production,
    double **destruction, double **diffusion_s, double **diffusion_v, double **convection,
    double **SAflux, double ***viscflux)
{
  int i;
  const int num_eqs = 4;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;

  /* 1D arrays */
  (*mubar0) = (double*)malloc(sizeof(double)*numberOfCells);
  (*tauxx) = (double*)malloc(sizeof(double)*numberOfFaces);
  (*tauxy) = (double*)malloc(sizeof(double)*numberOfFaces);
  (*tauyy) = (double*)malloc(sizeof(double)*numberOfFaces);
  (*qx) = (double*)malloc(sizeof(double)*numberOfFaces);
  (*qy) = (double*)malloc(sizeof(double)*numberOfFaces);
  (*production) = (double*)malloc(sizeof(double)*numberOfCells);
  (*destruction) = (double*)malloc(sizeof(double)*numberOfCells);
  (*diffusion_s) = (double*)malloc(sizeof(double)*numberOfFaces);
  (*diffusion_v) = (double*)malloc(sizeof(double)*numberOfCells);
  (*convection) = (double*)malloc(sizeof(double)*numberOfFaces);
  (*SAflux) = (double*)malloc(sizeof(double)*numberOfCells);

  /* 2D arrays */
  (*viscflux) = (double**)malloc(sizeof(double*)*numberOfFaces);

  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++)
    (*viscflux)[i] = (double*) malloc(sizeof(double) * num_eqs);
}


/**
 * Releases common heap memory for Euler-JST solver.
 */
void free_common_memory_Euler(
  const int numberOfCells,
  const int numberOfFaces,
  double *rho0,
  double *dt,
  double *specRad,
  double *specRadFace,
  double **w0,
  double **invflux
  )
{
  int i;

  /* 1D arrays */
  free(rho0);
  free(dt);
  free(specRad);
  free(specRadFace);

  /* 2D arrays */
  for (i = 0; i < numberOfCells; i++)
    free(w0[i]);
  for (i = 0; i < numberOfFaces; i++)
    free(invflux[i]);

  free(w0);
  free(invflux);
}


/**
 * Releases common heap memory for RANS-JST solver.
 */
void free_common_memory_RANS(
  const int numberOfCells,
  const int numberOfFaces,
  double *mubar0,
  double *tauxx,
  double *tauxy,
  double *tauyy,
  double *qx,
  double *qy,
  double *production,
  double *destruction,
  double *diffusion_s,
  double *diffusion_v,
  double *convection,
  double *SAflux,
  double **viscflux
  )
{
  int i;

  /* 1D arrays */
  free(mubar0);
  free(tauxx);
  free(tauxy);
  free(tauyy);
  free(qx);
  free(qy);
  free(production);
  free(destruction);
  free(diffusion_s);
  free(diffusion_v);
  free(convection);
  free(SAflux);

  /* 2D arrays */

  for (i = 0; i < numberOfFaces; i++)
    free(viscflux[i]);

  free(viscflux);
}


/**
 * Allocates memory for weighted Runge-Kutta residuals.
 */
double **allocate_memory_RKRes(const int numberOfCells)
{
  double **RKRes;

  RKRes = (double**)malloc(sizeof(double*)*numberOfCells);
  for (int i = 0; i < numberOfCells; i++)
    RKRes[i] = (double*) calloc(4, sizeof(double));

  return RKRes;
}


/**
 * Allocates memory for weighted SA residuals.
 */
double *allocate_memory_SARes(const int numberOfCells)
{
  double *SARes;

  SARes = (double*)calloc(numberOfCells, sizeof(double));

  return SARes;
}


/**
 * Releases memory for weighted Runge-Kutta residuals.
 */
void free_memory_RKRes(const int numberOfCells, double **RKRes)
{
  for (int i = 0; i < numberOfCells; i++)
    free(RKRes[i]);

  free(RKRes);
}


/**
 * Releases memory for weighted SA residuals.
 */
void free_memory_SARes(double *SARes)
{
  free(SARes);
}


/**
 * Calculates spacetime (ST) geometry for initial problem. In particular it
 * works out the following:
 *     (1)  numberOfNodes
 *     (2)  numberOfFaces
 *     (3)  numberOfCells
 *     (4)  indx_f
 *     (5)  neighbourCell
 *     (6)  numberOfFaceVertices
 *     (7)  faceVert
 *     (8)  x
 *     (9)  y
 *     (10) t
 */
void get_st_initial(const int numNodesST, const face_map *indx_f0,
    const face_map *indx_fST, const int *numFaceVertST, int **edgeVert, int **faceVertST,
    int **neighbourFace, const double *xST, const double *yST,
    int *numNodes, int *numFaces, int *numCells, face_map **indx_f, int **numFaceVerts,
    int ***faceVert, int ***neighbourCell, double **x, double **y, double **t)
{
  int i, j, k, numInitialNodes;
  int *localId;
  const int numInitialCells = indx_fST->solid - indx_fST->initial;
  const int numInitialFaces = indx_f0->numberOfFaces;

  localId = (int*)malloc(sizeof(int)*numNodesST);
  for (i = 0; i < numNodesST; i++)
    localId[i] = -1;
  for (i = indx_fST->initial; i < indx_fST->solid; i++)
  {
    for (j = 0; j < numFaceVertST[i]; j++)
      localId[faceVertST[i][j]]++;
  }
  numInitialNodes = 0;
  for (i = 0; i < numNodesST; i++)
  {
    if (localId[i] >= 0)
      localId[i] = numInitialNodes++;
  }

  (*numNodes) = 2 * numInitialNodes;
  (*numFaces) = 2 * numInitialCells + numInitialFaces;
  (*numCells) = numInitialCells;
  (*indx_f) = (face_map*)malloc(sizeof(face_map));
  (*indx_f)->inner = 0;
  (*indx_f)->initial = indx_f0->solid - indx_f0->inner;
  (*indx_f)->solid = (*indx_f)->initial + numInitialCells;
  (*indx_f)->farfield = (*indx_f)->solid + (indx_f0->farfield - indx_f0->solid);
  (*indx_f)->final = (*indx_f)->farfield + (indx_f0->numberOfFaces - indx_f0->farfield);
  (*indx_f)->numberOfFaces = (*indx_f)->final + numInitialCells;
  if ((*numFaces) != (*indx_f)->numberOfFaces)
    error("failed to work out number of faces in initial geometry");

  /* allocate dynamic memory */
  (*neighbourCell) = (int**)malloc(sizeof(int*)*(*numFaces));
  (*numFaceVerts) = (int*)malloc(sizeof(int)*(*numFaces));
  (*faceVert) = (int**)malloc(sizeof(int*)*(*numFaces));
  (*x) = (double*)malloc(sizeof(double)*(*numNodes));
  (*y) = (double*)malloc(sizeof(double)*(*numNodes));
  (*t) = (double*)malloc(sizeof(double)*(*numNodes));

  for (i = 0; i < (*numFaces); i++)
    (*neighbourCell)[i] = (int*)malloc(sizeof(int)*2);

  /* calculate neighbourCells, numFaceVerts, faceVert */

  for (i = 0; i < (*indx_f)->initial; i++)
  {
    (*neighbourCell)[i][RIGHT] = neighbourFace[i][RIGHT];
    (*neighbourCell)[i][LEFT] = neighbourFace[i][LEFT];
    (*numFaceVerts)[i] = 4;
    (*faceVert)[i] = (int*)malloc(sizeof(int)*(*numFaceVerts)[i]);
    for (k = 0; k < 2; k++)
    {
      (*faceVert)[i][k] = localId[edgeVert[i][k]];
      (*faceVert)[i][k + 2] = localId[edgeVert[i][1 - k]] + numInitialNodes;
    }
  }

  for (i = (*indx_f)->initial; i < (*indx_f)->solid; i++)
  {
    j = i - (*indx_f)->initial;
    (*neighbourCell)[i][RIGHT] = j;
    (*neighbourCell)[i][LEFT] = -1;
    j += indx_fST->initial;
    (*numFaceVerts)[i] = numFaceVertST[j];
    (*faceVert)[i] = (int*)malloc(sizeof(int)*(*numFaceVerts)[i]);
    for (k = 0; k < numFaceVertST[j]; k++)
      (*faceVert)[i][k] = localId[faceVertST[j][k]];
  }

  for (i = (*indx_f)->solid; i < (*indx_f)->farfield; i++)
  {
    j = indx_f0->solid + i - (*indx_f)->solid;
    (*neighbourCell)[i][RIGHT] = neighbourFace[j][RIGHT];
    (*neighbourCell)[i][LEFT] = -1;
    (*numFaceVerts)[i] = 4;
    (*faceVert)[i] = (int*)malloc(sizeof(int)*(*numFaceVerts)[i]);
    for (k = 0; k < 2; k++)
    {
      (*faceVert)[i][k] = localId[edgeVert[j][k]];
      (*faceVert)[i][k + 2] = localId[edgeVert[j][1 - k]] + numInitialNodes;
    }
  }

  for (i = (*indx_f)->farfield; i < (*indx_f)->final; i++)
  {
    j = indx_f0->farfield + i - (*indx_f)->farfield;
    (*neighbourCell)[i][RIGHT] = neighbourFace[j][RIGHT];
    (*neighbourCell)[i][LEFT] = -1;
    (*numFaceVerts)[i] = 4;
    (*faceVert)[i] = (int*)malloc(sizeof(int)*(*numFaceVerts)[i]);
    for (k = 0; k < 2; k++)
    {
      (*faceVert)[i][k] = localId[edgeVert[j][k]];
      (*faceVert)[i][k + 2] = localId[edgeVert[j][1 - k]] + numInitialNodes;
    }
  }

  for (i = (*indx_f)->final; i < (*indx_f)->numberOfFaces; i++)
  {
    j = i - (*indx_f)->final;
    (*neighbourCell)[i][RIGHT] = j;
    (*neighbourCell)[i][LEFT] = -1;
    j += indx_fST->initial;
    (*numFaceVerts)[i] = numFaceVertST[j];
    (*faceVert)[i] = (int*)malloc(sizeof(int)*(*numFaceVerts)[i]);
    for (k = 0; k < numFaceVertST[j]; k++)
      (*faceVert)[i][k] = localId[faceVertST[j][k]] + numInitialNodes;
  }


  /* calculate coordinates */
  for (i = indx_fST->initial; i < indx_fST->solid; i++)
  {
    for (j = 0; j < numFaceVertST[i]; j++)
    {
      k = localId[faceVertST[i][j]];
      (*x)[k] = (*x)[k + numInitialNodes] = xST[faceVertST[i][j]];
      (*y)[k] = (*y)[k + numInitialNodes] = yST[faceVertST[i][j]];
      (*t)[k] = 0.0;
      (*t)[k + numInitialNodes] = 1.0;
    }
  }

  free(localId);
}



/**
 * Work out gradients of Euler primitive vars (rho, u, v, p) at cell centres.
 *
 * @warning This function allocates dynamic memory for gradients. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_gradients_Euler(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const double *volume,
  const flow *cellFlow,
  double ***gradient_rho,
  double ***gradient_u,
  double ***gradient_v,
  double ***gradient_p
  )
{
  int i, j, left, right;
  int num_dims = 3;
  double normArea, dp, drho, du, dv;
  double **grad_rho, **grad_u, **grad_v, **grad_p;

  // allocate dynamic memory for gradients
  grad_rho = (double**)malloc(sizeof(double*)*numberOfCells);
  grad_u = (double**)malloc(sizeof(double*)*numberOfCells);
  grad_v = (double**)malloc(sizeof(double*)*numberOfCells);
  grad_p = (double**)malloc(sizeof(double*)*numberOfCells);

  for (i = 0; i < numberOfCells; i++)
  {
    grad_rho[i] = (double*) calloc(num_dims, sizeof(double));
    grad_u[i] = (double*) calloc(num_dims, sizeof(double));
    grad_v[i] = (double*) calloc(num_dims, sizeof(double));
    grad_p[i] = (double*) calloc(num_dims, sizeof(double));
  }

  /* iterate over inner faces (single-threaded) */
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    dp   = (cellFlow[left].p   + cellFlow[right].p)   / 2.0;
    drho = (cellFlow[left].rho + cellFlow[right].rho) / 2.0;
    du   = (cellFlow[left].u   + cellFlow[right].u)   / 2.0;
    dv   = (cellFlow[left].v   + cellFlow[right].v)   / 2.0;
    for (j = 0; j < num_dims; j++)
    {
      normArea = normalVec[i][j] * area[i];
      grad_rho[right][j] += drho * normArea;
      grad_u[right][j]   +=  du  * normArea;
      grad_v[right][j]   +=  dv  * normArea;
      grad_p[right][j]   +=  dp  * normArea;
      grad_rho[left][j]  -= drho * normArea;
      grad_u[left][j]    -=  du  * normArea;
      grad_v[left][j]    -=  dv  * normArea;
      grad_p[left][j]    -=  dp  * normArea;
    }
  }

  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    dp = (cellFlow[left].p + cellFlow[right].p) / 2.0;
    drho = (cellFlow[left].rho + cellFlow[right].rho) / 2.0;
    du = (cellFlow[left].u + cellFlow[right].u) / 2.0;
    dv = (cellFlow[left].v + cellFlow[right].v) / 2.0;
    for (j = 0; j < num_dims; j++)
    {
      normArea = normalVec[i][j] * area[i];
      grad_rho[right][j] += drho * normArea;
      grad_u[right][j] += du * normArea;
      grad_v[right][j] += dv * normArea;
      grad_p[right][j] += dp * normArea;
    }
  }

  /*iterate over cells */
#pragma omp parallel for private(j)
  for (i = 0; i < numberOfCells; i++)
  {
    for (j = 0; j < num_dims; j++)
    {
      grad_rho[i][j] /= volume[i];
      grad_u[i][j] /= volume[i];
      grad_v[i][j] /= volume[i];
      grad_p[i][j] /= volume[i];
    }
  }

  (*gradient_rho) = grad_rho;
  (*gradient_u) = grad_u;
  (*gradient_v) = grad_v;
  (*gradient_p) = grad_p;
}


/**
 * Work out gradient of temperature (T) at cell centres.
 *
 * @warning This function allocates dynamic memory for gradient. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_gradient_T(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const double *volume,
  const flow *cellFlow,
  double ***gradient_T
  )
{
  int i, j, left, right;
  int num_dims = 3;
  double normArea, dT;
  double **grad_T;

  // allocate dynamic memory for gradient
  grad_T = (double**)malloc(sizeof(double*)*numberOfCells);
  for (i = 0; i < numberOfCells; i++)
    grad_T[i] = (double*) calloc(num_dims, sizeof(double));

  /* iterate over inner faces (single-threaded) */
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    dT = (cellFlow[left].T + cellFlow[right].T) / 2.0;
    for (j = 0; j < num_dims; j++)
    {
      normArea = normalVec[i][j] * area[i];
      grad_T[right][j] += dT * normArea;
      grad_T[left][j]  -= dT * normArea;
    }
  }

  /* iterate over boundary faces (single-threaded) */
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    dT = (cellFlow[left].T + cellFlow[right].T) / 2.0;
    for (j = 0; j < num_dims; j++)
    {
      normArea = normalVec[i][j] * area[i];
      grad_T[right][j] += dT * normArea;
    }
  }

  /*iterate over cells */
#pragma omp parallel for private(j)
  for (i = 0; i < numberOfCells; i++)
  {
    for (j = 0; j < num_dims; j++)
      grad_T[i][j] /= volume[i];
  }

  (*gradient_T) = grad_T;
}

/**
 * Work out gradient of nubar at cell centres.
 *
 * @warning This function allocates dynamic memory for gradients. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_gradient_nub(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const double *volume,
  const flow *cellFlow,
  double ***gradient_nub
  )
{
  int i, j, k, left, right;
  int num_dims = 3;
  double normArea, drho, dnub;
  const double sign[2] = {1.0, -1.0};
  double **grad_nub;

  // allocate dynamic memory for gradients
  grad_nub = (double**) malloc(sizeof(double*) * numberOfCells);
  for (i = 0; i < numberOfCells; i++)
    grad_nub[i] = (double*) calloc(num_dims, sizeof(double));

  /* iterate over inner faces (single-threaded) */
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    for (j = 0; j < num_dims; j++)
    {
      normArea = normalVec[i][j] * area[i];
      drho = (cellFlow[left].rho + cellFlow[right].rho) / 2.0;
      dnub = (cellFlow[left].mubar + cellFlow[right].mubar) / (2.0*drho);
      for (k = 0; k < 2; k++)
        grad_nub[neighbourCell[i][k]][j] += sign[k] * dnub * normArea;
    }
  }

  /* iterate over boundary faces (multi-threaded) */
  k = RIGHT;
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    for (j = 0; j < num_dims; j++)
    {
      normArea = normalVec[i][j] * area[i];
      drho = (cellFlow[left].rho + cellFlow[right].rho) / 2.0;
      dnub = (cellFlow[left].mubar + cellFlow[right].mubar) / (2.0 * drho);
      grad_nub[neighbourCell[i][k]][j] += sign[k] * dnub * normArea;
    }
  }

  /*iterate over cells */
#pragma omp parallel for private(i, j)
  for (i = 0; i < numberOfCells; i++)
  {
    for (j = 0; j < num_dims; j++)
      grad_nub[i][j] /= volume[i];
  }

  (*gradient_nub) = grad_nub;
}

/**
 * Calculate undivided Laplacians for Euler eqs. via a central-difference approach.
 * There are 4 components at each cell: continuity (1), momentum (2) and energy (1).
 *
 * @warning This function allocates dynamic memory for laplacians. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
double **calculate_undivided_Laplacians_Euler(
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double *faceFactor,  // usually snorm = sqrt(nx^2 + ny^2) but perhaps could make snorm = 1.0.
  const flow *cellFlow,
  double **W
  )
{
  int i;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  const int num_eqs = 4; // hard-coded for Euler eqs. of motion - perhaps could make it a run-time parameter.
  double **lapF, **laplacian;

  // allocate dynamic memory for face contributions to cell Laplacians.
  lapF = (double**)malloc(sizeof(double*)*numberOfFaces);
  laplacian = (double**)malloc(sizeof(double*)*numberOfCells);
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++)
    lapF[i] = (double*) malloc(sizeof(double) * num_eqs);
  for (i = 0; i < numberOfCells; i++)
    laplacian[i] = (double*) calloc(num_eqs, sizeof(double));

    /* loop over faces */
#pragma omp parallel for
  for (i = 0; i < indx_f->numberOfFaces; i++)
  {
    lapF[i][0] = (cellFlow[neighbourCell[i][LEFT]].rho - cellFlow[neighbourCell[i][RIGHT]].rho) * faceFactor[i];
    lapF[i][1] = (cellFlow[neighbourCell[i][LEFT]].rho * cellFlow[neighbourCell[i][LEFT]].u
        - cellFlow[neighbourCell[i][RIGHT]].rho * cellFlow[neighbourCell[i][RIGHT]].u) * faceFactor[i];
    lapF[i][2] = (cellFlow[neighbourCell[i][LEFT]].rho * cellFlow[neighbourCell[i][LEFT]].v
        - cellFlow[neighbourCell[i][RIGHT]].rho * cellFlow[neighbourCell[i][RIGHT]].v) * faceFactor[i];
    lapF[i][3] = (cellFlow[neighbourCell[i][LEFT]].rho * cellFlow[neighbourCell[i][LEFT]].e
        - cellFlow[neighbourCell[i][RIGHT]].rho * cellFlow[neighbourCell[i][RIGHT]].e) * faceFactor[i];
  }

  /* work out cell laplacians (loop over inner faces -> single-threaded) */
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    for (int j = 0; j < num_eqs; j++)
    {
      laplacian[neighbourCell[i][RIGHT]][j] += lapF[i][j];
      laplacian[neighbourCell[i][LEFT]][j] -= lapF[i][j];
    }
  }

  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    for (int j = 0; j < num_eqs; j++)
      laplacian[neighbourCell[i][RIGHT]][j] += lapF[i][j];
  }

  // release heap memory
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++)
    free(lapF[i]);

  free(lapF);

  return laplacian;
}


/**
 * Calculate conserved variables from primitive variables.
 */
void calculate_conserved_vars(
  const int numberOfCells,
  const flow *cellFlow,
  double **W
  )
{
#pragma omp parallel for
  for (int i = 0; i < numberOfCells; i++)
  {
    W[i][0] = cellFlow[i].rho;
    W[i][1] = cellFlow[i].rho * cellFlow[i].u;
    W[i][2] = cellFlow[i].rho * cellFlow[i].v;
    W[i][3] = cellFlow[i].rho * cellFlow[i].e;
  }
}


/**
 * Works out primitive vars (rho, u, v, p, e) from conserved vars.
 */
void calculate_primitive_vars(
  const int numberOfCells,
  double **W,
  flow *cellFlow
  )
{
#pragma omp parallel for
  for (int i = 0; i < numberOfCells; i++)
  {
    cellFlow[i].rho = W[i][0];
    cellFlow[i].u = W[i][1] / W[i][0];
    cellFlow[i].v = W[i][2] / W[i][0];
    cellFlow[i].e = W[i][3] / W[i][0];
    cellFlow[i].p = pressure(cellFlow[i].rho, cellFlow[i].u, cellFlow[i].v, cellFlow[i].e);
  }
}


/**
 * Calculates primitive variables at halo cell at a farfield boundary face.
 */
static void calculate_farfield_BC_Euler(
  const int faceId,
  const double sosinf,
  const double Minf,
  double **normalVec,
  const flow *refFlow,
  const flow *cellFlow,
  flow *haloFlow
  )
{
  double qfar, qb, q0, Rm, Rp, ub, vb, rhob, pb, entrop, porho, sosb;

  /* Riemann invariants */
  qfar = refFlow->u*normalVec[faceId][0] + refFlow->v*normalVec[faceId][1];
  Rm = qfar - (2.0*sosinf / (GAMMA - 1.0));
  q0 = cellFlow->u*normalVec[faceId][0] + cellFlow->v*normalVec[faceId][1];
  Rp = q0 + (2.0*sound_speed(cellFlow->p, cellFlow->rho) / (GAMMA - 1.0));

  /* Values at boudary */
  qb = 0.5*(Rp + Rm);
  sosb = 0.25*(GAMMA - 1)*(Rp - Rm);
  if (q0 / sosb > 1.0){
    ub = cellFlow->u;
    vb = cellFlow->v;
    entrop = cellFlow->p / pow(cellFlow->rho, GAMMA);
  }
  else if (q0 / sosb < -1.0){
    ub = refFlow->u;
    vb = refFlow->v;
    entrop = refFlow->p / pow(refFlow->rho, GAMMA);
  }
  else {  /* subsonic flow at farfield boundaries */
    if (qb <= 0.0){
      ub = refFlow->u + (qb - qfar)*normalVec[faceId][0];
      vb = refFlow->v + (qb - qfar)*normalVec[faceId][1];
      entrop = refFlow->p / pow(refFlow->rho, GAMMA);
    }
    else {
      ub = cellFlow->u + (qb - q0)*normalVec[faceId][0];
      vb = cellFlow->v + (qb - q0)*normalVec[faceId][1];
      entrop = cellFlow->p / pow(cellFlow->rho, GAMMA);
    }
  }

  porho = pow(sosb, 2.0) / GAMMA;
  rhob = pow(porho / entrop, 1.0 / (GAMMA - 1.0));
  pb = porho*rhob;

  /* Values at halo cell */
  haloFlow->rho = 2.0*rhob - cellFlow->rho;
  haloFlow->u = 2.0*ub - cellFlow->u;
  haloFlow->v = 2.0*vb - cellFlow->v;
  haloFlow->p = 2.0*pb - cellFlow->p;
}

/**
 * Calculates primitive variables at halo cell at a farfield boundary face.
 */
static void calculate_farfield_BC_RANS(
  const int faceId,
  const double sosinf,
  const double Minf,
  double **normalVec,
  const turb *saConst,
  const flow *refFlow,
  const flow *cellFlow,
  flow *haloFlow
  )
{
  calculate_farfield_BC_Euler(faceId, sosinf, Minf, normalVec, refFlow, cellFlow, haloFlow);

  if ((haloFlow->u + cellFlow->u)*normalVec[faceId][0] + (haloFlow->v + cellFlow->v)*normalVec[faceId][1] >= 0.0)
  {
    haloFlow->mubar = cellFlow->mubar;
  }
  else {
    haloFlow->mubar = saConst->far_mubar;
  }
}

/**
 * Calculates primitive variables at halo cell at a no-slip solid face.
 */
static void calculate_solid_BC_NoSlip(
  const int faceId,
  double **solidFaceVel,
  const flow *cellFlow,
  flow *haloFlow
  )
{
  haloFlow->rho = cellFlow->rho;
  haloFlow->u = 2.0*solidFaceVel[faceId][0] - cellFlow->u;
  haloFlow->v = 2.0*solidFaceVel[faceId][1] - cellFlow->v;
  haloFlow->p = cellFlow->p;
  haloFlow->mubar = -cellFlow->mubar;
}

/**
 * Calculates primitive variables at halo cell at a no-slip solid face.
 */
static void calculate_solid_BC_Euler(
  const int cellId,
  const int halloId,
  const double *normalVec,
  flow *cellFlow
  )
{
  const double ub = (cellFlow[cellId].u * pow(normalVec[1], 2.0) - cellFlow[cellId].v * normalVec[0] * normalVec[1]
      - normalVec[2] * normalVec[0]) / (pow(normalVec[0], 2.0) + pow(normalVec[1], 2.0));
  const double vb = (cellFlow[cellId].v * pow(normalVec[0], 2.0) - cellFlow[cellId].u * normalVec[0] * normalVec[1]
      - normalVec[2] * normalVec[1]) / (pow(normalVec[0], 2.0) + pow(normalVec[1], 2.0));

  cellFlow[halloId].rho = cellFlow[cellId].rho;
  cellFlow[halloId].u = 2.0*ub - cellFlow[cellId].u;
  cellFlow[halloId].v = 2.0*vb - cellFlow[cellId].v;
  cellFlow[halloId].p = cellFlow[cellId].p;
}

/**
 * Calculates primitive variables at halo cell at a final time boundary face.
 */
static void calculate_final_BC_Euler(
  const int cellId,
  const int halloId,
  flow *cellFlow
  )
{
  cellFlow[halloId].rho = cellFlow[cellId].rho;
  cellFlow[halloId].u = cellFlow[cellId].u;
  cellFlow[halloId].v = cellFlow[cellId].v;
  cellFlow[halloId].p = cellFlow[cellId].p;
}


/**
 * Calculates primitive variables at halo cell at a final time boundary face.
 */
static void calculate_final_BC_RANS(
  const int cellId,
  const int halloId,
  flow *cellFlow
  )
{
  calculate_final_BC_Euler(cellId, halloId, cellFlow);
  cellFlow[halloId].mubar = cellFlow[cellId].mubar;
}

/**
 * Calculate primitive variables (rho, u, v, p, mubar) at halo cells.
 */
void calculate_halo_cells_RANS(
  const double sosinf,
  const double Minf,
  double **normalVec,
  double **solidFaceVel,
  int **neighbourCell,
  const face_map *indx_f,
  const flow *refFlow,
  const turb *saConst,
  flow *cellFlow
  )
{
  /* no update of hallo cells at initial boundary */
  /* solid faces */
#pragma omp parallel for
    for (int i = indx_f->solid; i < indx_f->farfield; i++)
    {
      const int faceId = i - indx_f->solid;
      calculate_solid_BC_NoSlip(faceId, solidFaceVel, &cellFlow[neighbourCell[i][RIGHT]],
        &cellFlow[neighbourCell[i][LEFT]]);
    }

  /* farfield faces */
#pragma omp parallel for
    for (int i = indx_f->farfield; i < indx_f->final; i++)
      calculate_farfield_BC_RANS(i, sosinf, Minf, normalVec, saConst, refFlow, &cellFlow[neighbourCell[i][RIGHT]],
        &cellFlow[neighbourCell[i][LEFT]]);

  /* final faces */
#pragma omp parallel for
    for (int i = indx_f->final; i < indx_f->numberOfFaces; i++)
      calculate_final_BC_RANS(neighbourCell[i][RIGHT], neighbourCell[i][LEFT], cellFlow);

}

/**
 * Calculate primitive variables (rho, u, v, p) at halo cells.
 */
void calculate_halo_cells_Euler(
  const double sosinf,
  const double Minf,
  double **normalVec,
  int **neighbourCell,
  const face_map *indx_f,
  const flow *refFlow,
  flow *cellFlow
  )
{
  /* no update of hallo cells at initial boundary */
  /* solid faces */
#pragma omp parallel for
  for (int i = indx_f->solid; i < indx_f->farfield; i++)
    calculate_solid_BC_Euler(neighbourCell[i][RIGHT], neighbourCell[i][LEFT], normalVec[i], cellFlow);

  /* farfield faces */
#pragma omp parallel for
  for (int i = indx_f->farfield; i < indx_f->final; i++)
    calculate_farfield_BC_Euler(i, sosinf, Minf, normalVec, refFlow, &cellFlow[neighbourCell[i][RIGHT]],
      &cellFlow[neighbourCell[i][LEFT]]);

  /* final faces */
#pragma omp parallel for
  for (int i = indx_f->final; i < indx_f->numberOfFaces; i++)
    calculate_final_BC_Euler(neighbourCell[i][RIGHT], neighbourCell[i][LEFT], cellFlow);
}

/**
 * Constructs augmented state from independent variables (rho, u, v, p, mubar) for RANS.
 */
void construct_augmented_state_RANS(const int numValues, const double Minf, const double Tref,
    const turb *saConst, flow *flowVar)
{
  double chi, chi3, fv1;

#pragma omp parallel for private(chi, chi3, fv1)
  for (int i = 0; i < numValues; i++)
  {
    flowVar[i].e = energy(flowVar[i].rho, flowVar[i].u, flowVar[i].v, flowVar[i].p);
    flowVar[i].T = temperature(flowVar[i].rho, flowVar[i].p, Minf);
    flowVar[i].mu = viscosity(Tref, flowVar[i].T);
    chi = max(flowVar[i].mubar / flowVar[i].mu, 1.0e-4);
    chi3 = pow(chi, 3.0);
    fv1 = chi3 / (chi3 + saConst->cv1_3);
    flowVar[i].mut = flowVar[i].mubar * fv1;
  }
}

/**
 * Constructs augmented state from independent variables (rho, u, v, p) for Euler.
 */
void construct_augmented_state_Euler(const int numValues, flow *flowVar)
{
#pragma omp parallel for
  for (int i = 0; i < numValues; i++)
    flowVar[i].e = energy(flowVar[i].rho, flowVar[i].u, flowVar[i].v, flowVar[i].p);
}


/**
 * Work out gradients at solid boundary.
 */
static void gradients_at_solid(
  const int faceIndx,
  int **neighbourCell,
  const flow *cellFlow,
  double ***connecVec,
  double **normalVec,
  double **grad_T,
  double *duF,
  double *dvF,
  double *dTF
  )
{
  int i, num_dims = 3;
  double dudn, dvdn, dn;

  /* NOTE THAT THIS GRADIENTS AT FACES ARE WORKED OUT USING A CENTRAL DIFFERENCE APPROACH */

  dn = 0.0;
  for (i = 0; i < num_dims; i++){
    dn += connecVec[faceIndx][RIGHT][i] * normalVec[faceIndx][i];
  }
  if (fabs(dn) < EPSILON){
    error("dividing by zero");
  }
  dudn = -cellFlow[neighbourCell[faceIndx][RIGHT]].u / dn;
  dvdn = -cellFlow[neighbourCell[faceIndx][RIGHT]].v / dn;

  for (i = 0; i < 2; i++){
    duF[i] = dudn*normalVec[faceIndx][i];
    dvF[i] = dvdn*normalVec[faceIndx][i];
    dTF[i] = grad_T[neighbourCell[faceIndx][RIGHT]][i];
  }
}


/**
 * Works out gradients at farfield boundary.
 */
static void gradients_at_farfield(
    const int faceIndx,
    int **neighbourCell,
    double **grad_u,
    double **grad_v,
    double **grad_T,
    double *duF,
    double *dvF,
    double *dTF
  )
{
  for (int j = 0; j < 2; j++)
  {
    duF[j] = grad_u[neighbourCell[faceIndx][RIGHT]][j];
    dvF[j] = grad_v[neighbourCell[faceIndx][RIGHT]][j];
    dTF[j] = grad_T[neighbourCell[faceIndx][RIGHT]][j];
  }
}

/**
 * Works out gradients at inner face.
 */
static void gradients_at_inner(
    const int faceIndx,
    int **neighbourCell,
    double **grad_u,
    double **grad_v,
    double **grad_T,
    double *duF,
    double *dvF,
    double *dTF
  )
{
  for (int j = 0; j < 2; j++)
  {
    duF[j] = (grad_u[neighbourCell[faceIndx][LEFT]][j] + grad_u[neighbourCell[faceIndx][RIGHT]][j]) / 2.0;
    dvF[j] = (grad_v[neighbourCell[faceIndx][LEFT]][j] + grad_v[neighbourCell[faceIndx][RIGHT]][j]) / 2.0;
    dTF[j] = (grad_T[neighbourCell[faceIndx][LEFT]][j] + grad_T[neighbourCell[faceIndx][RIGHT]][j]) / 2.0;
  }
}


/**
 * Calculates viscous flux at face.
 */
static void calculate_viscous_flux(
  const int faceId,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const double tauxx,
  const double tauxy,
  const double tauyy,
  const double qx,
  const double qy,
  const flow *cellFlow,
  double **viscflux
  )
{
  const double uF = (cellFlow[neighbourCell[faceId][LEFT]].u + cellFlow[neighbourCell[faceId][RIGHT]].u) / 2.0;
  const double vF = (cellFlow[neighbourCell[faceId][LEFT]].v + cellFlow[neighbourCell[faceId][RIGHT]].v) / 2.0;

  viscflux[faceId][0] = 0.0;
  viscflux[faceId][1] = -(tauxx * normalVec[faceId][0] + tauxy * normalVec[faceId][1]) * area[faceId];
  viscflux[faceId][2] = -(tauxy * normalVec[faceId][0] + tauyy * normalVec[faceId][1]) * area[faceId];
  viscflux[faceId][3] = -((qx + uF * tauxx + vF * tauxy) * normalVec[faceId][0]
      + (qy + uF * tauxy + vF * tauyy) * normalVec[faceId][1]) * area[faceId];
}

/**
 * Calculates viscous fluxes at faces.
 */
void calculate_viscous_face_fluxes(
  const face_map *indx_f,
  int **neighbourCell,
  double ***connecVec,
  double **normalVec,
  double **grad_u,
  double **grad_v,
  double **grad_T,
  const double *area,
  const double Reinf,
  const double Prinf,
  const double Minf,
  const flow *cellFlow,
  const turb *saConst,
  double **viscflux
  )
{
  int i;
  double k1, k2, k3, muF, mutF, duF[2], dvF[2], dTF[2];
  double tauxx, tauxy, tauyy, qx, qy;

  k1 = 2.0 / (3.0*Reinf);
  k2 = 1.0 / Reinf;
  k3 = 1.0 / (Reinf*(GAMMA - 1.0)*pow(Minf, 2.0));

#pragma omp parallel for private(muF, mutF, duF, dvF, dTF, tauxx, tauxy, tauyy, qx, qy)
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    muF = (cellFlow[neighbourCell[i][LEFT]].mu + cellFlow[neighbourCell[i][RIGHT]].mu) / 2.0;
    mutF = (cellFlow[neighbourCell[i][LEFT]].mut + cellFlow[neighbourCell[i][RIGHT]].mut) / 2.0;
    gradients_at_inner(i, neighbourCell, grad_u, grad_v, grad_T, duF, dvF, dTF);
    tauxx = k1 * (muF + mutF) * (2.0 * duF[0] - dvF[1]);
    tauyy = k1 * (muF + mutF) * (2.0 * dvF[1] - duF[0]);
    tauxy = k2 * (muF + mutF) * (duF[1] + dvF[0]);
    qx = k3 * (muF / Prinf + mutF / saConst->Prturb) * dTF[0];
    qy = k3 * (muF / Prinf + mutF / saConst->Prturb) * dTF[1];
    calculate_viscous_flux(i, neighbourCell, normalVec, area, tauxx, tauxy, tauyy, qx, qy, cellFlow, viscflux);
  }
#pragma omp parallel for private(muF, mutF, duF, dvF, dTF, tauxx, tauxy, tauyy, qx, qy)
  for (i = indx_f->solid; i < indx_f->farfield; i++)
  {
    muF = (cellFlow[neighbourCell[i][LEFT]].mu + cellFlow[neighbourCell[i][RIGHT]].mu) / 2.0;
    mutF = (cellFlow[neighbourCell[i][LEFT]].mut + cellFlow[neighbourCell[i][RIGHT]].mut) / 2.0;
    gradients_at_solid(i, neighbourCell, cellFlow, connecVec, normalVec, grad_T, duF, dvF, dTF);
    tauxx = k1 * (muF + mutF) * (2.0 * duF[0] - dvF[1]);
    tauyy = k1 * (muF + mutF) * (2.0 * dvF[1] - duF[0]);
    tauxy = k2 * (muF + mutF) * (duF[1] + dvF[0]);
    qx = k3 * (muF / Prinf + mutF / saConst->Prturb) * dTF[0];
    qy = k3 * (muF / Prinf + mutF / saConst->Prturb) * dTF[1];
    calculate_viscous_flux(i, neighbourCell, normalVec, area, tauxx, tauxy, tauyy, qx, qy, cellFlow, viscflux);
  }
#pragma omp parallel for private(muF, mutF, duF, dvF, dTF, tauxx, tauxy, tauyy, qx, qy)
  for (i = indx_f->farfield; i < indx_f->final; i++)
  {
    muF = (cellFlow[neighbourCell[i][LEFT]].mu + cellFlow[neighbourCell[i][RIGHT]].mu) / 2.0;
    mutF = (cellFlow[neighbourCell[i][LEFT]].mut + cellFlow[neighbourCell[i][RIGHT]].mut) / 2.0;
    gradients_at_farfield(i, neighbourCell, grad_u, grad_v, grad_T, duF, dvF, dTF);
    tauxx = k1 * (muF + mutF) * (2.0 * duF[0] - dvF[1]);
    tauyy = k1 * (muF + mutF) * (2.0 * dvF[1] - duF[0]);
    tauxy = k2 * (muF + mutF) * (duF[1] + dvF[0]);
    qx = k3 * (muF / Prinf + mutF / saConst->Prturb) * dTF[0];
    qy = k3 * (muF / Prinf + mutF / saConst->Prturb) * dTF[1];
    calculate_viscous_flux(i, neighbourCell, normalVec, area, tauxx, tauxy, tauyy, qx, qy, cellFlow, viscflux);
  }
}


/**
 * Calculates spectral radii (at cells and faces).
 */
void calculate_spectral_radii(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double *area,
  double **normalVec,
  const flow *cellFlow,
  double *specRad,
  double *specRadFace
  )
{
  int i, j, left, right;
  double sos;

  /* initialize spectral radii */
#pragma omp parallel for
  for (i = 0; i < numberOfCells; i++)
    specRad[i] = 0.0;

    /* loop over faces */
#pragma omp parallel for private (sos, left, right)
  for (i = 0; i < indx_f->numberOfFaces; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    sos = (sound_speed(cellFlow[left].p, cellFlow[left].rho) + sound_speed(cellFlow[right].p, cellFlow[right].rho))
        / 2.0;
    specRadFace[i] = (sos
        + fabs(
          0.5 * (cellFlow[left].u + cellFlow[right].u) * normalVec[i][0]
              + 0.5 * (cellFlow[left].v + cellFlow[right].v) * normalVec[i][1] + normalVec[i][2])) * area[i];
  }

  /* work out cell spectral radii */
  for (i = indx_f->inner; i < indx_f->initial; i++){
    for (j = 0; j < 2; j++){
      specRad[neighbourCell[i][j]] += specRadFace[i];
    }
  }

  /* can NOT parallelize this loop because there can be (inner) faces with two boundary faces, i.e.
     at the corner of certain C-grids there could be a cell with two farfield faces */
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
    specRad[neighbourCell[i][RIGHT]] += specRadFace[i];
}


/**
 * Calculates unidivided laplacians of nu_bar at cells.
 *
 * @warning This function allocates dynamic memory for laplacians. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_undivided_laplacians_nub(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const flow *cellFlow,
  double **laplacian_nub
  )
{
  int i;
  double *lap_nub, lap_nub_f;

  /* alloc memory for laplacians and initialize to 0.0 */
  lap_nub = (double*)calloc(numberOfCells, sizeof(double));

  /* can NOT parallelize this loop because there can be (inner) faces with two boundary faces, i.e.
     at the corner of certain C-grids there could be a cell with two farfield faces */
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
    lap_nub[neighbourCell[i][RIGHT]] += (cellFlow[neighbourCell[i][LEFT]].mubar / cellFlow[neighbourCell[i][LEFT]].rho)
        - (cellFlow[neighbourCell[i][RIGHT]].mubar / cellFlow[neighbourCell[i][RIGHT]].rho);

  /* work out cell laplacians (inner face loop) */
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    lap_nub_f = (cellFlow[neighbourCell[i][LEFT]].mubar / cellFlow[neighbourCell[i][LEFT]].rho)
        - (cellFlow[neighbourCell[i][RIGHT]].mubar / cellFlow[neighbourCell[i][RIGHT]].rho);
    lap_nub[neighbourCell[i][RIGHT]] += lap_nub_f;
    lap_nub[neighbourCell[i][LEFT]] -= lap_nub_f;
  }

  (*laplacian_nub) = (double*)lap_nub;
}

/**
 * Calculates Runge-Kutta residuals.
 *
 * @warning This function allocates dynamic memory for RKflux. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
double **calculate_runge_kutta_residuals(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const int numberOfTerms,
  ...
  )
{
  int i, j, k, num_eqs = 4;
  double tflux;
  double **RKflux, ***terms;
  va_list args;

  // obtain pointer to terms
  terms = (double***)malloc(sizeof(double**)*numberOfTerms);
  va_start(args, numberOfTerms);
  for (i = 0 ; i < numberOfTerms; i++)
    terms[i] = (double**)va_arg(args, double**);
  va_end(args);

  // allocate dynamic memory for RKflux
  RKflux = (double**)malloc(sizeof(double*)*numberOfCells);
  for (i = 0; i < numberOfCells; i++)
    RKflux[i] = (double*) calloc(num_eqs, sizeof(double));

  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    for (j = 0; j < num_eqs; j++)
    {
      tflux = 0.0;
      for (k = 0; k < numberOfTerms; k++)
        tflux += terms[k][i][j];
      RKflux[neighbourCell[i][RIGHT]][j] += tflux;
      RKflux[neighbourCell[i][LEFT]][j] -= tflux;
    }
  }

  /* can NOT parallelize this loop because there can be (inner) faces with two boundary faces, i.e.
     at the corner of certain C-grids there could be a cell with two farfield faces */
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
  {
    for (j = 0; j < num_eqs; j++)
    {
      tflux = 0.0;
      for (k = 0; k < numberOfTerms; k++)
        tflux += terms[k][i][j];
      RKflux[neighbourCell[i][RIGHT]][j] += tflux;
    }
  }

  free(terms);

  return RKflux;
}

/**
 * Calculates Spalart-Allmaras residuals.
 */
void calculate_SA_residuals(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double *convection,
  const double *production,
  const double *destruction,
  const double *diffusion_v,
  const double *diffusion_s,
  double *SAflux
  )
{
  int i;
  double tflux;

#pragma omp parallel for
  for (i = 0; i < numberOfCells; i++)
    SAflux[i] = production[i] - destruction[i] + diffusion_v[i];

  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    tflux = convection[i] + diffusion_s[i];
    SAflux[neighbourCell[i][RIGHT]] += tflux;
    SAflux[neighbourCell[i][LEFT]] -= tflux;
  }

  /* can NOT parallelize this loop because there can be (inner) faces with two boundary faces, i.e.
     at the corner of certain C-grids there could be a cell with two farfield faces */
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
    SAflux[neighbourCell[i][RIGHT]] += convection[i] + diffusion_s[i];
}


/**
 * Works out the timestep size for Euler eqs. of motion.
 */
void calculate_timestep_Euler(
  const int numberOfCells,
  int **neighbourCell,
  const double CFL,
  const double *volume,
  const double *specRad,
  const face_map *indx_f,
  double *dt
  )
{
  int i;
  double *dt0, dtf;

  dt0 = (double*)malloc(sizeof(double)*numberOfCells);

#pragma omp parallel for
  for (i = 0; i < numberOfCells; i++)
  {
    /* INVISCID timestep */
    dt0[i] = CFL * volume[i] / specRad[i];
    dt[i] = 1.0e+5;
  }

  /* iterate only over inner faces */
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    dtf = min(dt0[neighbourCell[i][LEFT]], dt0[neighbourCell[i][RIGHT]]);
    dt[neighbourCell[i][LEFT]] = min(dt[neighbourCell[i][LEFT]], dtf);
    dt[neighbourCell[i][RIGHT]] = min(dt[neighbourCell[i][RIGHT]], dtf);
  }

  free(dt0);
}

/**
 * Works out the timestep size for RANS eqs. of motion.
 */
void calculate_timestep_RANS(
  const int numberOfCells,
  int **neighbourCell,
  const double CFL,
  const double Reinf,
  const double Prinf,
  const double *Psi,
  const double *volume,
  const double *area,
  const double *specRad,
  const face_map *indx_f,
  const flow *cellFlow,
  const turb *saConst,
  double *dt
  )
{
  int i;
  double *dt0, dtf, vsr;

  dt0 = (double*)malloc(sizeof(double)*numberOfCells);

#pragma omp parallel for private(i, dtf, vsr)
    for (i = 0; i < numberOfCells; i++)
    {
      /* INVISCID timestep */
      dt0[i] = CFL*volume[i] / specRad[i];
      /* VISCOUS timestep */
      vsr = (cellFlow[i].mu + cellFlow[i].mubar) / cellFlow[i].rho;
      dtf = CFL * Reinf * pow (volume[i], 2.0) / (vsr * Psi[i]);
      /* average viscous and inviscid timestep */
      dt0[i] = 1.0 / ((1.0 / dt0[i]) + (1.0 / dtf));
      dt[i] = 1.0e+5;
    }

  /* iterate only over inner faces */
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    dtf = min(dt0[neighbourCell[i][LEFT]], dt0[neighbourCell[i][RIGHT]]);
    dt[neighbourCell[i][LEFT]] = min(dt[neighbourCell[i][LEFT]], dtf);
    dt[neighbourCell[i][RIGHT]] = min(dt[neighbourCell[i][RIGHT]], dtf);
  }

  free(dt0);
}


/**
 * Works out volume contributions to SA eqn.
 *
 * @warning Must call this subroutine BEFORE `calculate_surface_contrib_SA()`
 */
void calculate_volume_contrib_SA(
  const int numberOfCells,
  const double Reinf,
  const double *volume,
  const double *wallDist,
  const double *lap_nub,
  double **grad_rho,
  double **grad_u,
  double **grad_v,
  double **grad_nub,
  const flow *cellFlow,
  const turb *saConst,
  double *production,
  double *destruction,
  double *diffusion_v
  )
{
  double chi, chi2, chi3, fv1, fv2, r0, g0, fw, d_2, ft2, absomega, omegabar, omegahat;

#pragma omp parallel for private(chi, chi2, chi3, fv1, fv2, r0, g0, fw, d_2, ft2, absomega, omegabar, omegahat)
  for (int i = 0; i < numberOfCells; i++)
  {
    chi = cellFlow[i].mubar / cellFlow[i].mu;
    chi = max(chi, EPS);
    chi2 = pow(chi, 2.0);
    chi3 = pow(chi, 3.0);
    fv1 = chi3 / (chi3 + saConst->cv1_3);
    fv2 = 1.0 - (chi / (1.0 + chi * fv1));
    d_2 = pow(wallDist[i], 2.0);
    absomega = fabs(grad_u[i][1] - grad_v[i][0]);
    omegahat = (cellFlow[i].mubar * fv2) / (Reinf * cellFlow[i].rho * saConst->k_2 * d_2);
    if (omegahat < -saConst->cv2 * absomega)
    {
      omegabar = absomega
          * (1.0
              + (pow(saConst->cv2, 2.0) * absomega + saConst->cv3 * omegahat)
                  / (absomega * (saConst->cv3 - 2.0 * saConst->cv2) - omegahat));
    }
    else
    {
      omegabar = absomega + omegahat;
    }
    r0 = cellFlow[i].mubar / (Reinf * cellFlow[i].rho * omegabar * saConst->k_2 * d_2);
    r0 = max(r0, EPS);
    r0 = min(r0, 10.0);
    g0 = r0 + saConst->cw2 * (pow(r0, 6.0) - r0);
    fw = g0 * pow((1.0 + saConst->cw3_6) / (pow(g0, 6.0) + saConst->cw3_6), 1.0 / 6.0);
    ft2 = saConst->ct3 * exp(-saConst->ct4 * chi2);
    production[i] = -(saConst->cb1 * (1.0 - ft2) * omegabar * cellFlow[i].mubar) * volume[i];
    destruction[i] = -((saConst->cw1 * fw - (saConst->cb1 * ft2) / saConst->k_2)
        * (pow(cellFlow[i].mubar, 2.0) / (cellFlow[i].rho * d_2)) / Reinf) * volume[i];
    diffusion_v[i] = (cellFlow[i].mu + cellFlow[i].mubar) * volume[i]
        * (saConst->cb2 * lap_nub[i]
            + (1.0 + saConst->cb2) * (grad_nub[i][0] * grad_rho[i][0] + grad_nub[i][1] * grad_rho[i][1])
                / cellFlow[i].rho) / (saConst->sigma * Reinf);
  }
}



/**
 * Works out surface contributions to SA eqn.
 *
 * @warning Must call this subroutine AFTER `calculate_volume_contrib_SA()`.
 */
void calculate_surface_contrib_SA(
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  double **grad_nub,
  const double Reinf,
  const flow *cellFlow,
  const turb *saConst,
  double *diffusion_s
  )
{
  double dnu_x_n, mu_mub;
  const double Kdiff = (1.0 + saConst->cb2) / (saConst->sigma*Reinf);

#pragma omp parallel for private(dnu_x_n, mu_mub)
  for (int i = indx_f->inner; i < indx_f->initial; i++)
  {
    dnu_x_n = ((grad_nub[neighbourCell[i][RIGHT]][0] + grad_nub[neighbourCell[i][LEFT]][0]) * normalVec[i][0]
        + (grad_nub[neighbourCell[i][RIGHT]][1] + grad_nub[neighbourCell[i][LEFT]][1]) * normalVec[i][1]) / 2.0;
    mu_mub = 0.5
        * ((cellFlow[neighbourCell[i][LEFT]].mu + cellFlow[neighbourCell[i][RIGHT]].mu)
            + (cellFlow[neighbourCell[i][LEFT]].mubar + cellFlow[neighbourCell[i][RIGHT]].mubar));
    diffusion_s[i] = -Kdiff * mu_mub * dnu_x_n * area[i];
  }

#pragma omp parallel for private(dnu_x_n, mu_mub)
  for (int i = indx_f->initial; i < indx_f->numberOfFaces; i++)
  {
    dnu_x_n = grad_nub[neighbourCell[i][RIGHT]][0] * normalVec[i][0]
        + grad_nub[neighbourCell[i][RIGHT]][1] * normalVec[i][1];
    mu_mub = 0.5
        * ((cellFlow[neighbourCell[i][LEFT]].mu + cellFlow[neighbourCell[i][RIGHT]].mu)
            + (cellFlow[neighbourCell[i][LEFT]].mubar + cellFlow[neighbourCell[i][RIGHT]].mubar));
    diffusion_s[i] = -Kdiff * mu_mub * dnu_x_n * area[i];
  }
}



/**
 * Works out convective contributions to SA eqn.
 *
 * @warning Must call this subroutine AFTER `calculate_volume_contrib_SA()`.
 */
void calculate_convective_SA(
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const flow *cellFlow,
  double *convection
  )
{
  double  Kcon, muuL, muuR, muvL, muvR;

  /* convective terms are evaluated using a FIRST ORDER UPWIND scheme */
#pragma omp parallel for private(Kcon, muuL, muuR, muvL, muvR)
  for (int i = 0; i < indx_f->numberOfFaces; i++)
  {
    Kcon =
        ((cellFlow[neighbourCell[i][RIGHT]].u + cellFlow[neighbourCell[i][LEFT]].u) * normalVec[i][0] / 2.0
            + (cellFlow[neighbourCell[i][RIGHT]].v + cellFlow[neighbourCell[i][LEFT]].v) * normalVec[i][1] / 2.0)
            / max(
              sqrt(pow((cellFlow[neighbourCell[i][RIGHT]].u + cellFlow[neighbourCell[i][LEFT]].u) / 2.0, 2.0) + pow((cellFlow[neighbourCell[i][RIGHT]].v + cellFlow[neighbourCell[i][LEFT]].v) / 2.0, 2.0)),
              1.0e-4);
    if (Kcon < 0.0)
    {
      Kcon = -pow(fabs(Kcon), 1.0 / 9.0);
    }
    else
    {
      Kcon = pow(Kcon, 1.0 / 9.0);
    }
    muuL = cellFlow[neighbourCell[i][LEFT]].mubar * cellFlow[neighbourCell[i][LEFT]].u;
    muuR = cellFlow[neighbourCell[i][RIGHT]].mubar * cellFlow[neighbourCell[i][RIGHT]].u;
    muvL = cellFlow[neighbourCell[i][LEFT]].mubar * cellFlow[neighbourCell[i][LEFT]].v;
    muvR = cellFlow[neighbourCell[i][RIGHT]].mubar * cellFlow[neighbourCell[i][RIGHT]].v;
    convection[i] = 0.5 * area[i]
        * (((1.0 + Kcon) * muuR + (1.0 - Kcon) * muuL) * normalVec[i][0]
            + ((1.0 + Kcon) * muvR + (1.0 - Kcon) * muvL) * normalVec[i][1]
            + ((1.0 + Kcon) * cellFlow[neighbourCell[i][RIGHT]].mubar
                + (1.0 - Kcon) * cellFlow[neighbourCell[i][LEFT]].mubar) * normalVec[i][2]);
  }
}


/**
 * Adds RK residual contribution to final step update.
 */
void add_stage_RKRes_contribution_to_update(const int numberOfCells, const double coeff, double **RKflux_stage,
  double**RKRes)
{
  const int num_eqs = 4;
  int i, j;

#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++)
    {
      for (j = 0; j < num_eqs; j++)
        RKRes[i][j] += coeff * RKflux_stage[i][j] / 6.0;
    }
  }
}


/**
 * Adds SA residual contribution to final step update.
 */
void add_stage_SARes_contribution_to_update(const int numberOfCells, const double coeff, double *SAflux_stage,
  double*SARes)
{
  int i;

#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      SARes[i] += coeff * SAflux_stage[i] / 6.0;
  }
}


/**
 * Releases heap memory at every stage in the Runge-Kutta iteration of Euler solver.
 */
void free_partial_memory_Euler(
  const int numberOfCells,
  double **RKflux,
  double **grad_rho,
  double **grad_u,
  double **grad_v,
  double **grad_p
  )
{
  int i;

  /* 2D arrays */
  for (i = 0; i < numberOfCells; i++)
  {
    free(RKflux[i]);
    free(grad_rho[i]);
    free(grad_u[i]);
    free(grad_v[i]);
    free(grad_p[i]);
  }

  free(RKflux);
  free(grad_rho);
  free(grad_u);
  free(grad_v);
  free(grad_p);
}


/**
 * Releases heap memory at every stage in the Runge-Kutta iteration of RANS solver.
 */
void free_partial_memory_RANS(
  const int numberOfCells,
  double **grad_T,
  double **grad_nub
  )
{
  int i;

  /* 2D arrays */
  for (i = 0; i < numberOfCells; i++)
  {
    free(grad_T[i]);
    free(grad_nub[i]);
  }

  free(grad_T);
  free(grad_nub);
}


/**
 * Calculates convergence residuals based on l2-norm of density.
 */
double calculate_convergence_residual(const int numberOfCells, const double totalVolume,
    const double *volume, const double *dt, const double *rho0, const flow *cellFlow)
{
  int i;
  double residual = 0.0;

  for (i = 0; i < numberOfCells; i++){
    residual += pow(volume[i] * (cellFlow[i].rho - rho0[i]) / dt[i], 2.0);
  }
  residual = sqrt(residual) / totalVolume;

  return residual;
}
