/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "main_headers.h"

/**
 * Calculates area, normalVector and faceCentre.
 */
void calculate_faces_3d(
  const int numberOfFaces,
  const int *numberOfFaceVertices,
  int **faceVert,
  double **normalVec,
  double ***faceCentre,
  double *area,
  const double *x,
  const double *y,
  const double *z
  );


/**
 * Work out cell centres.
 */
double **get_cell_centres_3d(
  const face_map *indx_f,
  const int numberOfNodes,
  const int numberOfCells,
  const int *numberOfFaceVertices,
  int **neighbourCell,
  int **faceVert,
  const double *x,
  const double *y,
  const double *z
  );



/**
 * Works out cell volumes.
 */
double *get_cell_volumes_3d(
  const face_map *indx_f,
  const int numberOfCells,
  int **neighbourCell,
  double ****connecVec,
  double **faceCentre,
  double **cellCentre,
  double **normalVec,
  const double *faceArea
  );


/**
 * Normal vectors must point towards left cell
 */
void orient_normal_vectors_3d(
  const face_map *indx_f,
  int **neighbourCell,
  double **cellCentre,
  double **faceCentre,
  double **normalVec
  );


/**
 * Gets initial 2D geometry from 3D mesh. In particular it calculates the following:
 *     (1) numberOfEdges
 *     (2) neighbourFace[][]
 *     (3) edgeVert[][]
 */
void get_initial_2d_geometry(
  const face_map *indx_f,
  face_map *indx_f0,
  const int numberOfNodes,
  int *numberOfEdges,
  const int *numberOfFaceVertices,
  int **faceVert,
  int ***neighbourFace,
  int ***edgeVert
  );

#endif
