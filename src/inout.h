/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef INOUT_H
#define INOUT_H

#include "main_headers.h"
#include "misc.h"


int text_len, leftpad, rightpad;

#define log_box_line(fp) do{fprintf(fp, "+%88s+\n","----------------------------------------------------------------------------------------");} while(0);
#define log_box_empty(fp) do{fprintf(fp, "|%88s|\n","");} while(0);
#define log_box_text(fp,text) do{text_len = strlen(text); leftpad = rightpad = (LINE_LEN - text_len - 2)/2; while (leftpad + text_len + rightpad < LINE_LEN - 2){rightpad++;} fprintf(fp, "|%*s%*s%*s|\n", leftpad, "", text_len, text, rightpad, "");} while(0);
#define log_blank_line(fp) do{fprintf(fp,"\n");} while(0);
#define log_setup_float(fp,name,value) do{fprintf(fp, "%47s : %-40lg\n", name, value);} while(0);
#define log_setup_exp(fp,name,value) do{fprintf(fp, "%47s : 1.0e%+-36lg\n", name, value);} while(0);
#define log_setup_int(fp,name,value) do{fprintf(fp, "%47s : %-40d\n", name, value);} while(0);
#define log_setup_text(fp,name,value) do{fprintf(fp, "%47s : %-40s\n", name, value);} while(0);


/*****************************************************************************/
/*                                                                           */
/*  print_mesh_statistics()       Prints out mesh statistics                 */
/*                                                                           */
/*****************************************************************************/

void print_mesh_statistics(
  FILE *fp,
  int numberOfNodes,
  int numberOfCells,
  face_map *indx_f
  );

/*****************************************************************************/
/*                                                                           */
/*  read_seed                           Reads solution from seeding file     */
/*                                                                           */
/*****************************************************************************/

flow *read_seed(
    int numberOfCells,
    int totalNumCells,
    char *filename
  );


/// Seeds initial solution with 2D inviscid isentropic vortex.
/**
 * @param numberOfCells Number of cells (faces) in the initial geometry.
 * @param totalNumCells Number of cells (faces) plus halo cells in the initial geometry. The number of halo cells is
 *   equal to the number of boundary edges.
 * @param firstInitialFace Index of first initial face in spacetime geometry.
 * @param numOfFaceVert Array of number of vertices per face in the initial geometry.
 * @param[in] faceVert Array of vertices indices per face in the spacetime geometry.
 * @param x Array of x-coordinates for vertices in spacetime mesh.
 * @param y Array of y-coordinates for vertices in spacetime mesh.
 * @param freeFlow Struct of type 'flow' with freestream properties.
 * @return initial spacetime solution with 2D inviscid isentropic vortex centred in the domain.
 */
flow *seed_inviscid_vortex
(
  const int numberOfCells,
  const int totalNumCells,
  const int firstInitialFace,
  const int *numOfFaceVert,
  int **faceVert,
  const double *x,
  const double *y,
  const flow* freeFlow
  );



/// Seeds initial solution with Sod / shock tube problem.
/**
 * @param numberOfCells Number of cells (faces) in the initial geometry.
 * @param totalNumCells Number of cells (faces) plus halo cells in the initial geometry. The number of halo cells is
 *   equal to the number of boundary edges.
 * @param firstInitialFace Index of first initial face in spacetime geometry.
 * @param numOfFaceVert Array of number of vertices per face in the initial geometry.
 * @param[in] faceVert Array of vertices indices per face in the spacetime geometry.
 * @param x Array of x-coordinates for vertices in spacetime mesh.
 * @param y Array of y-coordinates for vertices in spacetime mesh.
 * @param leftState Struct of type 'flow' with freestream properties on the left side.
 * @param leftState Struct of type 'flow' with freestream properties on the right side.
 * @return initial spacetime solution for Sod / shock tube problem (with discontinuity at centre).
 */
flow *seed_Sod_problem(const int numberOfCells, const int totalNumCells, const int firstInitialFace,
  const int *numOfFaceVert, int **faceVert, const double *x, const double *y, const flow *leftState,
  const flow *rightState);

/*****************************************************************************/
/*                                                                           */
/*  save_seed                             Saves solution to seeding file     */
/*                                                                           */
/*****************************************************************************/

void save_seed(
  int numberOfCells,
  char *filename,
  flow *cellFlow
  );


/*****************************************************************************/
/*                                                                           */
/*  save_3d_solution()                 Saves 3D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_3d_solution(
    const int numberOfNodes,
    const int numberOfCells,
    const int *numberOfFaceVertices,
    int **faceVert,
    const char *out_file,
    const double *x,
    const double *y,
    const double *t,
    const double *wallDist,
    const flow *Flow,
    const face_map *indx_f,
    int **neighbourCell
  );


void save_3d_Euler(
  const int numberOfNodes,
  const int numberOfCells,
  const int *numberOfFaceVertices,
  int **faceVert,
  const char *out_file,
  const double *x,
  const double *y,
  const double *t,
  const flow *Flow,
  const face_map *indx_f,
  int **neighbourCell
  );


void save_solid_surfaces_Euler(
  const int numberOfNodes,
  const int *numberOfFaceVertices,
  int **faceVert,
  const char *out_file,
  const double *x,
  const double *y,
  const double *t,
  const flow *Flow,
  const face_map *indx_f,
  int **neighbourCell
  );


/*****************************************************************************/
/*                                                                           */
/*  save_3d_solution_periodic()        Saves 3D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_3d_solution_periodic(
  int numberOfNodes,
  int numberOfCells,
  int *numberOfFaceVertices,
  int **faceVert,
  char *out_file,
  double *x,
  double *y,
  double *t,
  flow *Flow,
  face_map *indx_f,
  int **neighbourCell
  );

/*****************************************************************************/
/*                                                                           */
/*  save_2d_solution()                 Saves 2D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_2d_solution(
  int numberOfNodes,
  int **edgeVert,
  char *out_file,
  double *x,
  double *y,
  double *wallDist0,
  flow *iniFlow,
  face_map *indx_f0,
  face_map *indx_f,
  int **neighbourFace
  );


/*****************************************************************************/
/*                                                                           */
/*  save_3d_solution_QUAD()            Saves 3D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_3d_solution_QUAD(
  int numberOfNodes,
  int numberOfCells,
  int **cellVert,
  char *out_file,
  double *x,
  double *y,
  double *t,
  double *wallDist,
  flow *Flow
  );


/**
 * Saves 3D solution of QUADRILATERAL meshes to Tecplot file (Euler solution).
 */
void save_3d_solution_QUAD_Euler(
  int numberOfNodes,
  int numberOfCells,
  int **cellVert,
  char *out_file,
  double *x,
  double *y,
  double *t,
  flow *Flow
  );

/*****************************************************************************/
/*                                                                           */
/*  save_y_plus_QUAD()                   Saves y+ values to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_y_plus_QUAD(
    const int numberOfNodes,
    int **faceVert,
    int **neighbourCell,
    const char *out_file,
    const double Reinf,
    const double *x,
    const double *y,
    const double *t,
    const double *wallDist,
    double **normalVec,
    const flow *Flow,
    const face_map *indx_f
  );


/**
 * Gets 3D geometry from mesh file.
 */
void read_mesh_3d_ASCII(
  const char *mesh_file,
  int *numberOfNodes,
  int *numberOfFaces,
  int *numberOfCells,
  int *numberOfSolidTypes,
  int **solidTypes,
  int **numberOfFaceVertices,
  int ***faceVert,
  int ***neighbourCell,
  double ***normalVec,
  double **area,
  double **x,
  double **y,
  double **z,
  const int tcoor,
  face_map *indx_f
  );

#endif
