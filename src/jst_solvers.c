/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "jst.h"
#include "common.h"
#include "jst_solvers.h"

/**
 * Calculates scaling factors s2 and s4 at faces.
 *
 * @warning This function allocates dynamic memory for scaling factors. It is responsibility of
 *   the caller to release this memory when not needed anymore.
 */
static void calculate_scaling_factors(
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  double **scaling2,
  double **scaling4
  )
{
  int *numNeigh;
  double *s2, *s4;

  /* get number of neighbours per cell */
  numNeigh = (int*)calloc(numberOfCells, sizeof(int));
  s2 = (double*)malloc(sizeof(double)*(indx_f->numberOfFaces - indx_f->inner));
  s4 = (double*)malloc(sizeof(double)*(indx_f->numberOfFaces - indx_f->inner));

  for (int i = indx_f->inner; i < indx_f->initial; i++)
  {
    numNeigh[neighbourCell[i][LEFT]] += 1;
    numNeigh[neighbourCell[i][RIGHT]] += 1;
  }

//#pragma omp parallel for
    for (int i = indx_f->initial; i < indx_f->numberOfFaces; i++)
      numNeigh[neighbourCell[i][RIGHT]] += 1;

  /* get scaling factors */
//#pragma omp parallel for
    for (int i = indx_f->inner; i < indx_f->initial; i++)
    {
      s2[i] = 2.0 * (numNeigh[neighbourCell[i][RIGHT]] + numNeigh[neighbourCell[i][LEFT]])
          / (numNeigh[neighbourCell[i][RIGHT]] * numNeigh[neighbourCell[i][LEFT]]);
      s4[i] = pow(s2[i] / 2.0, 2.0);
    }

//#pragma omp parallel for
    for (int i = indx_f->initial; i < indx_f->numberOfFaces; i++)
    {
      s2[i] = 4.0*numNeigh[neighbourCell[i][RIGHT]] / pow(numNeigh[neighbourCell[i][RIGHT]], 2.0);
      s4[i] = pow(s2[i] / 2.0, 2.0);
    }

  free(numNeigh);
  (*scaling2) = s2;
  (*scaling4) = s4;
}


/**
 * Solver Euler-JST.
 */
int solver_Euler_jst(
  const int isave,
  const int maxiter,
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double eps_res,
  const double CFL,
  const double k2,
  const double k4,
  const double *area,
  const double *volume,
  double **normalVec,
  const flow *freeFlow,
  flow *cellFlow,
  FILE *fres
  )
{
  int i, info;
  const int num_eqs = 4; // continuity (1), momentum (2) and energy (1)
  static int iter = 1;
  time_t t0, t1;
  double *s2 = NULL, *s4 = NULL, *snorm = NULL;
  double **W = NULL;
  static double residual0 = 1.0;
  double residual, totalVolume;

  calculate_scaling_factors(numberOfCells, neighbourCell, indx_f, &s2, &s4);

  /**
   * Because we want to show the difference between the pure central-difference with respect to
   * upwinding, set snorm[] = 1.0 everywhere. It should yield more non-physical pressure waves
   * travelling backwards it time.
   */
  snorm = (double*)malloc(sizeof(double)*(indx_f->numberOfFaces - indx_f->inner));
  for (i = 0; i < indx_f->numberOfFaces; i++){
    //snorm[i] = sqrt(pow(normalVec[i][0], 2.0) + pow(normalVec[i][1], 2.0));
    snorm[i] = 1.0;
  }

  totalVolume = 0.0;
  for (i = 0; i < numberOfCells; i++)
    totalVolume += volume[i];

  /* initialize simulation */
  W = (double**)malloc(sizeof(double*)*numberOfCells);
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      W[i] = (double*)malloc(sizeof(double)*num_eqs);
  }
  calculate_conserved_vars(numberOfCells, cellFlow, W);

  if (iter > maxiter){
    info = ERR_MAXITER;
  }

  /* simulation */
  t0 = time(NULL);
  printf("       Iteration     log10(residual)\n");
  printf("      -----------   -----------------\n");
  for (i = iter; i < maxiter + 1; i++){
    residual = perform_update_Euler_jst(numberOfCells, neighbourCell, indx_f, totalVolume,
        CFL, k2, k4, area, volume, snorm, s2, s4, normalVec, W, freeFlow, cellFlow);
    if (iter == 1)
      residual0 = residual;
    residual /= residual0;
    fprintf(fres, "%d  %lg\n", iter++, residual);
    if (i == 1 || i % 10 == 0)
      printf("       %6d               %+.2lf\n", i, log10(residual));
    fflush(fres);
    if (isnan(residual) || isinf(residual)){
      info = ERR_RESIDUAL;
      break;
    }
    if (log10(residual) < eps_res){
      info = CONVERGED_SOLUTION;
      iter = 1;
      break;
    }
    t1 = time(NULL);
    if ((t1 - t0) / (60 * isave) > 0){
      t0 = t1;
      info = NEED_TO_SAVE;
      break;
    }
  }
  if (iter > maxiter){
    info = ERR_MAXITER;
    iter = 1;
  }

#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      free(W[i]);
  }
  free(W);
  free(s2);
  free(s4);
  free(snorm);

  return info;
}


/**
 * Solver RANS-JST.
 */
int solver_RANS_jst(
  const int isave,
  const int maxiter,
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double eps_res,
  const double Tref,
  const double Reinf,
  const double Prinf,
  const double CFL,
  const double k2,
  const double k4,
  const double *Psi,
  const double *area,
  const double *volume,
  const double *wallDist,
  double **normalVec,
  double **solidFaceVel,
  double ***connecVec,
  const flow *freeFlow,
  flow *cellFlow,
  const turb *saConst,
  FILE *fres
  )
{
  int i, info;
  const int num_eqs = 4; // continuity (1), momentum (2) and energy (1)
  static int iter = 1;
  time_t t0, t1;
  double *s2 = NULL, *s4 = NULL, *snorm = NULL;
  double **W = NULL;
  static double residual0 = 1.0;
  double residual, totalVolume;

  calculate_scaling_factors(numberOfCells, neighbourCell, indx_f, &s2, &s4);

  /**
   * Because we want to show the difference between the pure central-difference with respect to
   * upwinding, set snorm[] = 1.0 everywhere. It should yield more non-physical pressure waves
   * travelling backwards it time.
   */
  snorm = (double*)malloc(sizeof(double)*(indx_f->numberOfFaces - indx_f->inner));
  for (i = 0; i < indx_f->numberOfFaces; i++){
    //snorm[i] = sqrt(pow(normalVec[i][0], 2.0) + pow(normalVec[i][1], 2.0));
    snorm[i] = 1.0;
  }

  totalVolume = 0.0;
  for (i = 0; i < numberOfCells; i++)
    totalVolume += volume[i];

  /* initialize simulation */
  W = (double**)malloc(sizeof(double*)*numberOfCells);
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      W[i] = (double*)malloc(sizeof(double)*num_eqs);
  }
  calculate_conserved_vars(numberOfCells, cellFlow, W);

  if (iter > maxiter){
    info = ERR_MAXITER;
  }

  /* simulation */
  t0 = time(NULL);
  printf("       Iteration     log10(residual)\n");
  printf("      -----------   -----------------\n");
  for (i = iter; i < maxiter + 1; i++){
    residual = perform_update_RANS_jst(numberOfCells, neighbourCell, indx_f, totalVolume,
        Tref, Reinf, Prinf, CFL, k2, k4, Psi, area, volume, wallDist, snorm, s2, s4,
        normalVec, solidFaceVel, W, connecVec, freeFlow, saConst, cellFlow);
    if (iter == 1)
      residual0 = residual;
    residual /= residual0;
    fprintf(fres, "%d  %lg\n", iter++, residual);
    if (i == 1 || i % 10 == 0)
      printf("       %6d               %+.2lf\n", i, log10(residual));
    fflush(fres);
    if (isnan(residual) || isinf(residual)){
      info = ERR_RESIDUAL;
      break;
    }
    if (log10(residual) < eps_res){
      iter = 1;
      info = CONVERGED_SOLUTION;
      break;
    }
    t1 = time(NULL);
    if ((t1 - t0) / (60 * isave) > 0){
      t0 = t1;
      info = NEED_TO_SAVE;
      break;
    }
  }
  if (iter > maxiter){
    info = ERR_MAXITER;
    iter = 1;
  }

#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      free(W[i]);
  }
  free(W);
  free(s2);
  free(s4);
  free(snorm);

  return info;
}

