/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "inout.h"



static int get_solid_faces(const int numberOfNodes, const face_map *indx_f, const int *numFaceVert, int **faceVert,
  int **global2local, int **local2global)
{
  int numLocalNodes = 0;
  int temp = 0;
  int *local = (int*)calloc(numberOfNodes, sizeof(int));

  for (int face = indx_f->solid; face < indx_f->farfield; face++)
  {
    for (int v = 0; v < numFaceVert[face]; v++)
    {
      local[faceVert[face][v]]++;
      temp++;
    }
  }

  int *global = (int*)malloc(sizeof(int)*temp);
  for (int v = 0; v < numberOfNodes; v++)
  {
    if (local[v] > 0)
    {
      local[v] = numLocalNodes;
      global[numLocalNodes] = v;
      numLocalNodes++;
    }
    else
    {
      local[v] = -1;
    }
  }
  global = (int*)realloc(global, sizeof(int)*numLocalNodes);

  (*global2local) = local;
  (*local2global) = global;

  return numLocalNodes;
}


/*****************************************************************************/
/*                                                                           */
/*  print_mesh_statistics()       Prints out mesh statistics                 */
/*                                                                           */
/*****************************************************************************/

void print_mesh_statistics(
  FILE *fp,
  int numberOfNodes,
  int numberOfCells,
  face_map *indx_f
  )
{
  const int isInitial = indx_f->solid - indx_f->initial == 0 ? 1 : 0;

  log_box_line(fp);
  if (isInitial)
  {
    log_box_text(fp, "INITIAL MESH STATISTICS");
  }
  else
  {
    log_box_text(fp, "SPACETIME MESH STATISTICS");
  }
  log_box_line(fp);

  log_setup_int(fp, "Number of vertices (total)", numberOfNodes);
  log_setup_int(fp, "Number of faces (total)", indx_f->numberOfFaces);
  log_setup_int(fp, "* inner   ", indx_f->initial - indx_f->inner);
  if (!isInitial)
  {
    log_setup_int(fp, "* initial ", indx_f->solid - indx_f->initial);
    log_setup_int(fp, "* final   ", indx_f->numberOfFaces - indx_f->final);
  }
  log_setup_int(fp, "* solid   ", indx_f->farfield - indx_f->solid);
  log_setup_int(fp, "* farfield", indx_f->final - indx_f->farfield);
  log_setup_int(fp, "Number of cells (total)", numberOfCells);
  log_blank_line(fp);
  log_box_line(fp);
  log_box_text(fp, "SIMULATION LOG");
  log_box_line(fp);
}


/*****************************************************************************/
/*                                                                           */
/*  read_seed                           Reads solution from seeding file     */
/*                                                                           */
/*****************************************************************************/

flow *read_seed(
  int numberOfCells,
  int totalNumCells,
  char *filename
  )
{
  FILE *fp;
  int i, ncell;
  flow *Flow;

  fp = fopen(filename, "rb");
  if (!fp){
    error("Failed to open file \"%s\"", filename);
  }

  if (fread(&ncell, sizeof(int), 1, fp) != 1)
    error("failed to read number of cells in seed file \"%s\".", filename);
  if (ncell != numberOfCells){
    error("wrong number of cells in seeding file: \"%s\" (expected = %d, found = %d)", filename, numberOfCells, ncell);
  }

  Flow = (flow*)malloc(sizeof(flow)*totalNumCells);
  for (i = 0; i < numberOfCells; i++){
    if (fread(&Flow[i], sizeof(flow), 1, fp) != 1)
      error("failed to read flow variables in seed file \"%s\".", filename);
  }

  fclose(fp);

  return Flow;
}


/// Seeds initial solution with 2D inviscid isentropic vortex.
/**
 * @param numberOfCells Number of cells (faces) in the initial geometry.
 * @param totalNumCells Number of cells (faces) plus halo cells in the initial geometry. The number of halo cells is
 *   equal to the number of boundary edges.
 * @param firstInitialFace Index of first initial face in spacetime geometry.
 * @param numOfFaceVert Array of number of vertices per face in the initial geometry.
 * @param[in] faceVert Array of vertices indices per face in the spacetime geometry.
 * @param x Array of x-coordinates for vertices in spacetime mesh.
 * @param y Array of y-coordinates for vertices in spacetime mesh.
 * @param freeFlow Struct of type 'flow' with freestream properties.
 * @return initial spacetime solution with 2D inviscid isentropic vortex.
 */
flow *seed_inviscid_vortex
(
  const int numberOfCells,
  const int totalNumCells,
  const int firstInitialFace,
  const int *numOfFaceVert,
  int **faceVert,
  const double *x,
  const double *y,
  const flow* freeFlow
  )
{
  double xmin, xmax, ymin, ymax;
  double *x0, *y0;
  flow *Flow;

  // some parameters
  const double beta = 5.0;


  // work out cell centres
  x0 = (double*)calloc(numberOfCells, sizeof(double));
  y0 = (double*)calloc(numberOfCells, sizeof(double));
  xmin = ymin = 1.0e+100;
  xmax = ymax = -1.0e+100;
  for (int i = 0; i < numberOfCells; i++)
  {
    const int ii = firstInitialFace + i;
    for (int j = 0; j < numOfFaceVert[ii]; j++)
    {
      x0[i] += x[faceVert[ii][j]] / numOfFaceVert[ii];
      y0[i] += y[faceVert[ii][j]] / numOfFaceVert[ii];
      xmin = min(xmin, x[faceVert[ii][j]]);
      xmax = max(xmax, x[faceVert[ii][j]]);
      ymin = min(ymin, y[faceVert[ii][j]]);
      ymax = max(ymax, y[faceVert[ii][j]]);
    }
  }

  const double xc = xmin + 0.25*(xmax - xmin);
  const double yc = 0.5*(ymin+ymax);

  // initialize 2D isentropic inviscid vortex
  Flow = (flow*)malloc(sizeof(flow)*totalNumCells);
  for (int i = 0; i < numberOfCells; i++)
  {
    const double r = sqrt(pow(x0[i] - xc, 2.0) + pow(y0[i] - yc, 2.0));
    const double cv = beta*exp((1.0 - pow(r, 2.0)) / 2.0)/TWO_PI;
    const double dT = -(GAMMA - 1.0)*pow(beta, 2.0)*exp(1.0 - pow(r, 2.0)) / (8.0*GAMMA*pow(PI, 2.0));
    Flow[i].u = freeFlow->u - cv*(y0[i] - yc);
    Flow[i].v = freeFlow->v + cv*(x0[i] - xc);
    Flow[i].T = 1.0 + dT;
    Flow[i].rho = pow(Flow[i].T, 1.0/(GAMMA - 1.0));
    Flow[i].p = Flow[i].rho*Flow[i].T;
    Flow[i].e = (Flow[i].p / ((GAMMA - 1.0)*Flow[i].rho)) + ((pow(Flow[i].u, 2.0) + pow(Flow[i].v, 2.0)) / 2.0);
  }

  return Flow;
}


/// Seeds initial solution with Sod / shock tube problem.
/**
 * @param numberOfCells Number of cells (faces) in the initial geometry.
 * @param totalNumCells Number of cells (faces) plus halo cells in the initial geometry. The number of halo cells is
 *   equal to the number of boundary edges.
 * @param firstInitialFace Index of first initial face in spacetime geometry.
 * @param numOfFaceVert Array of number of vertices per face in the initial geometry.
 * @param[in] faceVert Array of vertices indices per face in the spacetime geometry.
 * @param x Array of x-coordinates for vertices in spacetime mesh.
 * @param y Array of y-coordinates for vertices in spacetime mesh.
 * @param leftState Struct of type 'flow' with freestream properties on the left side.
 * @param leftState Struct of type 'flow' with freestream properties on the right side.
 * @return initial spacetime solution for Sod / shock tube problem (with discontinuity at centre).
 */
flow *seed_Sod_problem(const int numberOfCells, const int totalNumCells, const int firstInitialFace,
  const int *numOfFaceVert, int **faceVert, const double *x, const double *y, const flow *leftState,
  const flow *rightState)
{
  double *x0, xmin, xmax;
  flow *Flow;

  // work out cell centres
  x0 = (double*)calloc(numberOfCells, sizeof(double));
  xmin = 1.0e+100;
  xmax = -1.0e+100;
  for (int i = 0; i < numberOfCells; i++)
  {
    const int ii = firstInitialFace + i;
    for (int j = 0; j < numOfFaceVert[ii]; j++)
    {
      x0[i] += x[faceVert[ii][j]] / numOfFaceVert[ii];
      xmin = min(xmin, x[faceVert[ii][j]]);
      xmax = max(xmax, x[faceVert[ii][j]]);
    }
  }
  const double xshock = 0.5*(xmax+xmin);

  // initialize Sod / shock tube problem
  Flow = (flow*)malloc(sizeof(flow)*totalNumCells);
  for (int i = 0; i < numberOfCells; i++)
  {
    if (x0[i] < xshock)
    {
      // left state
      Flow[i].rho = leftState->rho;
      Flow[i].u = leftState->u;
      Flow[i].v = leftState->v;
      Flow[i].p = leftState->p;
      Flow[i].e = (Flow[i].p / ((GAMMA - 1.0)*Flow[i].rho)) + ((pow(Flow[i].u, 2.0) + pow(Flow[i].v, 2.0)) / 2.0);
    }
    else
    {
      // right state
      Flow[i].rho = rightState->rho;
      Flow[i].u = rightState->u;
      Flow[i].v = rightState->v;
      Flow[i].p = rightState->p;
      Flow[i].e = (Flow[i].p / ((GAMMA - 1.0)*Flow[i].rho)) + ((pow(Flow[i].u, 2.0) + pow(Flow[i].v, 2.0)) / 2.0);
    }
  }

  return Flow;
}

/*****************************************************************************/
/*                                                                           */
/*  save_seed                             Saves solution to seeding file     */
/*                                                                           */
/*****************************************************************************/

void save_seed(
  int numberOfCells,
  char *filename,
  flow *cellFlow
  )
{
  FILE *fp;
  int i;

  fp = fopen(filename, "wb");
  if (!fp){
    error("Failed to create file \"%s\"", filename);
  }
  fwrite(&numberOfCells, sizeof(int), 1, fp);
  for (i = 0; i < numberOfCells; i++){
    fwrite(&cellFlow[i], sizeof(flow), 1, fp);
  }

  fclose(fp);

}


/*****************************************************************************/
/*                                                                           */
/*    flush_primVars()                                                       */
/*                                                                           */
/*****************************************************************************/

static void flush_primVars(
  FILE *fp,
  const int numberOfValues,
  const flow *Flow
  )
{
  int i, j;

  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].p);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].rho);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].u);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].v);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].mu);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].mubar);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
}

/**
 * Flush primvars Euler (p, rho, u, v).
 */
static void flush_primVars_Euler(
  FILE *fp,
  const int numberOfValues,
  const flow *Flow
  )
{
  int i, j;

  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].p);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].rho);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].u);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = 0; j < numberOfValues; j++){
    fprintf(fp, "%lg ", Flow[j].v);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
}

/**
 * Flush primvars Euler (p, rho, u, v).
 */
static void flush_primVars_Euler_boundary(FILE *fp, const int index0, const int index1, int **neighbourCell,
  const flow *Flow)
{
  int i, j;

  i = 0;
  for (j = index0; j < index1; j++){
    fprintf(fp, "%lg ", Flow[neighbourCell[j][RIGHT]].p);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = index0; j < index1; j++){
    fprintf(fp, "%lg ", Flow[neighbourCell[j][RIGHT]].rho);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = index0; j < index1; j++){
    fprintf(fp, "%lg ", Flow[neighbourCell[j][RIGHT]].u);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (j = index0; j < index1; j++){
    fprintf(fp, "%lg ", Flow[neighbourCell[j][RIGHT]].v);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
}

/*****************************************************************************/
/*                                                                           */
/*  save_3d_solution()                 Saves 3D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_3d_solution(
  const int numberOfNodes,
  const int numberOfCells,
  const int *numberOfFaceVertices,
  int **faceVert,
  const char *out_file,
  const double *x,
  const double *y,
  const double *t,
  const double *wallDist,
  const flow *Flow,
  const face_map *indx_f,
  int **neighbourCell
  )
{
  FILE *fp;
  int k, i, j, totalNumFaceNodes;
  char fname[FILENAME_MAX];

  strcpy(fname, out_file);
  strcat(fname, ".plt");
  fp = fopen(fname, "w");
  if (!fp){
    error("Failed to open file \"%s\"", fname);
  }

  totalNumFaceNodes = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    totalNumFaceNodes += numberOfFaceVertices[i];
  }
  /* write header */
  fprintf(fp, "TITLE = \"2D Spacetime solution (2D + Time)\", FILETYPE = FULL\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"t\" \"p\" \"rho\" \"u\" \"v\" \"mu\" \"mubar\" \"wallDist\"\n");
  fprintf(fp, "ZONE T=\"Spacetime solution\"\n");
  fprintf(fp, "NODES=%d, FACES=%d, ELEMENTS=%d, DATAPACKING=BLOCK, ZONETYPE=FEPOLYHEDRON\n", numberOfNodes, indx_f->numberOfFaces, numberOfCells);
  fprintf(fp, "TOTALNUMFACENODES=%d, NUMCONNECTEDBOUNDARYFACES=%d, TOTALNUMBOUNDARYCONNECTIONS=%d\n", totalNumFaceNodes, 0, 0);
  fprintf(fp, "VARLOCATION=([1,2,3]=NODAL,[4,5,6,7,8,9,10]=CELLCENTERED)\n");

  /* save coordinates */
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", x[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", y[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", t[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write solution at each cell */
  flush_primVars(fp, numberOfCells, Flow);

  i = 0;
  for (j = 0; j < numberOfCells; j++){
    fprintf(fp, "%lg ", wallDist[j]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write vertices per face */
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", numberOfFaceVertices[i]);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      fprintf(fp, "%d ", faceVert[i][j] + 1);
      k++;
    }
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write left and right neighbours at each face */
  k = 0;
  for (i = indx_f->inner; i < indx_f->initial; i++){
    fprintf(fp, "%d ", neighbourCell[i][LEFT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", 0);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", neighbourCell[i][RIGHT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);
}


void save_3d_Euler(
  const int numberOfNodes,
  const int numberOfCells,
  const int *numberOfFaceVertices,
  int **faceVert,
  const char *out_file,
  const double *x,
  const double *y,
  const double *t,
  const flow *Flow,
  const face_map *indx_f,
  int **neighbourCell
  )
{
  FILE *fp;
  int k, i, j, totalNumFaceNodes;
  char fname[FILENAME_MAX];

  strcpy(fname, out_file);
  strcat(fname, ".plt");
  fp = fopen(fname, "w");
  if (!fp){
    error("Failed to open file \"%s\"", fname);
  }

  totalNumFaceNodes = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    totalNumFaceNodes += numberOfFaceVertices[i];
  }
  /* write header */
  fprintf(fp, "TITLE = \"2D Spacetime solution (2D + Time)\", FILETYPE = FULL\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"t\" \"p\" \"rho\" \"u\" \"v\"\n");
  fprintf(fp, "ZONE T=\"Spacetime solution\"\n");
  fprintf(fp, "NODES=%d, FACES=%d, ELEMENTS=%d, DATAPACKING=BLOCK, ZONETYPE=FEPOLYHEDRON\n", numberOfNodes, indx_f->numberOfFaces, numberOfCells);
  fprintf(fp, "TOTALNUMFACENODES=%d, NUMCONNECTEDBOUNDARYFACES=%d, TOTALNUMBOUNDARYCONNECTIONS=%d\n", totalNumFaceNodes, 0, 0);
  fprintf(fp, "VARLOCATION=([1,2,3]=NODAL,[4,5,6,7]=CELLCENTERED)\n");

  /* save coordinates */
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", x[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", y[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", t[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write solution at each cell */
  flush_primVars_Euler(fp, numberOfCells, Flow);

  /* write vertices per face */
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", numberOfFaceVertices[i]);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      fprintf(fp, "%d ", faceVert[i][j] + 1);
      k++;
    }
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write left and right neighbours at each face */
  k = 0;
  for (i = indx_f->inner; i < indx_f->initial; i++){
    fprintf(fp, "%d ", neighbourCell[i][LEFT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", 0);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", neighbourCell[i][RIGHT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);
}



void save_solid_surfaces_Euler(
  const int numberOfNodes,
  const int *numberOfFaceVertices,
  int **faceVert,
  const char *out_file,
  const double *x,
  const double *y,
  const double *t,
  const flow *Flow,
  const face_map *indx_f,
  int **neighbourCell
  )
{
  FILE *fp;
  int k, i, j;
  int *global2local, *local2global;
  char fname[FILENAME_MAX];

  strcpy(fname, out_file);
  strcat(fname, ".surf.plt");
  fp = fopen(fname, "w");
  if (!fp){
    error("Failed to create file \"%s\"", fname);
  }

  int numNodes = get_solid_faces(numberOfNodes, indx_f, numberOfFaceVertices, faceVert, &global2local, &local2global);

  /* write header */
  fprintf(fp, "TITLE = \"Solid surface solution (2D + Time)\", FILETYPE = FULL\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"t\" \"p\" \"rho\" \"u\" \"v\"\n");
  fprintf(fp, "ZONE T=\"Solid surfaces\"\n");
  fprintf(fp, "NODES=%d, ELEMENTS=%d, DATAPACKING=BLOCK, ZONETYPE=FEQUADRILATERAL\n", numNodes,
    indx_f->farfield - indx_f->solid);
  fprintf(fp, "VARLOCATION=([1,2,3]=NODAL,[4,5,6,7]=CELLCENTERED)\n");

  /* save coordinates */
  i = 0;
  for (k = 0; k < numNodes; k++){
    fprintf(fp, "%lg ", x[local2global[k]]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numNodes; k++){
    fprintf(fp, "%lg ", y[local2global[k]]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numNodes; k++){
    fprintf(fp, "%lg ", t[local2global[k]]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write solution at each cell */
  flush_primVars_Euler_boundary(fp, indx_f->solid, indx_f->farfield, neighbourCell, Flow);

  /* write vertices per face */
  k = 0;
  for (i = indx_f->solid; i < indx_f->farfield; i++){
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      fprintf(fp, "%d ", global2local[faceVert[i][j]] + 1);
      k++;
    }
    if (numberOfFaceVertices[i] == 3)
    {
      fprintf(fp, "%d ", global2local[faceVert[i][2]] + 1);
      k++;
    }
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);

  free(local2global);
  free(global2local);
}





/*****************************************************************************/
/*                                                                           */
/*  save_3d_solution_periodic()        Saves 3D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_3d_solution_periodic(
  int numberOfNodes,
  int numberOfCells,
  int *numberOfFaceVertices,
  int **faceVert,
  char *out_file,
  double *x,
  double *y,
  double *t,
  flow *Flow,
  face_map *indx_f,
  int **neighbourCell
  )
{
  FILE *fp;
  int k, i, j, totalNumFaceNodes;
  char fname[FILENAME_MAX];

  strcpy(fname, out_file);
  strcat(fname, ".plt");
  fp = fopen(fname, "w");
  if (!fp){
    error("Failed to open file \"%s\"", fname);
  }

  totalNumFaceNodes = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    totalNumFaceNodes += numberOfFaceVertices[i];
  }
  /* write header */
  fprintf(fp, "TITLE = \"2D Spacetime solution (2D + Time)\", FILETYPE = FULL\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"t\" \"p\" \"rho\" \"u\" \"v\"\n");
  fprintf(fp, "ZONE T=\"Spacetime solution\"\n");
  fprintf(fp, "NODES=%d, FACES=%d, ELEMENTS=%d, DATAPACKING=BLOCK, ZONETYPE=FEPOLYHEDRON\n", numberOfNodes, indx_f->numberOfFaces, numberOfCells);
  fprintf(fp, "TOTALNUMFACENODES=%d, NUMCONNECTEDBOUNDARYFACES=%d, TOTALNUMBOUNDARYCONNECTIONS=%d\n", totalNumFaceNodes, 0, 0);
  fprintf(fp, "VARLOCATION=([1,2,3]=NODAL,[4,5,6,7]=CELLCENTERED)\n");

  /* save coordinates */
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", x[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", y[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", t[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write solution at each cell */
  flush_primVars(fp, numberOfCells, Flow);

  /* write vertices per face */
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", numberOfFaceVertices[i]);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      fprintf(fp, "%d ", faceVert[i][j] + 1);
      k++;
    }
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write left and right neighbours at each face */
  k = 0;
  for (i = indx_f->inner; i < indx_f->initial; i++){
    fprintf(fp, "%d ", neighbourCell[i][LEFT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", 0);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  k = 0;
  for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
    fprintf(fp, "%d ", neighbourCell[i][RIGHT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);
}


/*****************************************************************************/
/*                                                                           */
/*  save_2d_solution()                 Saves 2D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_2d_solution(
  int numberOfNodes,
  int **edgeVert,
  char *out_file,
  double *x,
  double *y,
  double *wallDist0,
  flow *iniFlow,
  face_map *indx_f0,
  face_map *indx_f,
  int **neighbourFace
  )
{
  FILE *fp;
  int k, i, j, numberOfFaces;

  fp = fopen(out_file, "w");
  if (!fp){
    error("Failed to open file \"%s\"", out_file);
  }
  numberOfFaces = indx_f->solid - indx_f->initial;

  /* write header */
  fprintf(fp, "TITLE = \"Initial solution\", FILETYPE = FULL\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"p\" \"rho\" \"u\" \"v\" \"mu\" \"mubar\" \"dist\"\n");
  fprintf(fp, "ZONE T=\"Fluid domain\"\n");
  fprintf(fp, "NODES=%d, FACES=%d, ELEMENTS=%d, DATAPACKING=BLOCK, ZONETYPE=FEPOLYGON\n", numberOfNodes, indx_f0->numberOfFaces, numberOfFaces);
  fprintf(fp, "NUMCONNECTEDBOUNDARYFACES=%d, TOTALNUMBOUNDARYCONNECTIONS=%d\n", 0, 0);
  fprintf(fp, "VARLOCATION=([1,2]=NODAL,[3,4,5,6,7,8,9]=CELLCENTERED)\n");

  /* save coordinates */
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", x[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", y[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write solution at each cell */
  flush_primVars(fp, numberOfFaces, iniFlow);

  k = 0;
  for (j = 0; j < numberOfFaces; j++){
    fprintf(fp, "%lg ", wallDist0[j]);
    k++;
    if (k >= 25){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write vertices per edge */
  k = 0;
  for (i = indx_f0->inner; i < indx_f0->numberOfFaces; i++){
    for (j = 0; j < 2; j++){
      fprintf(fp, "%d ", edgeVert[i][j] + 1);
      k++;
    }
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* write left and right neighbours at each face */
  k = 0;
  for (i = indx_f0->inner; i < indx_f0->initial; i++){
    fprintf(fp, "%d ", neighbourFace[i][LEFT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  k = 0;
  for (i = indx_f0->initial; i < indx_f0->numberOfFaces; i++){
    fprintf(fp, "%d ", 0);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }
  k = 0;
  for (i = indx_f0->inner; i < indx_f0->numberOfFaces; i++){
    fprintf(fp, "%d ", neighbourFace[i][RIGHT] + 1);
    k++;
    if (k >= 30){
      k = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);
}


/*****************************************************************************/
/*                                                                           */
/*  save_3d_solution_QUAD()            Saves 3D solution to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_3d_solution_QUAD(
  int numberOfNodes,
  int numberOfCells,
  int **cellVert,
  char *out_file,
  double *x,
  double *y,
  double *t,
  double *wallDist,
  flow *Flow
  )
{
  FILE *fp;
  char fname[FILENAME_MAX];
  int k, i;

  strcpy(fname, out_file);
  strcat(fname, ".plt");
  fp = fopen(fname, "w");
  if (!fp){
    error("Failed to open file \"%s\"", fname);
  }

  fprintf(fp, "TITLE = \"2D spacetime solution\"\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"t\" \"p\" \"rho\" \"u\" \"v\" \"mu\" \"mu_t\" \"wallDist\"\n");
  fprintf(fp, "ZONE T=\"Fluid solution\"\n");
  fprintf(fp, "N=%d, E=%d, ET=BRICK, F=FEBLOCK\n", numberOfNodes, numberOfCells);
  fprintf(fp, "VARLOCATION=([1,2,3]=NODAL,[4,5,6,7,8,9,10]=CELLCENTERED)\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", x[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", y[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", t[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  flush_primVars(fp, numberOfCells, Flow);
  i = 0;
  for (k = 0; k < numberOfCells; k++){
    fprintf(fp, "%lg ", wallDist[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  i = 0;
  for (k = 0; k < numberOfCells; k++){
    fprintf(fp, "%d %d %d %d %d %d %d %d ", cellVert[k][0] + 1, cellVert[k][1] + 1, cellVert[k][2] + 1, cellVert[k][3] + 1, cellVert[k][4] + 1, cellVert[k][5] + 1, cellVert[k][6] + 1, cellVert[k][7] + 1);
    i++;
    if (i >= 5){
      i = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);

}



/**
 * Saves 3D solution of QUADRILATERAL meshes to Tecplot file (Euler solution).
 */
void save_3d_solution_QUAD_Euler(
  int numberOfNodes,
  int numberOfCells,
  int **cellVert,
  char *out_file,
  double *x,
  double *y,
  double *t,
  flow *Flow
  )
{
  FILE *fp;
  char fname[FILENAME_MAX];
  int k, i;

  strcpy(fname, out_file);
  strcat(fname, ".plt");
  fp = fopen(fname, "w");
  if (!fp){
    error("Failed to open file \"%s\"", fname);
  }

  fprintf(fp, "TITLE = \"2D spacetime solution\"\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"t\" \"p\" \"rho\" \"u\" \"v\"\n");
  fprintf(fp, "ZONE T=\"Fluid solution\"\n");
  fprintf(fp, "N=%d, E=%d, ET=BRICK, F=FEBLOCK\n", numberOfNodes, numberOfCells);
  fprintf(fp, "VARLOCATION=([1,2,3]=NODAL,[4,5,6,7]=CELLCENTERED)\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", x[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", y[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", t[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  flush_primVars_Euler(fp, numberOfCells, Flow);

  i = 0;
  for (k = 0; k < numberOfCells; k++){
    fprintf(fp, "%d %d %d %d %d %d %d %d ", cellVert[k][0] + 1, cellVert[k][1] + 1, cellVert[k][2] + 1, cellVert[k][3] + 1, cellVert[k][4] + 1, cellVert[k][5] + 1, cellVert[k][6] + 1, cellVert[k][7] + 1);
    i++;
    if (i >= 5){
      i = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);
}


/*****************************************************************************/
/*                                                                           */
/*  save_y_plus_QUAD()                   Saves y+ values to tecplot file     */
/*                                                                           */
/*****************************************************************************/

void save_y_plus_QUAD(
  const int numberOfNodes,
  int **faceVert,
  int **neighbourCell,
  const char *out_file,
  const double Reinf,
  const double *x,
  const double *y,
  const double *t,
  const double *wallDist,
  double **normalVec,
  const flow *Flow,
  const face_map *indx_f
  )
{
  FILE *fp;
  char fname[FILENAME_MAX];
  int k, i;
  double y_plus, u_abs;

  strcpy(fname, out_file);
  strcat(fname, ".yplus.plt");
  fp = fopen(fname, "w");
  if (!fp){
    error("Failed to open file \"%s\"", fname);
  }

  fprintf(fp, "TITLE = \"y_plus\"\n");
  fprintf(fp, "VARIABLES = \"x\" \"y\" \"t\" \"y_plus\"\n");
  fprintf(fp, "ZONE T=\"y_plus\"\n");
  fprintf(fp, "N=%d, E=%d, ET=QUADRILATERAL, F=FEBLOCK\n", numberOfNodes, indx_f->farfield - indx_f->solid);
  fprintf(fp, "VARLOCATION=([1,2,3]=NODAL,[4]=CELLCENTERED)\n");

  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", x[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", y[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");
  i = 0;
  for (k = 0; k < numberOfNodes; k++){
    fprintf(fp, "%lg ", t[k]);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  /* calculation of y+ */
  i = 0;
  for (k = indx_f->solid; k < indx_f->farfield; k++){
    u_abs = fabs(Flow[neighbourCell[k][RIGHT]].u*normalVec[k][1] - Flow[neighbourCell[k][RIGHT]].v*normalVec[k][0]);
    y_plus = 2.0*sqrt(Reinf)*sqrt(Flow[neighbourCell[k][RIGHT]].rho*u_abs*wallDist[neighbourCell[k][RIGHT]] / Flow[neighbourCell[k][RIGHT]].mu);
    fprintf(fp, "%lg ", y_plus);
    i++;
    if (i >= 25){
      i = 0;
      fprintf(fp, "\n");
    }
  }
  fprintf(fp, "\n");

  i = 0;
  for (k = indx_f->solid; k < indx_f->farfield; k++){
    fprintf(fp, "%d %d %d %d ", faceVert[k][0] + 1, faceVert[k][1] + 1, faceVert[k][2] + 1, faceVert[k][3] + 1);
    i++;
    if (i >= 11){
      i = 0;
      fprintf(fp, "\n");
    }
  }

  fclose(fp);

}


/**
 * Gets 3D geometry from mesh file.
 */
void read_mesh_3d_ASCII(
  const char *mesh_file,
  int *numberOfNodes,
  int *numberOfFaces,
  int *numberOfCells,
  int *numberOfSolidTypes,
  int **solidTypes,
  int **numberOfFaceVertices,
  int ***faceVert,
  int ***neighbourCell,
  double ***normalVec,
  double **area,
  double **x,
  double **y,
  double **z,
  const int tcoor,
  face_map *indx_f
  )
{
  FILE *fp;
  int i, j, k, l, nf[5], maxCellIndex;
  int **neigh, *bmk, **vert, *numFaceVert, *nsol, numFace, numSolidTypes, oldSolidMarker;
  double t_min;

  fp = fopen(mesh_file, "r");
  if (!fp){
    error("Failed to open file \"%s\"", mesh_file);
  }

  if (fscanf(fp, "%d %d", numberOfNodes, &numFace) != 2)
    error("failed to read number of nodes and faces");

  (*x) = (double*)malloc(sizeof(double)*(*numberOfNodes));
  (*y) = (double*)malloc(sizeof(double)*(*numberOfNodes));
  (*z) = (double*)malloc(sizeof(double)*(*numberOfNodes));

  t_min = 1.0e+100;
  if (tcoor == 1){
    for (i = 0; i<(*numberOfNodes); i++){
      if (fscanf(fp, "%lg %lg %lg", &(*z)[i], &(*x)[i], &(*y)[i]) != 3)
        error("failed to read t, y and z coordinates");
      t_min = t_min > (*z)[i] ? (*z)[i] : t_min;
    }
  }
  else if (tcoor == 2){
    for (i = 0; i<(*numberOfNodes); i++){
      if (fscanf(fp, "%lg %lg %lg", &(*x)[i], &(*z)[i], &(*y)[i]) != 3)
        error("failed to read x, t and z coordinates");
      t_min = t_min > (*z)[i] ? (*z)[i] : t_min;
    }
  }
  else if (tcoor == 3){
    for (i = 0; i<(*numberOfNodes); i++){
      if (fscanf(fp, "%lg %lg %lg", &(*x)[i], &(*y)[i], &(*z)[i]) != 3)
        error("failed to read x, y and t coordinates");
      t_min = t_min > (*z)[i] ? (*z)[i] : t_min;
    }
  }

  vert = (int**)malloc(sizeof(int*)*numFace);
  neigh = (int**)malloc(sizeof(int*)*numFace);
  for (i = 0; i < numFace; i++){
    neigh[i] = (int*)malloc(sizeof(int)* 2);
  }
  bmk = (int*)malloc(sizeof(int)*numFace);
  numFaceVert = (int*)malloc(sizeof(int)*numFace);

  maxCellIndex = 0;
  for (j = 0; j < 5; j++){
    nf[j] = 0;
  }

  numSolidTypes = oldSolidMarker = 0;
  for (i = 0; i < numFace; i++){
    if (fscanf(fp, "%d ", &numFaceVert[i]) != 1)
      error("failed to read number of vertices per face");
    vert[i] = (int*)malloc(sizeof(int)* numFaceVert[i]);
    for (j = 0; j < numFaceVert[i]; j++){
      if (fscanf(fp, "%d ", &vert[i][j]) != 1)
        error("failed to read vertices at face");
    }

    if (fscanf(fp, "%d %d %d", &neigh[i][LEFT], &neigh[i][RIGHT], &bmk[i]) != 3)
      error("failed to read neighbours and boundary marker at face");
    if (bmk[i] == BMK_SOLID){
      oldSolidMarker++;
    }
    if (bmk[i] >= BMK_SOLID_START && bmk[i] < BMK_SOLID_START + BMK_SOLID_MAX_TYPES){
      if (bmk[i] > numSolidTypes){
        numSolidTypes = bmk[i];
      }
    }
    for (j = 0; j < 2; j++){
      if (neigh[i][j]>maxCellIndex){
        maxCellIndex = neigh[i][j];
      }
    }

    if (bmk[i] == BMK_INITIAL && fabs((*z)[vert[i][0]] - t_min) > 1.0e-6){
      bmk[i] = BMK_FINAL;
    }

    switch (bmk[i]){
    case BMK_INNER:
      nf[BMK_INNER]++;
      break;
    case BMK_SOLID:
      if (neigh[i][RIGHT] < 0 || neigh[i][LEFT] < 0){
        nf[BMK_SOLID]++;
      }
      else {
        nf[BMK_SOLID]++;
        nf[BMK_SOLID]++;
      }
      break;
    case BMK_FARFIELD:
      nf[BMK_FARFIELD]++;
      break;
    case BMK_INITIAL:
      nf[BMK_INITIAL]++;
      break;
    case BMK_FINAL:
      nf[BMK_FINAL]++;
      break;
    default:
      if (bmk[i] >= BMK_SOLID_START && bmk[i] < BMK_SOLID_START + BMK_SOLID_MAX_TYPES){
          if (neigh[i][RIGHT] < 0 || neigh[i][LEFT] < 0){
          nf[BMK_SOLID]++;
        }
        else {
          nf[BMK_SOLID]++;
          nf[BMK_SOLID]++;
        }
      }
      else {
        error("Unknown boundary marker -> %d", bmk[i]);
      }
    }
  }
  fclose(fp);
  *numberOfCells = maxCellIndex + 1;
  if (oldSolidMarker > 0 && numSolidTypes > 0){
    numSolidTypes -= BMK_SOLID_START - 2;
  }
  else if (numSolidTypes > 0){
    numSolidTypes -= BMK_SOLID_START - 1;
  }
  else if (oldSolidMarker > 0){
    numSolidTypes = 1;
  }
  (*numberOfSolidTypes) = numSolidTypes;
  (*solidTypes) = (int*)calloc(numSolidTypes, sizeof(int));
  nsol = (int*)malloc(sizeof(int)*numSolidTypes);
  for (i = 0; i < numFace; i++){
    if (bmk[i] == BMK_SOLID){
      bmk[i] = BMK_SOLID_START + numSolidTypes - 1;
    }
    if (bmk[i] >= BMK_SOLID_START && bmk[i] < BMK_SOLID_START + BMK_SOLID_MAX_TYPES){
      (*solidTypes)[bmk[i] - BMK_SOLID_START]++;
    }
  }
  if (oldSolidMarker > 0 && numSolidTypes > 0 && (*solidTypes)[numSolidTypes - 1] != oldSolidMarker){
    error("failed to read solid faces corresponding to boundary marker 2");
  }

  /* sort faces by type: inner, solid, farfield, initial, final */
  indx_f->inner = 0;
  indx_f->initial = indx_f->inner + nf[BMK_INNER];
  indx_f->solid = indx_f->initial + nf[BMK_INITIAL];
  indx_f->farfield = indx_f->solid + nf[BMK_SOLID];
  indx_f->final = indx_f->farfield + nf[BMK_FARFIELD];
  indx_f->numberOfFaces = (*numberOfFaces) = indx_f->final + nf[BMK_FINAL];

  (*numberOfFaceVertices) = (int*)malloc(sizeof(int)*(*numberOfFaces));
  (*faceVert) = (int**)malloc(sizeof(int*)*(*numberOfFaces));
  (*neighbourCell) = (int**)malloc(sizeof(int*)*(*numberOfFaces));
  (*normalVec) = (double**)malloc(sizeof(double*)*(*numberOfFaces));
  (*area) = (double*)malloc(sizeof(double)*(*numberOfFaces));

  for (i = 0; i < (*numberOfFaces); i++){
    (*normalVec)[i] = (double*) malloc (sizeof(double) * 3);
    (*neighbourCell)[i] = (int*) malloc (sizeof(int) * 2);
  }

  nf[BMK_INNER] = indx_f->inner;
  nf[BMK_INITIAL] = indx_f->initial;
  nf[BMK_SOLID] = indx_f->solid;
  nf[BMK_FARFIELD] = indx_f->farfield;
  nf[BMK_FINAL] = indx_f->final;
  nsol[0] = indx_f->solid;
  for (i = 1; i < numSolidTypes; i++){
    nsol[i] = nsol[i - 1] + (*solidTypes)[i - 1];
  }
  for (i = 0; i < numFace; i++){
    switch (bmk[i]){
    case BMK_INNER:
      j = nf[BMK_INNER];
      (*numberOfFaceVertices)[j] = numFaceVert[i];
      (*faceVert)[j] = (int*)malloc(sizeof(int)* ((*numberOfFaceVertices)[j]));
      for (k = 0; k < 2; k++){
        (*neighbourCell)[j][k] = neigh[i][k];
      }
      for (k = 0; k < (*numberOfFaceVertices)[j]; k++){
        (*faceVert)[j][k] = vert[i][k];
      }
      nf[BMK_INNER]++;
      break;
    case BMK_FARFIELD:
      j = nf[BMK_FARFIELD];
      (*numberOfFaceVertices)[j] = numFaceVert[i];
      (*faceVert)[j] = (int*)malloc(sizeof(int)* ((*numberOfFaceVertices)[j]));
      for (k = 0; k < 2; k++){
        if (neigh[i][k] >= 0){
          (*neighbourCell)[j][0] = neigh[i][k];
          break;
        }
      }
      for (k = 0; k < (*numberOfFaceVertices)[j]; k++){
        (*faceVert)[j][k] = vert[i][k];
      }
      nf[BMK_FARFIELD]++;
      break;
    case BMK_INITIAL:
      j = nf[BMK_INITIAL];
      (*numberOfFaceVertices)[j] = numFaceVert[i];
      (*faceVert)[j] = (int*)malloc(sizeof(int)* ((*numberOfFaceVertices)[j]));
      for (k = 0; k < 2; k++){
        if (neigh[i][k] >= 0){
          (*neighbourCell)[j][0] = neigh[i][k];
          break;
        }
      }
      for (k = 0; k < (*numberOfFaceVertices)[j]; k++){
        (*faceVert)[j][k] = vert[i][k];
      }
      nf[BMK_INITIAL]++;
      break;
    case BMK_FINAL:
      j = nf[BMK_FINAL];
      (*numberOfFaceVertices)[j] = numFaceVert[i];
      (*faceVert)[j] = (int*)malloc(sizeof(int)* ((*numberOfFaceVertices)[j]));
      for (k = 0; k < 2; k++){
        if (neigh[i][k] >= 0){
          (*neighbourCell)[j][0] = neigh[i][k];
          break;
        }
      }
      for (k = 0; k < (*numberOfFaceVertices)[j]; k++){
        (*faceVert)[j][k] = vert[i][k];
      }
      nf[BMK_FINAL]++;
      break;
    default:
      if (bmk[i] >= BMK_SOLID_START && bmk[i] < BMK_SOLID_START + BMK_SOLID_MAX_TYPES){
        for (k = 0; k < 2; k++){
          if (neigh[i][k] >= 0){
            if (bmk[i] - BMK_SOLID_START < 0){
              error("nsol[%d] cannot be evaluated", bmk[i] - BMK_SOLID_START);
            }
            j = nsol[bmk[i] - BMK_SOLID_START];
            (*numberOfFaceVertices)[j] = numFaceVert[i];
            (*faceVert)[j] = (int*)malloc(sizeof(int)* ((*numberOfFaceVertices)[j]));
            (*neighbourCell)[j][0] = neigh[i][k];
            for (l = 0; l < (*numberOfFaceVertices)[j]; l++){
              (*faceVert)[j][l] = vert[i][l];
            }
            nsol[bmk[i] - BMK_SOLID_START]++;
            nf[BMK_SOLID]++;
          }
        }
      }
      else {
        error("Unknown boundary marker -> %d", bmk[i]);
      }
    }
  }

  if (numSolidTypes > 0 && nsol[numSolidTypes - 1] != nf[BMK_SOLID]){
    error("failed to work out different solid parts");
  }

  free(bmk);
  for (i = 0; i < numFace; i++){
    free(vert[i]);
    free(neigh[i]);
  }
  free(vert);
  free(neigh);
  free(numFaceVert);
  free(nsol);
}

