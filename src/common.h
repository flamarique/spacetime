/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef SOLVER_COMMON_HEADERS_H
#define SOLVER_COMMON_HEADERS_H

#include "main_headers.h"


/**
 * Allocates dynamic memory for Euler (common to JST & UW) solver.
 *
 * @warning This function allocates dynamic memory. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void allocate_common_memory_Euler(const int numberOfCells, const face_map *indx_f, double **rho0,
    double **dt, double **specRad, double **specRadFace, double ***w0, double ***invflux);


/**
 * Allocates memory for RANS (common to JST & UW) solver.
 *
 * @warning This function allocates dynamic memory. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void allocate_common_memory_RANS(const int numberOfCells, const face_map *indx_f, double **mubar0,
    double **tauxx, double **tauxy, double **tauyy, double **qx, double **qy, double **production,
    double **destruction, double **diffusion_s, double **diffusion_v, double **convection,
    double **SAflux, double ***viscflux);


/**
 * Releases common heap memory for Euler-JST solver.
 */
void free_common_memory_Euler(
  const int numberOfCells,
  const int numberOfFaces,
  double *rho0,
  double *dt,
  double *specRad,
  double *specRadFace,
  double **w0,
  double **invflux
  );


/**
 * Releases common heap memory for RANS-JST solver.
 */
void free_common_memory_RANS(
  const int numberOfCells,
  const int numberOfFaces,
  double *mubar0,
  double *tauxx,
  double *tauxy,
  double *tauyy,
  double *qx,
  double *qy,
  double *production,
  double *destruction,
  double *diffusion_s,
  double *diffusion_v,
  double *convection,
  double *SAflux,
  double **viscflux
  );


/**
 * Allocates memory for weighted Runge-Kutta residuals.
 */
double **allocate_memory_RKRes(const int numberOfCells);


/**
 * Allocates memory for weighted SA residuals.
 */
double *allocate_memory_SARes(const int numberOfCells);


/**
 * Releases memory for weighted Runge-Kutta residuals.
 */
void free_memory_RKRes(const int numberOfCells, double **RKRes);


/**
 * Releases memory for weighted SA residuals.
 */
void free_memory_SARes(double *SARes);


/**
 * Calculates spacetime (ST) geometry for initial problem. In particular it
 * works out the following:
 *     (1)  numberOfNodes
 *     (2)  numberOfFaces
 *     (3)  numberOfCells
 *     (4)  indx_f
 *     (5)  neighbourCell
 *     (6)  numberOfFaceVertices
 *     (7)  faceVert
 *     (8)  x
 *     (9)  y
 *     (10) t
 */
void get_st_initial(const int numNodesST, const face_map *indx_f0,
    const face_map *indx_fST, const int *numFaceVertST, int **edgeVert, int **faceVertST,
    int **neighbourFace, const double *xST, const double *yST,
    int *numNodes, int *numFaces, int *numCells, face_map **indx_f, int **numFaceVerts,
    int ***faceVert, int ***neighbourCell, double **x, double **y, double **t);


/**
 * Work out gradients of Euler primitive vars (rho, u, v, p) at cell centres.
 *
 * @warning This function allocates dynamic memory for gradients. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_gradients_Euler(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const double *volume,
  const flow *cellFlow,
  double ***gradient_rho,
  double ***gradient_u,
  double ***gradient_v,
  double ***gradient_p
  );


/**
 * Work out gradient of temperature (T) at cell centres.
 *
 * @warning This function allocates dynamic memory for gradient. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_gradient_T(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const double *volume,
  const flow *cellFlow,
  double ***gradient_T
  );


/**
 * Work out gradient of nubar at cell centres.
 *
 * @warning This function allocates dynamic memory for gradients. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_gradient_nub(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const double *volume,
  const flow *cellFlow,
  double ***gradient_nub
  );

/**
 * Calculate undivided Laplacians for Euler eqs. via a central-difference approach.
 * There are 4 components at each cell: continuity (1), momentum (2) and energy (1).
 *
 * @warning This function allocates dynamic memory for laplacians. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
double **calculate_undivided_Laplacians_Euler(
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double *faceFactor,  // usually snorm = sqrt(nx^2 + ny^2) but perhaps could make snorm = 1.0.
  const flow *cellFlow,
  double **W
  );

/**
 * Calculate conserved variables from primitive variables.
 */
void calculate_conserved_vars(
  const int numberOfCells,
  const flow *cellFlow,
  double **W
  );

/**
 * Works out primitive vars (rho, u, v, p, e) from conserved vars.
 */
void calculate_primitive_vars(
  const int numberOfCells,
  double **W,
  flow *cellFlow
  );

/**
 * Calculate primitive variables (rho, u, v, p, mubar) at halo cells.
 */
void calculate_halo_cells_RANS(
    const double sosinf,
    const double Minf,
    double **normalVec,
    double **solidFaceVel,
    int **neighbourCell,
    const face_map *indx_f,
    const flow *refFlow,
    const turb *saConst,
    flow *cellFlow
  );

/**
 * Calculate primitive variables (rho, u, v, p, mubar) at halo cells.
 */
void calculate_halo_cells_Euler(
  const double sosinf,
  const double Minf,
  double **normalVec,
  int **neighbourCell,
  const face_map *indx_f,
  const flow *refFlow,
  flow *cellFlow
  );

/**
 * Constructs augmented state from independent variables (rho, u, v, p, mubar) for RANS.
 */
void construct_augmented_state_RANS(const int numValues, const double Minf, const double Tref,
    const turb *saConst, flow *flowVar);

/**
 * Constructs augmented state from independent variables (rho, u, v, p) for Euler.
 */
void construct_augmented_state_Euler(const int numValues, flow *flowVar);

/**
 * Calculates viscous fluxes at faces.
 */
void calculate_viscous_face_fluxes(
  const face_map *indx_f,
  int **neighbourCell,
  double ***connecVec,
  double **normalVec,
  double **grad_u,
  double **grad_v,
  double **grad_T,
  const double *area,
  const double Reinf,
  const double Prinf,
  const double Minf,
  const flow *cellFlow,
  const turb *saConst,
  double **viscflux
  );

/**
 * Calculates spectral radii (at cells and faces).
 */
void calculate_spectral_radii(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double *area,
  double **normalVec,
  const flow *cellFlow,
  double *specRad,
  double *specRadFace
  );

/**
 * Calculates unidivided laplacians of nu_bar at cells.
 *
 * @warning This function allocates dynamic memory for laplacians. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
void calculate_undivided_laplacians_nub(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const flow *cellFlow,
  double **laplacian_nub
  );

/**
 * Calculates Runge-Kutta residuals.
 *
 * @warning This function allocates dynamic memory for RKflux. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
double **calculate_runge_kutta_residuals(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const int numberOfTerms,
  ...
  );

/**
 * Calculates Spalart-Allmaras residuals.
 */
void calculate_SA_residuals(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double *convection,
  const double *production,
  const double *destruction,
  const double *diffusion_v,
  const double *diffusion_s,
  double *SAflux
  );

/**
 * Works out the timestep size for Euler eqs. of motion.
 */
void calculate_timestep_Euler(
  const int numberOfCells,
  int **neighbourCell,
  const double CFL,
  const double *volume,
  const double *specRad,
  const face_map *indx_f,
  double *dt
  );

/**
 * Works out the timestep size for RANS eqs. of motion.
 */
void calculate_timestep_RANS(
  const int numberOfCells,
  int **neighbourCell,
  const double CFL,
  const double Reinf,
  const double Prinf,
  const double *Psi,
  const double *volume,
  const double *area,
  const double *specRad,
  const face_map *indx_f,
  const flow *cellFlow,
  const turb *saConst,
  double *dt
  );

/**
 * Works out volume contributions to SA eqn.
 *
 * @warning Must call this subroutine BEFORE `calculate_surface_contrib_SA()`
 */
void calculate_volume_contrib_SA(
  const int numberOfCells,
  const double Reinf,
  const double *volume,
  const double *wallDist,
  const double *lap_nub,
  double **grad_rho,
  double **grad_u,
  double **grad_v,
  double **grad_nub,
  const flow *cellFlow,
  const turb *saConst,
  double *production,
  double *destruction,
  double *diffusion_v
  );

/**
 * Works out surface contributions to SA eqn.
 *
 * @warning Must call this subroutine AFTER `calculate_volume_contrib_SA()`.
 */
void calculate_surface_contrib_SA(
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  double **grad_nub,
  const double Reinf,
  const flow *cellFlow,
  const turb *saConst,
  double *diffusion_s
  );


/**
 * Works out convective contributions to SA eqn.
 *
 * @warning Must call this subroutine AFTER `calculate_volume_contrib_SA()`.
 */
void calculate_convective_SA(
  const face_map *indx_f,
  int **neighbourCell,
  double **normalVec,
  const double *area,
  const flow *cellFlow,
  double *convection
  );

/**
 * Adds RK residual contribution to final step update.
 */
void add_stage_RKRes_contribution_to_update(const int numberOfCells, const double coeff, double **RKflux_stage,
  double**RKRes);


/**
 * Adds SA residual contribution to final step update.
 */
void add_stage_SARes_contribution_to_update(const int numberOfCells, const double coeff, double *SAflux_stage,
  double*SARes);


/**
 * Releases heap memory at every stage in the Runge-Kutta iteration of Euler solver.
 */
void free_partial_memory_Euler(
  const int numberOfCells,
  double **RKflux,
  double **grad_rho,
  double **grad_u,
  double **grad_v,
  double **grad_p
  );


/**
 * Releases heap memory at every stage in the Runge-Kutta iteration of RANS solver.
 */
void free_partial_memory_RANS(
  const int numberOfCells,
  double **grad_T,
  double **grad_nub
  );


/**
 * Calculates convergence residuals based on l2-norm of density.
 */
double calculate_convergence_residual(const int numberOfCells, const double totalVolume,
    const double *volume, const double *dt, const double *rho0, const flow *cellFlow);

#endif
