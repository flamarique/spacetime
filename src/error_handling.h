/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef ERROR_HANDLING_H
#define ERROR_HANDLING_H

#define error(...) do {fprintf(stdout,"[x] ERROR in %s(): ", __func__); fprintf(stdout,__VA_ARGS__); fprintf(stdout,"\n"); fflush(stdout); fprintf(flog,"[x] ERROR in %s(): ", __func__); fprintf(flog,__VA_ARGS__); fprintf(flog,"\n"); fflush(flog); fclose(flog); exit(EXIT_FAILURE);} while(0)
#define err_init(...) do {fprintf(stdout,"[x] ERROR in %s(): ", __func__); fprintf(stdout,__VA_ARGS__); fprintf(stdout,"\n"); fflush(stdout); exit(EXIT_FAILURE);} while(0)
#define warning(...) do {fprintf(stdout,"[!] WARNING: "); fprintf(stdout,__VA_ARGS__); fprintf(stdout,"\n"); fflush(stdout); fprintf(flog,"[!] WARNING: "); fprintf(flog,__VA_ARGS__); fprintf(flog,"\n"); fflush(flog);} while(0)
#define verbose(...) do {fprintf(stdout,"[+] "); fprintf(stdout,__VA_ARGS__); fprintf(stdout,"\n"); fflush(stdout); fprintf(flog,"[+] "); fprintf(flog,__VA_ARGS__); fprintf(flog,"\n"); fflush(flog);} while(0);

FILE *flog;

#define CONVERGED_SOLUTION 0
#define NEED_TO_SAVE 1
#define ERR_RESIDUAL 2
#define ERR_MAXITER 3

#endif
