/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "misc.h"

/*****************************************************************************/
/*                                                                           */
/*  get_time()               Calculates elapsed time from origin t0          */
/*                                                                           */
/*****************************************************************************/

static formated_time *get_time(time_t t0){
  formated_time *elapsed = (formated_time*)malloc(sizeof(formated_time));
  elapsed->sec = (int)(time(NULL) - t0);
  elapsed->min = elapsed->hr = elapsed->day = 0;
  if (elapsed->sec >= 86400){
    elapsed->day = elapsed->sec / 86400;
    elapsed->sec -= 86400 * elapsed->day;
  }
  if (elapsed->sec >= 3600){
    elapsed->hr = elapsed->sec / 3600;
    elapsed->sec -= 3600 * elapsed->hr;
  }
  if (elapsed->sec >= 60){
    elapsed->min = elapsed->sec / 60;
    elapsed->sec -= 60 * elapsed->min;
  }
  return elapsed;
}

/*****************************************************************************/
/*                                                                           */
/*  get_time_message()                Gets a total running time message      */
/*                                                                           */
/*****************************************************************************/

char *get_time_message(time_t t0){
  char num[4];
  char *str = (char*)malloc(sizeof(char)*FILENAME_MAX);
  formated_time *elapsed;

  elapsed = get_time(t0);
  strcpy(str, "Total running time is ");
  if (elapsed->day > 0){
    sprintf(num, "%d", elapsed->day);
    strcat(str, num);
    strcat(str, " days ");
  }
  if (elapsed->hr > 0){
    sprintf(num, "%d", elapsed->hr);
    strcat(str, num);
    strcat(str, " hours ");
  }
  if (elapsed->min > 0){
    sprintf(num, "%d", elapsed->min);
    strcat(str, num);
    strcat(str, " mins ");
  }
  sprintf(num, "%d", elapsed->sec);
  strcat(str, num);
  strcat(str, " secs");
  strcat(str, " (walltime)");
  free(elapsed);

  return str;
}

/*****************************************************************************/
/*                                                                           */
/*  stretch_grid()              stretches grid in the specified direction    */
/*                                                                           */
/*****************************************************************************/

void stretch_grid(
  int numberOfNodes,
  double stretch,
  double *x
  )
{
  int i;

  verbose("Stretching grid in the time direction (kt = %lg)", stretch);
  for (i = 0; i < numberOfNodes; i++){
    x[i] *= stretch;
  }
}


/**
 * Non-dimensionalizes 'xdim[]' with 'refValue' and deallocates memory for 'xdim[]'
 */
double *non_dim_x(
  int numberOfNodes,
  double *xdim,
  double refValue
  )
{
  int i;
  double *x;

  x = (double*)malloc(sizeof(double)*numberOfNodes);

  for (i = 0; i < numberOfNodes; i++){
    x[i] = xdim[i] / refValue;
  }

  free(xdim);

  return x;
}


/*****************************************************************************/
/*                                                                           */
/*  get_viscous_psi_init()      Works out Psi values for viscous timestep    */
/*                                                                           */
/*****************************************************************************/

double *get_viscous_psi_init(
  int *numberOfFaceVertices,
  int **faceVert,
  double *x,
  double *y,
  face_map *indx_f
  )
{
  int i, ii;
  double dx1, dy1, dx2, dy2;
  double *Psi;

  /* NOTE: vertices in face must be sorted clockwise or counter-clockwise */
  /* It accepts faces of type CTRIA3 or CQUAD4 from NASTRAN types */
  Psi = (double*)malloc(sizeof(double)*(indx_f->solid - indx_f->initial));
#pragma omp parallel
  {
#pragma omp for private (dx1, dy1, dx2, dy2, ii)
    for (i = indx_f->initial; i < indx_f->solid; i++){
      ii = i - indx_f->initial;
      dx1 = fabs(x[faceVert[i][2]] - x[faceVert[i][1]]);
      dy1 = fabs(y[faceVert[i][2]] - y[faceVert[i][1]]);
      dx2 = fabs(x[faceVert[i][0]] - x[faceVert[i][1]]);
      dy2 = fabs(y[faceVert[i][0]] - y[faceVert[i][1]]);
      Psi[ii] = pow(dx1 + dy2, 2.0) + pow(dx2 + dy1, 2.0);
      if (numberOfFaceVertices[i] == 3){
        Psi[ii] /= 4.0;
      }
    }
  }
  return Psi;
}


/**
 * Works out Psi values for viscous timestep.
 *
 * @warning Face vertices must be sorted counter-clockwise. A call to method
 *   calculate_faces_3d() is enough to guarantee this.
 */
double *get_viscous_psi_HEXA(
  const face_map *indx_f, // full mesh (before applying periodicity)
  const int numberOfCells,
  const int *numberOfFaceVertices,
  int **neighbourCell,
  int **faceVert,
  const double *x,
  const double *y,
  const double *t
  )
{
  int i, j, k, face;
  int **cellFaces;
  double dx, dy, dt, xmin, xmax, ymin, ymax, tmin, tmax;
  double *Psi;

  verbose("Calculating parameters for viscous time step");

  /* alloc dyanmic memory */
  cellFaces = (int**)malloc(sizeof(int*)*numberOfCells);
  for (i = 0; i < numberOfCells; i++)
    cellFaces[i] = (int*)calloc(5, sizeof(int));

  /* get cellFaces per cell */
  for (i = indx_f->inner; i < indx_f->initial; i++){
    if (numberOfFaceVertices[i] != 4)
      error("Invalid number of face vertices (it should be 4)");
    for (j = 0; j < 2; j++){
      if (cellFaces[neighbourCell[i][j]][0] > 3){
        cellFaces[neighbourCell[i][j]] = (int*)realloc(cellFaces[neighbourCell[i][j]], sizeof(int)*(cellFaces[neighbourCell[i][j]][0] + 2));
      }
      cellFaces[neighbourCell[i][j]][0]++;
      cellFaces[neighbourCell[i][j]][cellFaces[neighbourCell[i][j]][0]] = i;
    }
  }
  j = 0;
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    if (numberOfFaceVertices[i] != 4)
      error("Invalid number of face vertices (it should be 4)");
    if (cellFaces[neighbourCell[i][j]][0] > 3){
      cellFaces[neighbourCell[i][j]] = (int*)realloc(cellFaces[neighbourCell[i][j]], sizeof(int)*(cellFaces[neighbourCell[i][j]][0] + 2));
    }
    cellFaces[neighbourCell[i][j]][0]++;
    cellFaces[neighbourCell[i][j]][cellFaces[neighbourCell[i][j]][0]] = i;
  }

  /* calculate Psi values */
  Psi = (double*)malloc(sizeof(double)*numberOfCells);
  for (i = 0; i < numberOfCells; i++)
  {
    // Work maximum and minimum values of x and y coordinates for each cell
    xmax = ymax = tmax = -1.0e+265;
    xmin = ymin = tmin = +1.0e+265;
    for (j = 1; j < cellFaces[i][0] + 1; j++)
    {
      face = cellFaces[i][j];
      for (k = 0; k < numberOfFaceVertices[face]; k++)
      {
        xmin = min(xmin, x[faceVert[face][k]]);
        ymin = min(ymin, y[faceVert[face][k]]);
        tmin = min(tmin, t[faceVert[face][k]]);
        xmax = max(xmax, x[faceVert[face][k]]);
        ymax = max(ymax, y[faceVert[face][k]]);
        tmax = max(tmax, t[faceVert[face][k]]);
      }
    }

    // Now work out Psi values
    dx = fabs(xmax - xmin);
    dy = fabs(ymax - ymin);
    dt = fabs(tmax - tmin);
    Psi[i] = pow(dt, 2.0) * (pow(dx, 2.0) + pow(dy, 2.0));
  }

  /* free memory */
  for (i = 0; i < numberOfCells; i++)
    free(cellFaces[i]);
  free(cellFaces);

  return Psi;
}


/*****************************************************************************/
/*                                                                           */
/*  get_cell_centres()             Works out cell centres                    */
/*                                                                           */
/*****************************************************************************/

double **get_cell_centres(
  int numberOfCells,
  int *numberOfCellVertices,
  int **cellVert,
  double *x,
  double *y,
  double *t
  )
{
  int i, k;
  int num_dims = 3;
  double **cellCentre;

  verbose("Calculating cell centres");

  cellCentre = (double**)malloc(sizeof(double*)*numberOfCells);

  for (i = 0; i < numberOfCells; i++){
    cellCentre[i] = (double*)malloc(sizeof(double)*num_dims);
    cellCentre[i][0] = cellCentre[i][1] = cellCentre[i][2] = 0.0;
    for (k = 0; k < numberOfCellVertices[i]; k++){
      cellCentre[i][0] += x[cellVert[i][k]] / (double)numberOfCellVertices[i];
      cellCentre[i][1] += y[cellVert[i][k]] / (double)numberOfCellVertices[i];
      cellCentre[i][2] += t[cellVert[i][k]] / (double)numberOfCellVertices[i];
    }
  }

  return cellCentre;
}

/*****************************************************************************/
/*                                                                           */
/*  get_face_centres()             Works out face centres                    */
/*                                                                           */
/*****************************************************************************/

double **get_face_centres(
  int numberOfFaces,
  int *numberOfFaceVertices,
  int **faceVert,
  double *x,
  double *y,
  double *t
  )
{
  int i, k;
  double **faceCentre;

  verbose("Calculating face centres");

  faceCentre = (double**)malloc(sizeof(double*)*numberOfFaces);
  for (i = 0; i < numberOfFaces; i++){
    faceCentre[i] = (double*)malloc(sizeof(double)*3);
    faceCentre[i][0] = faceCentre[i][1] = faceCentre[i][2] = 0.0;
    for (k = 0; k < numberOfFaceVertices[i]; k++){
      faceCentre[i][0] += x[faceVert[i][k]] / (double)numberOfFaceVertices[i];
      faceCentre[i][1] += y[faceVert[i][k]] / (double)numberOfFaceVertices[i];
      faceCentre[i][2] += t[faceVert[i][k]] / (double)numberOfFaceVertices[i];
    }
  }

  return faceCentre;
}


/*****************************************************************************/
/*                                                                           */
/*  get_edge_centres()             Works out face centres                    */
/*                                                                           */
/*****************************************************************************/

double **get_edge_centres(
  int numberOfEdges,
  int **edgeVert,
  double *x,
  double *y
  )
{
  int i;
  double **edgeCentre;

  verbose("Calculating edge centres");

  edgeCentre = (double**)malloc(sizeof(double*)*numberOfEdges);
  for (i = 0; i < numberOfEdges; i++){
    edgeCentre[i] = (double*)malloc(sizeof(double)* 2);
    edgeCentre[i][0] = (x[edgeVert[i][0]] + x[edgeVert[i][1]]) / 2.0;
    edgeCentre[i][1] = (y[edgeVert[i][0]] + y[edgeVert[i][1]]) / 2.0;
  }

  return edgeCentre;
}



/*****************************************************************************/
/*                                                                           */
/*  get_connect_vec()              Works out connecting vectors from cell    */
/*                                 centres to face centres                   */
/*                                                                           */
/*****************************************************************************/

double ***get_connect_vec(
  int numDimensions,
  face_map *indx_f,
  int **neighbourCell,
  double **faceCentre,
  double **cellCentre
  )
{
  int i, j, k;
  double ***connecVec;

  connecVec = (double***)malloc(sizeof(double**)*indx_f->numberOfFaces);

#pragma omp parallel
  {
#pragma omp for private(j, k)
    for (i = indx_f->inner; i < indx_f->initial; i++){
      connecVec[i] = (double**)malloc(sizeof(double*)* 2);
      for (j = 0; j < 2; j++){
        connecVec[i][j] = (double*)malloc(sizeof(double)*numDimensions);
        for (k = 0; k < numDimensions; k++){
          connecVec[i][j][k] = faceCentre[i][k] - cellCentre[neighbourCell[i][j]][k];
        }
      }
    }
#pragma omp for private(k)
    for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
      connecVec[i] = (double**)malloc(sizeof(double*));
      connecVec[i][RIGHT] = (double*)malloc(sizeof(double)*numDimensions);
      for (k = 0; k < numDimensions; k++){
        connecVec[i][RIGHT][k] = faceCentre[i][k] - cellCentre[neighbourCell[i][RIGHT]][k];
      }
    }
  }

  return connecVec;
}


/*****************************************************************************/
/*                                                                           */
/*  get_connect_vec_P()            Works out connecting vectors from cell    */
/*                                 centres to face centres (PERIODIC MESH)   */
/*                                                                           */
/*****************************************************************************/

double ***get_connect_vec_P(
  face_map *indx_f,
  int **neighbourCell,
  double **faceCentre,
  double **cellCentre
  )
{
  int i, ii, j, k;
  int num_dims = 3;
  double ***connecVec;

  connecVec = (double***)malloc(sizeof(double**)*indx_f->final);

#pragma omp parallel
  {
#pragma omp for private(j, k)
    for (i = indx_f->inner; i < indx_f->initial; i++){
      connecVec[i] = (double**)malloc(sizeof(double*)* 2);
      for (j = 0; j < 2; j++){
        connecVec[i][j] = (double*)malloc(sizeof(double)*num_dims);
        for (k = 0; k < num_dims; k++){
          connecVec[i][j][k] = faceCentre[i][k] - cellCentre[neighbourCell[i][j]][k];
        }
      }
    }
#pragma omp for private(k, ii)
    for (i = indx_f->initial; i < indx_f->solid; i++){
      ii = indx_f->final + i - indx_f->initial;
      connecVec[i] = (double**)malloc(sizeof(double*)* 2);
      connecVec[i][RIGHT] = (double*)malloc(sizeof(double)*num_dims);
      for (k = 0; k < num_dims; k++){
        connecVec[i][RIGHT][k] = faceCentre[i][k] - cellCentre[neighbourCell[i][RIGHT]][k];
      }
      connecVec[i][LEFT] = (double*)malloc(sizeof(double)*num_dims);
      for (k = 0; k < num_dims; k++){
        connecVec[i][LEFT][k] = faceCentre[ii][k] - cellCentre[neighbourCell[ii][RIGHT]][k];
      }
    }
#pragma omp for private(k)
    for (i = indx_f->solid; i < indx_f->final; i++){
      connecVec[i] = (double**)malloc(sizeof(double*));
      connecVec[i][RIGHT] = (double*)malloc(sizeof(double)*num_dims);
      for (k = 0; k < num_dims; k++){
        connecVec[i][RIGHT][k] = faceCentre[i][k] - cellCentre[neighbourCell[i][RIGHT]][k];
      }
    }
  }

  return connecVec;
}


/*****************************************************************************/
/*                                                                           */
/*  get_solid_face_velocities()         works out solid face velocities      */
/*                                                                           */
/*****************************************************************************/

double **get_solid_face_velocities(
  const char *filename,
  const face_map *indx_f,
  const int totalNumNodes,
  const int *numberOfFaceVertices,
  int **faceVert
  )
{
  int i, ii, j, numSolidFaces, numberOfNodes, *id;
  double *xvel, *yvel, **fvel;
  char vel_file[FILENAME_MAX];
  FILE *fp;

  strcpy(vel_file, filename);
  strcat(vel_file, ".vel");

  fp = fopen(vel_file, "r");
  if (!fp){
    error("Failed to open file \"%s\"", vel_file);
  }

  if (fscanf(fp, "%d", &numberOfNodes) != 1)
    error("failed to read number of nodes and faces");

  id = (int*)malloc(sizeof(double)*totalNumNodes);
  xvel = (double*)malloc(sizeof(double)*numberOfNodes);
  yvel = (double*)malloc(sizeof(double)*numberOfNodes);

  for (i = 0; i < totalNumNodes; i++)
    id[i] = -1;

  for (i = 0; i < numberOfNodes; i++)
  {
    if (fscanf(fp, "%d %lg %lg", &j, &xvel[i], &yvel[i]) != 3)
      error("failed to read solid node velocities");
    id[j] = i;
  }

  fclose(fp);

  verbose("Calculating velocities at solid faces");
  numSolidFaces = indx_f->farfield - indx_f->solid;
  fvel = (double**)malloc(sizeof(double*)*numSolidFaces);
  for (i = indx_f->solid; i < indx_f->farfield; i++){
    ii = i - indx_f->solid;
    fvel[ii] = (double*)malloc(sizeof(double)* 2);
    fvel[ii][0] = fvel[ii][1] = 0.0;
    for (j = 0; j < numberOfFaceVertices[i]; j++){
      fvel[ii][0] += xvel[id[faceVert[i][j]]] / (double)numberOfFaceVertices[i];
      fvel[ii][1] += yvel[id[faceVert[i][j]]] / (double)numberOfFaceVertices[i];
    }
  }

  free(id);
  free(xvel);
  free(yvel);

  return fvel;
}

/*****************************************************************************/
/*                                                                           */
/* get_periodic_connectivity()         Works out pure periodic connectivity  */
/*                                                                           */
/*****************************************************************************/

face_map *get_periodic_connectivity(
  face_map *indx_f,
  int **neighbourCell,
  double ***connecVec
  )
{
  int i, j, ii;
  face_map *indx_f_p;

  if (indx_f->numberOfFaces - indx_f->final != indx_f->solid - indx_f->initial)
    error("Mesh is not periodic. Number of faces at initial boundary differs from final boundary.");

  verbose("Calculating periodic connectivity");
  for (i = indx_f->initial; i < indx_f->solid; i++){
    j = indx_f->final + i - indx_f->initial;
    neighbourCell[i][LEFT] = neighbourCell[j][RIGHT];
  }

  indx_f_p = (face_map*)malloc(sizeof(face_map));
  indx_f_p->inner = indx_f->inner;
  indx_f_p->initial = indx_f_p->solid = indx_f->solid;
  indx_f_p->farfield = indx_f->farfield;
  indx_f_p->final = indx_f_p->numberOfFaces = indx_f->final;

  // fix connecVec
  for (i = indx_f->initial; i < indx_f->solid; i++)
  {
    ii = indx_f->final + i - indx_f->initial;
    connecVec[i] = (double**)realloc(connecVec[i], sizeof(double*)*2);
    connecVec[i][LEFT] = (double*)malloc(sizeof(double)*3);
    for (j = 0; j < 3; j++)
      connecVec[i][LEFT][j] = connecVec[ii][RIGHT][j];
    free(connecVec[ii][RIGHT]);
    free(connecVec[ii]);
  }

  return indx_f_p;
}


/*****************************************************************************/
/*                                                                           */
/*  distance()              Works out the distance between points u and v    */
/*                                                                           */
/*****************************************************************************/

static double distance(
  int dimension,
  double *u,
  double *v
  )
{
  int i;
  double dist;

  dist = 0.0;
  for (i = 0; i < dimension; i++){
    dist += pow(u[i] - v[i], 2.0);
  }
  dist = sqrt(dist);

  return dist;
}



/*****************************************************************************/
/*                                                                           */
/*  get_distance_to_wall()      Gets distance of cell centres to solid wall  */
/*                                                                           */
/*****************************************************************************/

double *get_distance_to_wall(
  const face_map *indx_f,
  const int numberOfCells,
  const double *faceArea,
  double **faceCentre,
  double **cellCentre
  )
{
  int i, j;
  double *wallDist, dist0, maxDt;

  wallDist = (double*)malloc(sizeof(double)*numberOfCells);

  /* allocate memory for wallDist and initialize to big number */
  for (i = 0; i < numberOfCells; i++)
    wallDist[i] = 1.0e+10;

  /* work out smallest distance to wall */
  for (i = 0; i < numberOfCells; i++)
  {
    for (j = indx_f->solid; j < indx_f->farfield; j++)
    {
      maxDt = sqrt (faceArea[j]); // if distance in time is greater than this then they belong to different time-steps
      if (abs (faceCentre[j][2] - cellCentre[i][2]) > maxDt)
        continue;
      dist0 = distance (3, cellCentre[i], faceCentre[j]);
      wallDist[i] = min(wallDist[i], dist0);
    }
  }

  return wallDist;
}


/*****************************************************************************/
/*                                                                           */
/*  get_distance_to_wall_HEXA()  ets distance of cell centres to solid wall  */
/*                                                                           */
/*****************************************************************************/

double *get_distance_to_wall_HEXA(
  face_map *indx_f,
  int numberOfTimeSteps,
  int numberOfCells,
  double **faceCentre,
  double **cellCentre
  )
{
  int i, j, k, nSolid, nCells, cellId;
  double *wallDist, dist0;

  wallDist = (double*)malloc(sizeof(double)*numberOfCells);
  nCells = numberOfCells / numberOfTimeSteps;
  if (nCells*numberOfTimeSteps != numberOfCells){
    error("incorrect number of timesteps");
  }
  nSolid = (indx_f->farfield - indx_f->solid) / numberOfTimeSteps;
  if (nSolid*numberOfTimeSteps != indx_f->farfield - indx_f->solid){
    error("incorrect number of timesteps");
  }

#pragma omp parallel
  {
    /* allocate memory for wallDist and initialize to big number */
#pragma omp for
    for (i = 0; i < numberOfCells; i++){
      wallDist[i] = 1.0e+10;
    }

    /* work out smallest distance to wall */
#pragma omp for private (dist0, i, j, cellId)
    for (k = 0; k < numberOfTimeSteps; k++){
      for (i = 0; i < nCells; i++){
        cellId = k*nCells + i;
        for (j = indx_f->solid + k*nSolid; j < indx_f->solid + (k + 1)*nSolid; j++){
          dist0 = distance(3, cellCentre[cellId], faceCentre[j]);
          wallDist[cellId] = min(wallDist[cellId], dist0);
        }
      }
    }
  }

  return wallDist;
}


/*****************************************************************************/
/*                                                                           */
/* check_periodic_connectivity()               Checks periodic connectivity  */
/*                                                                           */
/*****************************************************************************/

void check_periodic_connectivity(
  face_map *indx_f,
  int numberOfTimeSteps,
  int **neighbourCell,
  int **faceVert,
  double *x,
  double *y,
  double *vx,
  double *vy
  )
{
  int i, ii, j, jj, k;
  int dcell, found;

  verbose("Checking periodic connectivity");

  printf("\ninner = %d\ninitial = %d\nsolid = %d\nfarfield = %d\nfinal = %d\nnumberOfFaces = %d\n\n", indx_f->inner, indx_f->initial, indx_f->solid, indx_f->farfield, indx_f->final, indx_f->numberOfFaces);

  for (i = indx_f->initial; i < indx_f->solid; i++){
    ii = i - indx_f->initial;
    k = indx_f->final + ii;
    for (j = 0; j < 4; j++){
      found = NO;
      for (jj = 0; jj < 4; jj++){
        if (fabs(x[faceVert[i][j]] - x[faceVert[k][jj]]) < 1.0e-10 && fabs(y[faceVert[i][j]] - y[faceVert[k][jj]]) < 1.0e-10){
          found = YES;
          break;
        }
      }
      if (!found){
        printf("\nInitial face:\n");
        printf("point 1 = (%lg,%lg)\n", x[faceVert[i][0]], y[faceVert[i][0]]);
        printf("point 2 = (%lg,%lg)\n", x[faceVert[i][1]], y[faceVert[i][1]]);
        printf("point 3 = (%lg,%lg)\n", x[faceVert[i][2]], y[faceVert[i][2]]);
        printf("point 4 = (%lg,%lg)\n", x[faceVert[i][3]], y[faceVert[i][3]]);
        printf("\nFinal face:\n");
        printf("point 1 = (%lg,%lg)\n", x[faceVert[k][0]], y[faceVert[k][0]]);
        printf("point 2 = (%lg,%lg)\n", x[faceVert[k][1]], y[faceVert[k][1]]);
        printf("point 3 = (%lg,%lg)\n", x[faceVert[k][2]], y[faceVert[k][2]]);
        printf("point 4 = (%lg,%lg)\n", x[faceVert[k][3]], y[faceVert[k][3]]);
        error("Inital (%d) and final (%d) faces are not coincident", i, k);
        //error("vertex %d of initial face %d (%lg,%lg) does not correspond with vertex %d of final face %d (%lg,%lg)", j, i, x[faceVert[i][j]], y[faceVert[i][j]], j, k, x[faceVert[k][j]], y[faceVert[k][j]]);
      }
    }
  }

  verbose("All vertices at initial boundary correspond with those at the final boundary");

  dcell = (numberOfTimeSteps - 1)*(indx_f->solid - indx_f->initial);
  for (i = indx_f->initial; i < indx_f->solid; i++){
    ii = i - indx_f->initial;
    k = indx_f->final + ii;
    if (neighbourCell[i][RIGHT] + dcell != neighbourCell[k][RIGHT]){
      error("neighbouring cell (%d) at initial face %d does not correspond with neighbouring cell (%d = %d + %d) at final face %d", neighbourCell[i][RIGHT], i, neighbourCell[k][RIGHT], neighbourCell[i][RIGHT], dcell, k);
    }
  }

  verbose("Check that solid nodes at initial boundary have the same velocities at the nodes at final boundary");
  for (i = 0; i < 255; i++){
    ii = 2550 + i;
    if (fabs(vx[i] - vx[ii])>EPSILON){
      verbose("Initial node -> (x,y) = (%.4lf,%.4lf)", x[i], y[i]);
      verbose("Final node   -> (x,y) = (%.4lf,%.4lf)", x[ii], y[ii]);
      error("node %d has different x-velocities at initial (%lg) and final (%lg) boundaries", i, vx[i], vx[ii]);
    }
    if (fabs(vy[i] - vy[ii])>EPSILON){
      verbose("Initial node -> (x,y) = (%.4lf,%.4lf)", x[i], y[i]);
      verbose("Final node   -> (x,y) = (%.4lf,%.4lf)", x[ii], y[ii]);
      error("node %d has different y-velocities at initial (%lg) and final (%lg) boundaries", i, vy[i], vy[ii]);
    }
  }

}
