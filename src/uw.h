/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef UW_HEADERS_H
#define UW_HEADERS_H

#include "main_headers.h"


/**
 * Updates the solution one time-step in pseudo-time for RANS eqs. of motion.
 */
double perform_update_RANS_uw(
    const int option,
    const int numberOfCells,
    int **neighbourCell,
    const face_map *indx_f,
    const double totalVolume,
    const double Tref,
    const double Reinf,
    const double Prinf,
    const double CFL,
    const double *Psi,
    const double *area,
    const double *volume,
    const double *wallDist,
    const double *faceFactor,
    double **normalVec,
    double **solidFaceVel,
    double **W,
    double ***connecVec,
    flow *cellFlow,
    const flow *freeFlow,
    const turb *saConst
  );


/**
 * Updates the solution one time-step in pseudo-time for Euler eqs. of motion.
 */
double perform_update_Euler_uw(
    const int option,
    const int numberOfCells,
    int **neighbourCell,
    const face_map *indx_f,
    const double totalVolume,
    const double CFL,
    const double *area,
    const double *volume,
    const double *faceFactor,
    double **normalVec,
    double **W,
    double ***connecVec,
    flow *cellFlow,
    const flow *freeFlow
  );


#endif
