/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef INCLUDE_HEADERS_H
#define INCLUDE_HEADERS_H

#ifdef _MSC_VER
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#else
#define LINUX
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "error_handling.h"

#ifdef _OPENMP
#include <omp.h>
#else
#define omp_get_num_threads() 0
#define omp_get_thread_num() 0
#endif

#define PI acos(-1.0)
#define TWO_PI (2.0*PI)
#define GAMMA 1.403
#define R_GAS 287.05
#define EPSILON 1.0e-16
#define EPS 1.0e-3

#define BMK_INNER 0
#define BMK_INITIAL 1
#define BMK_SOLID 2
#define BMK_SOLID_START 200
#define BMK_SOLID_MAX_TYPES 100
#define BMK_FARFIELD 3
#define BMK_FINAL 4

#define CENTRAL_DIFFERENCE 0
#define UPWIND 1

#define VAN_LEER 0
#define ROE 1

#define EULER 0
#define RANS 1

#define LEFT 1
#define RIGHT 0

#define LINE_LEN 90
#define MAX_NUM_SOLUTION_FILES 10

#define MESH_VERSION 12
#define SPACETIME 1
#define YES 1
#define NO 0

typedef struct FACE_MAP{
  int inner;
  int initial;
  int solid;
  int farfield;
  int final;
  int numberOfFaces;
} face_map;

typedef struct NODES_MAP{
  int solid[2];
} nodes_map;

typedef struct FLOW{
  double rho;
  double u;
  double v;
  double p;
  double e;
  double T;
  double mu;
  double mut;
  double mubar;
} flow;

typedef struct TURB{
  double cv1;
  double cv1_3;
  double cv2;
  double cv3;
  double cw2;
  double cw3;
  double cw3_6;
  double ct3;
  double ct4;
  double cw1;
  double cb1;
  double cb2;
  double cn1;
  double sigma;
  double k;
  double k_2;
  double far_mubar;
  double Prturb;
} turb;

typedef struct hhmmss{
  int sec;
  int min;
  int hr;
  int day;
} formated_time;

#ifdef LINUX
#define max(a,b) \
  ({ __typeof__(a) _a = (a); \
  __typeof__(b) _b = (b); \
  _a > _b ? _a : _b; })

#define min(a,b) \
  ({ __typeof__(a) _a = (a); \
  __typeof__(b) _b = (b); \
  _a < _b ? _a : _b; })
#endif


#endif

