/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef MISC_HEADER_H

#include "main_headers.h"

#define temperature(rho,p,Minf) (GAMMA*pow((Minf), 2.0)*(p) / (rho))
#define pressure(rho,u,v,E) ((rho)*(GAMMA - 1.0)*((E) - (pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define energy(rho,u,v,p) (((p) / ((GAMMA - 1.0)*(rho))) + ((pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define suderland(Tref) (110.0/(Tref))
#define viscosity(Tref,T) (pow((T), 1.5)*(1.0 + suderland(Tref)) / ((T) + suderland(Tref)))
#define sound_speed(p,rho) sqrt(GAMMA*(p)/(rho))

/*****************************************************************************/
/*                                                                           */
/*  get_time_message()                Gets a total running time message      */
/*                                                                           */
/*****************************************************************************/

char *get_time_message(time_t t0);


/*****************************************************************************/
/*                                                                           */
/*  stretch_grid()              stretches grid in the specified direction    */
/*                                                                           */
/*****************************************************************************/

void stretch_grid(
  int numberOfNodes,
  double stretch,
  double *x
  );


/**
 * Non-dimensionalizes 'xdim[]' with 'refValue' and deallocates memory for 'xdim[]'
 */
double *non_dim_x(
  int numberOfNodes,
  double *xdim,
  double refValue
  );


/*****************************************************************************/
/*                                                                           */
/*  get_viscous_psi_init()      Works out Psi values for viscous timestep    */
/*                                                                           */
/*****************************************************************************/

double *get_viscous_psi_init(
  int *numberOfFaceVertices,
  int **faceVert,
  double *x,
  double *y,
  face_map *indx_f
  );


/*****************************************************************************/
/*                                                                           */
/*  get_viscous_psi()           Works out Psi values for viscous timestep    */
/*                                                                           */
/*****************************************************************************/

double *get_viscous_psi_HEXA_deprecated(
  int numberOfCells,
  int **cellVert,
  double *x,
  double *y,
  double *t
  );


/**
 * Works out Psi values for viscous timestep.
 *
 * @warning Face vertices must be sorted counter-clockwise. A call to method
 *   calculate_faces_3d() is enough to guarantee this.
 */
double *get_viscous_psi_HEXA(
  const face_map *indx_f, // full mesh (before applying periodicity)
  const int numberOfCells,
  const int *numberOfFaceVertices,
  int **neighbourCell,
  int **faceVert,
  const double *x,
  const double *y,
  const double *t
  );

/*****************************************************************************/
/*                                                                           */
/*  get_cell_centres()             Works out cell centres                    */
/*                                                                           */
/*****************************************************************************/

double **get_cell_centres(
  int numberOfCells,
  int *numberOfCellVertices,
  int **cellVert,
  double *x,
  double *y,
  double *t
  );


/*****************************************************************************/
/*                                                                           */
/*  get_face_centres()             Works out face centres                    */
/*                                                                           */
/*****************************************************************************/

double **get_face_centres(
  int numberOfFaces,
  int *numberOfFaceVertices,
  int **faceVert,
  double *x,
  double *y,
  double *t
  );


/*****************************************************************************/
/*                                                                           */
/*  get_edge_centres()             Works out face centres                    */
/*                                                                           */
/*****************************************************************************/

double **get_edge_centres(
  int numberOfEdges,
  int **edgeVert,
  double *x,
  double *y
  );


/*****************************************************************************/
/*                                                                           */
/*  get_connect_vec()              Works out connecting vectors from cell    */
/*                                 centres to face centres                   */
/*                                                                           */
/*****************************************************************************/

double ***get_connect_vec(
  int numDimensions,
  face_map *indx_f,
  int **neighbourCell,
  double **faceCentre,
  double **cellCentre
  );


/*****************************************************************************/
/*                                                                           */
/*  get_connect_vec_P()            Works out connecting vectors from cell    */
/*                                 centres to face centres (PERIODIC MESH)   */
/*                                                                           */
/*****************************************************************************/

double ***get_connect_vec_P(
  face_map *indx_f,
  int **neighbourCell,
  double **faceCentre,
  double **cellCentre
  );


/*****************************************************************************/
/*                                                                           */
/*  get_solid_face_velocities()         works out solid face velocities      */
/*                                                                           */
/*****************************************************************************/

double **get_solid_face_velocities(
  const char *filename,
  const face_map *indx_f,
  const int totalNumNodes,
  const int *numberOfFaceVertices,
  int **faceVert
  );


/*****************************************************************************/
/*                                                                           */
/* get_periodic_connectivity()         Works out pure periodic connectivity  */
/*                                                                           */
/*****************************************************************************/

face_map *get_periodic_connectivity(
  face_map *indx_f,
  int **neighbourCell,
  double ***connecVec
  );


/*****************************************************************************/
/*                                                                           */
/*  get_distance_to_wall()      Gets distance of cell centres to solid wall  */
/*                                                                           */
/*****************************************************************************/

double *get_distance_to_wall(
  const face_map *indx_f,
  const int numberOfCells,
  const double *faceArea,
  double **faceCentre,
  double **cellCentre
  );


/*****************************************************************************/
/*                                                                           */
/*  get_distance_to_wall_HEXA()  gets distance of cell centres to solid wall  */
/*                                                                           */
/*****************************************************************************/

double *get_distance_to_wall_HEXA(
  face_map *indx_f,
  int numberOfTimeSteps,
  int numberOfCells,
  double **faceCentre,
  double **cellCentre
  );


/*****************************************************************************/
/*                                                                           */
/* check_periodic_connectivity()               Checks periodic connectivity  */
/*                                                                           */
/*****************************************************************************/

void check_periodic_connectivity(
  face_map *indx_f,
  int numberOfTimeSteps,
  int **neighbourCell,
  int **faceVert,
  double *x,
  double *y,
  double *vx,
  double *vy
  );

#endif
