/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "jst.h"
#include "common.h"

#define temperature(rho,p,Minf) (GAMMA*pow((Minf), 2.0)*(p) / (rho))
#define pressure(rho,u,v,E) ((rho)*(GAMMA - 1.0)*((E) - (pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define energy(rho,u,v,p) (((p) / ((GAMMA - 1.0)*(rho))) + ((pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define suderland(Tref) (110.0/(Tref))
#define viscosity(Tref,T) (pow((T), 1.5)*(1.0 + suderland(Tref)) / ((T) + suderland(Tref)))
#define sound_speed(p,rho) sqrt(GAMMA*(p)/(rho))

static const double RK_coeff[4] = {1.0, 2.0, 2.0, 1.0};

/**
 * Allocates dynamic memory for Euler-JST solver.
 *
 * @warning This function allocates dynamic memory. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
static void allocate_memory_Euler(
  const int numberOfCells,
  const face_map *indx_f,
  double **rho0,
  double **dt,
  double **specRad,
  double **specRadFace,
  double ***w0,
  double ***invflux,
  double ***diss,
  flow **face
  )
{
  int i;
  const int num_eqs = 4;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;

  allocate_common_memory_Euler(numberOfCells, indx_f, rho0, dt, specRad, specRadFace, w0, invflux);

  (*face) = (flow*)malloc(sizeof(flow)*numberOfFaces);
  (*diss) = (double**)malloc(sizeof(double*)*numberOfFaces);

#pragma omp parallel
  {
#pragma omp for
    for (i = indx_f->inner; i < indx_f->numberOfFaces; i++)
      (*diss)[i] = (double*)malloc(sizeof(double)*num_eqs);
  }
}


/**
 * Allocates memory for RANS-JST solver.
 *
 * @warning This function allocates dynamic memory. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
static void allocate_memory_RANS(
  const int numberOfCells,
  const face_map *indx_f,
  double **rho0,
  double **dt,
  double **specRad,
  double **specRadFace,
  double **mubar0,
  double **tauxx,
  double **tauxy,
  double **tauyy,
  double **qx,
  double **qy,
  double **production,
  double **destruction,
  double **diffusion_s,
  double **diffusion_v,
  double **convection,
  double **SAflux,
  double ***w0,
  double ***invflux,
  double ***viscflux,
  double ***diss,
  flow **face
  )
{
  allocate_memory_Euler(numberOfCells, indx_f, rho0, dt, specRad, specRadFace, w0, invflux, diss, face);
  allocate_common_memory_RANS(numberOfCells, indx_f, mubar0, tauxx, tauxy, tauyy, qx, qy,
      production, destruction, diffusion_s, diffusion_v, convection, SAflux, viscflux);
}


/**
 * Releases heap memory for Euler-JST solver.
 */
static void free_memory_Euler(
  const int numberOfCells,
  const int numberOfFaces,
  double *rho0,
  double *dt,
  double *specRad,
  double *specRadFace,
  double **w0,
  double **invflux,
  double **diss,
  flow *face
  )
{
  int i;

  free_common_memory_Euler(numberOfCells, numberOfFaces, rho0, dt, specRad, specRadFace, w0, invflux);

  free(face);
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfFaces; i++)
      free(diss[i]);
  }
  free(diss);
}


/**
 * Releases heap memory for RANS-JST solver.
 */
static void free_memory_RANS(
  const int numberOfCells,
  const int numberOfFaces,
  double *rho0,
  double *dt,
  double *specRad,
  double *specRadFace,
  double *mubar0,
  double *tauxx,
  double *tauxy,
  double *tauyy,
  double *qx,
  double *qy,
  double *production,
  double *destruction,
  double *diffusion_s,
  double *diffusion_v,
  double *convection,
  double *SAflux,
  double **w0,
  double **invflux,
  double **viscflux,
  double **diss,
  flow *face
  )
{
  free_memory_Euler(numberOfCells, numberOfFaces, rho0, dt, specRad, specRadFace, w0, invflux, diss, face);
  free_common_memory_RANS(numberOfCells, numberOfFaces, mubar0, tauxx, tauxy, tauyy, qx, qy, production,
      destruction, diffusion_s, diffusion_v, convection, SAflux, viscflux);
}


/**
 * Reconstruct face values from cell values (rho, u, v, p).
 */
static void reconstruct_face_values_Euler(
    const face_map *indx_f,
    int **neighbourCell,
    const flow *cellFlow,
    flow *face
)
{
  int i, left, right;

#pragma omp parallel private(left, right)
  {
#pragma omp for
    for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
      left = neighbourCell[i][LEFT];
      right = neighbourCell[i][RIGHT];
      face[i].rho = 0.5*(cellFlow[right].rho + cellFlow[left].rho);
      face[i].u = 0.5*(cellFlow[right].u + cellFlow[left].u);
      face[i].v = 0.5*(cellFlow[right].v + cellFlow[left].v);
      face[i].p = 0.5*(cellFlow[right].p + cellFlow[left].p);
    }
  }
}



/**
 * Reconstruct face values from cell values (rho, u, v, p, mubar).
 */
static void reconstruct_face_values_RANS(
    const face_map *indx_f,
    int **neighbourCell,
    const flow *cellFlow,
    flow *face
)
{
  int i, left, right;

#pragma omp parallel
  {
#pragma omp for private(i, left, right)
    for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
      left = neighbourCell[i][LEFT];
      right = neighbourCell[i][RIGHT];
      face[i].rho = 0.5*(cellFlow[right].rho + cellFlow[left].rho);
      face[i].u = 0.5*(cellFlow[right].u + cellFlow[left].u);
      face[i].v = 0.5*(cellFlow[right].v + cellFlow[left].v);
      face[i].p = 0.5*(cellFlow[right].p + cellFlow[left].p);
      face[i].mubar = 0.5*(cellFlow[right].mubar + cellFlow[left].mubar);
    }
  }
}


/**
 * Calculates inviscid fluxes at faces.
 */
static void calculate_inviscid_face_fluxes(
  double **normalVec,
  const double *area,
  const face_map *indx_f,
  const flow *face,
  double **invflux
  )
{
  int i, j;
  const int num_eqs = 4;

#pragma omp parallel
  {
#pragma omp for
    for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
      /* x-aligned fluxes */
      invflux[i][0] = face[i].rho * face[i].u * normalVec[i][0];
      invflux[i][1] = (face[i].rho * pow(face[i].u, 2.0) + face[i].p) * normalVec[i][0];
      invflux[i][2] = face[i].rho * face[i].u * face[i].v * normalVec[i][0];
      invflux[i][3] = (face[i].rho * face[i].e + face[i].p)*face[i].u * normalVec[i][0];
      /* y-aligned fluxes */
      invflux[i][0] += face[i].rho * face[i].v * normalVec[i][1];
      invflux[i][1] += face[i].rho * face[i].u * face[i].v * normalVec[i][1];
      invflux[i][2] += (face[i].rho * pow(face[i].v, 2.0) + face[i].p) * normalVec[i][1];
      invflux[i][3] += (face[i].rho * face[i].e + face[i].p)*face[i].v * normalVec[i][1];
      /* t-aligned fluxes */
      invflux[i][0] += face[i].rho * normalVec[i][2];
      invflux[i][1] += face[i].rho * face[i].u * normalVec[i][2];
      invflux[i][2] += face[i].rho * face[i].v * normalVec[i][2];
      invflux[i][3] += face[i].rho * face[i].e * normalVec[i][2];
    }
#pragma omp for private(j)
    for (i = indx_f->inner; i < indx_f->numberOfFaces; i++){
      for (j = 0; j < num_eqs; j++){
        invflux[i][j] *= area[i];
      }
    }
  }
}


/**
 * Calculates factor `phi` at faces.
 *
 * @warning needs previous call to `calculate_spectral_radii()`.
 */
static void calculate_phi(
  int **neighbourCell,
  const face_map *indx_f,
  const double *specRad,
  const double *specRadFace,
  double *phi
  )
{
  int i;
  double phi0, phi1;

#pragma omp parallel private(phi0, phi1)
  {
#pragma omp for
    for (i = indx_f->inner; i < indx_f->initial; i++){
      phi0 = pow(specRad[neighbourCell[i][0]] / (4.0*specRadFace[i]), 0.3);
      phi1 = pow(specRad[neighbourCell[i][1]] / (4.0*specRadFace[i]), 0.3);
      phi[i] = 4.0*phi0*phi1 / (phi0 + phi1);
    }
#pragma omp for
    for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
      phi[i] = 0.0;
    }
  }
}

/**
 * Calculates pressure-based switches at faces.
 */
static void calculate_diss_switches(
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double k2,
  const double k4,
  const flow *cellFlow,
  const double *s2,
  const double *s4,
  const double *faceFactor,
  double *eps2,
  double *eps4
  )
{
  int i;
  double dpp, dpm;
  double *epscell, *numerator, *denominator;

  // allocate dynamic memory
  epscell = (double*)malloc(sizeof(double)*numberOfCells);
  numerator = (double*)calloc(numberOfCells, sizeof(double));
  denominator = (double*)calloc(numberOfCells, sizeof(double));

  /* initialize numerator, denominator */
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++){
      numerator[i] = denominator[i] = 0.0;
    }
  }

  /* iterate over faces */
  for (i = indx_f->inner; i < indx_f->initial; i++){
    dpm = (cellFlow[neighbourCell[i][LEFT]].p - cellFlow[neighbourCell[i][RIGHT]].p)*faceFactor[i];
    dpp = (cellFlow[neighbourCell[i][LEFT]].p + cellFlow[neighbourCell[i][RIGHT]].p)*faceFactor[i];
    numerator[neighbourCell[i][RIGHT]] += dpm;
    numerator[neighbourCell[i][LEFT]] -= dpm;
    denominator[neighbourCell[i][RIGHT]] += dpp;
    denominator[neighbourCell[i][LEFT]] += dpp;
  }
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
    dpp = 2.0*cellFlow[neighbourCell[i][RIGHT]].p*faceFactor[i];
    denominator[neighbourCell[i][RIGHT]] += dpp;
  }

#pragma omp parallel
  {
    /* iterate over cells */
#pragma omp for
    for (i = 0; i < numberOfCells; i++){
      epscell[i] = fabs(numerator[i]) / denominator[i];
    }
    /* work out eps2 and eps4 */
#pragma omp for
    for (i = indx_f->inner; i < indx_f->initial; i++){
      eps2[i] = k2*s2[i] * max(epscell[neighbourCell[i][RIGHT]], epscell[neighbourCell[i][LEFT]]);
      eps4[i] = s4[i] * max(0.0, k4 - eps2[i]);
    }
#pragma omp for
    for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
      eps2[i] = k2*s2[i] * epscell[neighbourCell[i][RIGHT]];
      eps4[i] = s4[i] * max(0.0, k4 - eps2[i]);
    }
  }

  // release dynamic memory
  free(epscell);
  free(numerator);
  free(denominator);
}


/**
 * Calculates dissipation fluxes at each face.
 *
 * @warning needs previous call to `calculate_spectral_radii()`.
 */
static void calculate_dissipation(
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double k2,
  const double k4,
  const double *s2,
  const double *s4,
  const double *specRad,
  const double *specRadFace,
  const double *faceFactor,
  const flow *cellFlow,
  double **diss,
  double **W
  )
{
  int i, j;
  const int num_eqs = 4;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  double *phi, *eps2, *eps4, **laplacian;

  // allocate dynamic memory
  phi = (double*)malloc(sizeof(double)*numberOfFaces);
  eps2 = (double*)malloc(sizeof(double)*numberOfFaces);
  eps4 = (double*)malloc(sizeof(double)*numberOfFaces);

  laplacian = calculate_undivided_Laplacians_Euler(numberOfCells, neighbourCell, indx_f, faceFactor, cellFlow, W);
  calculate_phi(neighbourCell, indx_f, specRad, specRadFace, phi);
  calculate_diss_switches(numberOfCells, neighbourCell, indx_f, k2, k4, cellFlow, s2, s4,
      faceFactor, eps2, eps4);

#pragma omp parallel private(j)
  {
#pragma omp for
    for (i = indx_f->inner; i < indx_f->initial; i++){
      for (j = 0; j < num_eqs; j++){
        diss[i][j] = - faceFactor[i] * (eps2[i] * (W[neighbourCell[i][LEFT]][j] - W[neighbourCell[i][RIGHT]][j])
            - eps4[i] * (laplacian[neighbourCell[i][LEFT]][j] - laplacian[neighbourCell[i][RIGHT]][j])) * phi[i] * specRadFace[i];
      }
    }
#pragma omp for
    for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
      for (j = 0; j < num_eqs; j++){
        diss[i][j] = 0.0;
      }
    }
  }

  // release dynamic memory
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      free(laplacian[i]);
  }
  free(phi);
  free(eps2);
  free(eps4);
  free(laplacian);
}

/**
 * Updates the solution one time-step in pseudo-time for RANS eqs. of motion.
 */
double perform_update_RANS_jst(
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double totalVolume,
  const double Tref,
  const double Reinf,
  const double Prinf,
  const double CFL,
  const double k2,
  const double k4,
  const double *Psi,
  const double *area,
  const double *volume,
  const double *wallDist,
  const double *faceFactor,
  const double *s2,
  const double *s4,
  double **normalVec,
  double **solidFaceVel,
  double **W,
  double ***connecVec,
  const flow *freeFlow,
  const turb *saConst,
  flow *cellFlow
  )
{
  int i, j, stage;
  const int num_eqs = 4;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  const int totalNumCells = numberOfCells + indx_f->numberOfFaces - indx_f->initial;
  double residual, sosinf, Minf;
  double **grad_rho = NULL, **grad_u = NULL, **grad_v = NULL, **grad_p = NULL, **grad_T = NULL, **grad_nub = NULL;
  double *rho0, *mubar0, *dt, *specRad, *specRadFace;
  double **w0, **RKflux, **invflux, **diss, **viscflux, **RKRes, *SAflux, *SARes;
  double *tauxx, *tauxy, *tauyy, *qx, *qy;
  double *production, *destruction, *diffusion_s, *diffusion_v, *convection;
  double *lap_nub = NULL;
  flow *face;

  sosinf = sound_speed(freeFlow->p, freeFlow->rho);
  Minf = sqrt(pow(freeFlow->u, 2.0) + pow(freeFlow->v, 2.0)) / sosinf;

  // allocate heap memory for RANS
  allocate_memory_RANS(numberOfCells, indx_f, &rho0, &dt, &specRad, &specRadFace, &mubar0,
      &tauxx, &tauxy, &tauyy, &qx, &qy, &production, &destruction, &diffusion_s, &diffusion_v, &convection,
      &SAflux, &w0, &invflux, &viscflux, &diss, &face);

#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      rho0[i] = cellFlow[i].rho;
      mubar0[i] = cellFlow[i].mubar;
      for (j = 0; j < num_eqs; j++){
        w0[i][j] = W[i][j];
      }
    }
  }

  RKRes = allocate_memory_RKRes(numberOfCells);
  SARes = allocate_memory_SARes(numberOfCells);

  /* RUNGE-KUTTA iteration */
  for (stage = 0; stage < 4; stage++){
    calculate_gradients_Euler(numberOfCells, indx_f, neighbourCell, normalVec, area, volume, cellFlow,
        &grad_rho, &grad_u, &grad_v, &grad_p);

    reconstruct_face_values_RANS(indx_f, neighbourCell, cellFlow, face);
    construct_augmented_state_RANS(indx_f->numberOfFaces, Minf, Tref, saConst, face);

    calculate_inviscid_face_fluxes(normalVec, area, indx_f, face, invflux);
    calculate_spectral_radii(numberOfCells, indx_f, neighbourCell, area, normalVec,
        cellFlow, specRad, specRadFace);
    calculate_dissipation(numberOfCells,neighbourCell, indx_f, k2, k4, s2, s4,
        specRad, specRadFace, faceFactor, cellFlow, diss, W);

    if (stage == 0)
    {
      calculate_gradient_T(numberOfCells, indx_f, neighbourCell, normalVec, area, volume, cellFlow, &grad_T);
      calculate_viscous_face_fluxes(indx_f, neighbourCell, connecVec, normalVec,
        grad_u, grad_v, grad_T, area, Reinf, Prinf, Minf, cellFlow, saConst, viscflux);
    }

    RKflux = calculate_runge_kutta_residuals(numberOfCells, indx_f, neighbourCell,
        3, invflux, viscflux, diss);

    if (stage == 0)
    {
      calculate_timestep_RANS(numberOfCells, neighbourCell, CFL, Reinf, Prinf, Psi, volume, area,
          specRad, indx_f, cellFlow, saConst, dt);

      calculate_undivided_laplacians_nub(numberOfCells, indx_f, neighbourCell, cellFlow, &lap_nub);
      calculate_gradient_nub(numberOfCells, indx_f, neighbourCell, normalVec, area, volume, cellFlow,
          &grad_nub);
      calculate_volume_contrib_SA(numberOfCells, Reinf, volume, wallDist, lap_nub, grad_rho, grad_u,
          grad_v, grad_nub, cellFlow, saConst, production, destruction, diffusion_v);
      calculate_surface_contrib_SA(indx_f, neighbourCell, normalVec, area, grad_nub, Reinf,
          cellFlow, saConst, diffusion_s);
      free(lap_nub);
    }
    calculate_convective_SA(indx_f, neighbourCell, normalVec, area, cellFlow, convection);
    calculate_SA_residuals(numberOfCells, indx_f, neighbourCell, convection, production,
        destruction, diffusion_v, diffusion_s, SAflux);

    if (stage == 0)
      free_partial_memory_RANS(numberOfCells, grad_T, grad_nub);

    if (stage != 3)
    {
#pragma omp parallel
      {
#pragma omp for private(j)
        for (i = 0; i < numberOfCells; i++)
        {
          cellFlow[i].mubar = mubar0[i] - (dt[i] * SAflux[i] / (RK_coeff[stage + 1] * volume[i]));
          if (cellFlow[i].mubar <= 0.0)
          {  // need to properly code negSA
            cellFlow[i].mubar = 1.0e-6;
          }
          for (j = 0; j < num_eqs; j++)
          {
            W[i][j] = w0[i][j] - (dt[i] * RKflux[i][j] / (RK_coeff[stage + 1] * volume[i]));
          }
        }
      }
    }

    add_stage_RKRes_contribution_to_update(numberOfCells, RK_coeff[stage], RKflux, RKRes);
    add_stage_SARes_contribution_to_update(numberOfCells, RK_coeff[stage], SAflux, SARes);

    if (stage != 3)
    {
      calculate_primitive_vars(numberOfCells, W, cellFlow);
      calculate_halo_cells_RANS(sosinf, Minf, normalVec, solidFaceVel, neighbourCell, indx_f, freeFlow, saConst,
        cellFlow);
      construct_augmented_state_RANS(totalNumCells, Minf, Tref, saConst, cellFlow);
    }
    free_partial_memory_Euler(numberOfCells, RKflux, grad_rho, grad_u, grad_v, grad_p);
  }

  // update solution at next timestep
#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      cellFlow[i].mubar = mubar0[i] - (dt[i] * SARes[i] / volume[i]);
      if (cellFlow[i].mubar <= 0.0){
        cellFlow[i].mubar = 1.0e-6;
      }
      for (j = 0; j < num_eqs; j++){
        W[i][j] = w0[i][j] - (dt[i] * RKRes[i][j] / volume[i]);
      }
    }
  }
  free_memory_RKRes(numberOfCells, RKRes);
  free_memory_SARes(SARes);

  calculate_primitive_vars(numberOfCells, W, cellFlow);
  calculate_halo_cells_RANS(sosinf, Minf, normalVec, solidFaceVel, neighbourCell, indx_f, freeFlow, saConst,
    cellFlow);
  construct_augmented_state_RANS(totalNumCells, Minf, Tref, saConst, cellFlow);

  residual = calculate_convergence_residual(numberOfCells, totalVolume, volume, dt, rho0, cellFlow);

  // release heap memory
  free_memory_RANS(numberOfCells, numberOfFaces, rho0, dt, specRad, specRadFace, mubar0,
      tauxx, tauxy, tauyy, qx, qy, production, destruction, diffusion_s, diffusion_v,
      convection, SAflux, w0, invflux, viscflux, diss, face);

  return residual;
}

/**
 * Updates the solution one time-step in pseudo-time for Euler eqs. of motion.
 */
double perform_update_Euler_jst(
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double totalVolume,
  const double CFL,
  const double k2,
  const double k4,
  const double *area,
  const double *volume,
  const double *faceFactor,
  const double *s2,
  const double *s4,
  double **normalVec,
  double **W,
  const flow *freeFlow,
  flow *cellFlow
  )
{
  int i, j, stage;
  const int num_eqs = 4;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  const int totalNumCells = numberOfCells + indx_f->numberOfFaces - indx_f->initial;
  double residual, sosinf, Minf;
  double *rho0, *dt, *specRad, *specRadFace;
  double **w0, **RKflux, **invflux, **diss, **RKRes;
  flow *face;

  sosinf = sound_speed(freeFlow->p, freeFlow->rho);
  Minf = sqrt(pow(freeFlow->u, 2.0) + pow(freeFlow->v, 2.0)) / sosinf;

  allocate_memory_Euler(numberOfCells, indx_f, &rho0, &dt, &specRad, &specRadFace,
      &w0, &invflux, &diss, &face);

#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      rho0[i] = cellFlow[i].rho;
      for (j = 0; j < num_eqs; j++){
        w0[i][j] = W[i][j];
      }
    }
  }

  RKRes = allocate_memory_RKRes(numberOfCells);

  /* RUNGE-KUTTA iteration */
  for (stage = 0; stage < 4; stage++){
    reconstruct_face_values_Euler(indx_f, neighbourCell, cellFlow, face);
    construct_augmented_state_Euler(indx_f->numberOfFaces, face);

    calculate_inviscid_face_fluxes(normalVec, area, indx_f, face, invflux);
    calculate_spectral_radii(numberOfCells, indx_f, neighbourCell, area, normalVec,
        cellFlow, specRad, specRadFace);
    calculate_dissipation(numberOfCells,neighbourCell, indx_f, k2, k4, s2, s4,
        specRad, specRadFace, faceFactor, cellFlow, diss, W);

    RKflux = calculate_runge_kutta_residuals(numberOfCells, indx_f, neighbourCell, 2, invflux, diss);

    if (stage == 0)
      calculate_timestep_Euler(numberOfCells, neighbourCell, CFL, volume, specRad, indx_f, dt);

    if (stage != 3)
    {
#pragma omp parallel
      {
#pragma omp for private(j)
        for (i = 0; i < numberOfCells; i++)
        {
          for (j = 0; j < num_eqs; j++)
            W[i][j] = w0[i][j] - (dt[i] * RKflux[i][j] / (RK_coeff[stage + 1] * volume[i]));
        }
      }
    }

    add_stage_RKRes_contribution_to_update(numberOfCells, RK_coeff[stage], RKflux, RKRes);

    if (stage != 3)
    {
      calculate_primitive_vars(numberOfCells, W, cellFlow);
      calculate_halo_cells_Euler(sosinf, Minf, normalVec, neighbourCell, indx_f, freeFlow, cellFlow);
      construct_augmented_state_Euler(totalNumCells, cellFlow);
    }
    free_memory_RKRes(numberOfCells, RKflux);
  }

  // update solution at next timestep
#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      for (j = 0; j < num_eqs; j++){
        W[i][j] = w0[i][j] - (dt[i] * RKRes[i][j] / volume[i]);
      }
    }
  }
  free_memory_RKRes(numberOfCells, RKRes);

  calculate_primitive_vars(numberOfCells, W, cellFlow);
  calculate_halo_cells_Euler(sosinf, Minf, normalVec, neighbourCell, indx_f, freeFlow, cellFlow);
  construct_augmented_state_Euler(totalNumCells, cellFlow);

  residual = calculate_convergence_residual(numberOfCells, totalVolume, volume, dt, rho0, cellFlow);

  // release heap memory
  free_memory_Euler(numberOfCells, numberOfFaces, rho0, dt, specRad, specRadFace, w0,
      invflux, diss, face);

  return residual;
}

