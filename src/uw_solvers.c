/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "uw.h"
#include "common.h"
#include "uw_solvers.h"

/*****************************************************************************/
/*                                                                           */
/*  solver_uw()                       Gets spacetime solution with Upwind    */
/*                                                                           */
/*****************************************************************************/

int solver_RANS_uw(
  const int option,
  const int isave,
  const int maxiter,
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double eps_res,
  const double Tref,
  const double Reinf,
  const double Prinf,
  const double CFL,
  const double *Psi,
  const double *area,
  const double *volume,
  const double *wallDist,
  double **normalVec,
  double **solidFaceVel,
  double ***connecVec,
  const flow *freeFlow,
  flow *cellFlow,
  const turb *saConst,
  FILE *fres
  )
{
  int i, info;
  const int num_eqs = 4; // continuity (1), momentum (2) and energy (1)
  static int iter = 1;
  time_t t0, t1;
  double *faceFactor, **W;
  static double residual0 = 1.0;
  double residual, totalVolume;

  faceFactor = (double*)malloc(sizeof(double)*(indx_f->numberOfFaces - indx_f->inner));
  for (i = 0; i < indx_f->numberOfFaces; i++){
    faceFactor[i] = sqrt(pow(normalVec[i][0], 2.0) + pow(normalVec[i][1], 2.0));
    //faceFactor[i] = 1.0;
  }

  totalVolume = 0.0;
  for (i = 0; i < numberOfCells; i++)
    totalVolume += volume[i];

  /* initialize simulation */
  W = (double**)malloc(sizeof(double*)*numberOfCells);
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      W[i] = (double*)malloc(sizeof(double)*num_eqs);
  }
  calculate_conserved_vars(numberOfCells, cellFlow, W);

  if (iter > maxiter){
    info = ERR_MAXITER;
  }

  /* simulation */
  t0 = time(NULL);
  printf("       Iteration     log10(residual)\n");
  printf("      -----------   -----------------\n");
  for (i = iter; i < maxiter + 1; i++){
    residual = perform_update_RANS_uw(option, numberOfCells, neighbourCell, indx_f, totalVolume,
        Tref, Reinf, Prinf, CFL, Psi, area, volume, wallDist, faceFactor, normalVec,
        solidFaceVel, W, connecVec, cellFlow, freeFlow, saConst);
    if (iter == 1)
      residual0 = residual;
    residual /= residual0;
    fprintf(fres, "%d  %lg\n", iter++, residual);
    if (i == 1 || i % 10 == 0)
      printf("       %6d               %+.2lf\n", i, log10(residual));
    fflush(fres);
    if (isnan(residual) || isinf(residual)){
      info = ERR_RESIDUAL;
      break;
    }
    if (log10(residual) < eps_res){
      info = CONVERGED_SOLUTION;
      iter = 1;
      break;
    }
    t1 = time(NULL);
    if ((t1 - t0) / (60 * isave) > 0){
      t0 = t1;
      info = NEED_TO_SAVE;
      break;
    }
  }
  if (iter > maxiter){
    info = ERR_MAXITER;
    iter = 1;
  }

#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      free(W[i]);
  }
  free(W);
  free(faceFactor);

  return info;
}


/**
 * Solver Euler-UW.
 */
int solver_Euler_uw(
  const int option,
  const int iSave,
  const int maxiter,
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double eps_res,
  const double CFL,
  const double *area,
  const double *volume,
  double **normalVec,
  double ***connecVec,
  const flow *freeFlow,
  flow *cellFlow,
  FILE *fres
  )
{
  int i, info;
  const int num_eqs = 4; // continuity (1), momentum (2) and energy (1)
  static int iter = 1;
  time_t t0, t1;
  double *faceFactor = NULL, **W = NULL;
  static double residual0 = 1.0;
  double residual, totalVolume;

  faceFactor = (double*)malloc(sizeof(double)*(indx_f->numberOfFaces - indx_f->inner));
  for (i = 0; i < indx_f->numberOfFaces; i++){
    faceFactor[i] = sqrt(pow(normalVec[i][0], 2.0) + pow(normalVec[i][1], 2.0));
    //faceFactor[i] = 1.0;
  }

  totalVolume = 0.0;
  for (i = 0; i < numberOfCells; i++)
    totalVolume += volume[i];

  /* initialize simulation */
  W = (double**)malloc(sizeof(double*)*numberOfCells);
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      W[i] = (double*)malloc(sizeof(double)*num_eqs);
  }
  calculate_conserved_vars(numberOfCells, cellFlow, W);

  if (iter > maxiter){
    info = ERR_MAXITER;
  }

  /* simulation */
  t0 = time(NULL);
  printf("       Iteration     log10(residual)\n");
  printf("      -----------   -----------------\n");
  for (i = iter; i < maxiter + 1; i++){
    residual = perform_update_Euler_uw(option, numberOfCells, neighbourCell, indx_f, totalVolume,
        CFL, area, volume, faceFactor, normalVec, W, connecVec, cellFlow, freeFlow);
    if (iter == 1)
      residual0 = residual;
    residual /= residual0;
    fprintf(fres, "%d  %lg\n", iter++, residual);
    if (i == 1 || i % 10 == 0)
      printf("       %6d               %+.2lf\n", i, log10(residual));
    fflush(fres);
    if (isnan(residual) || isinf(residual)){
      info = ERR_RESIDUAL;
      break;
    }
    if (log10(residual) < eps_res){
      info = CONVERGED_SOLUTION;
      iter = 1;
      break;
    }
    t1 = time(NULL);
    if ((t1 - t0) / (60 * iSave) > 0){
      t0 = t1;
      info = NEED_TO_SAVE;
      break;
    }
  }
  if (iter > maxiter){
    info = ERR_MAXITER;
    iter = 1;
  }

#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++)
      free(W[i]);
  }
  free(W);
  free(faceFactor);

  return info;
}
