/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "main_headers.h"
#include "geometry.h"
#include "inout.h"
#include "jst_solvers.h"
#include "uw_solvers.h"
#include "misc.h"
#include "common.h"

int numberOfNodes, numberOfInitialNodes, numberOfFaces, numberOfInitialEdges, numberOfCells, totalNumCells, numberOfInitialFaces;
int meshVersion, isPeriodicMesh, isViscous, iSave, maxiter, maxiter0, periodic, flag_dim, seed, seed0, seedInviscidVortex;
int calculate_initial, numberOfTimeSteps, stencil, uw_option, physModel, seedSodProblem;
int numberOfSolidTypes, *solidTypes;
int *numberOfFaceVert, *numberOfCellVert, **edgeVert, **faceVert, **cellVert, **neighbourFace, **neighbourCell;
int _numberOfNodes, _numberOfFaces, _numberOfCells;
int *_numberOfFaceVert, **_faceVert, **_neighbourCell;
double eps_res, eps_res0, tstretch, aoa, CFL, k2, k4, charlen, Minf, Prinf, Reinf, sosinf, sosref;
double *length, *area, *area0, *volume, *x, *y, *t, *vx, *vy, *Psi, *_Psi, *wallDist, *_wallDist;
double *xdim, *ydim, *tdim, *_x, *_y, *_t, *_area, *_volume, **_normalVec;
double **normalVecInit, **normalVec, **cellCentre, **_cellCentre, **faceCentre, **_faceCentre, **edgeCentre0, **solidFaceVel, **_solidFaceVel;
double ***connecVec, ***_connecVec;
turb saConst;
flow refFlow, freeFlow, *cellFlow, *_cellFlow, *iniFlow;

char out_file[FILENAME_MAX], seed_file[FILENAME_MAX], seed0_file[FILENAME_MAX], mesh_file[FILENAME_MAX];


/*****************************************************************************/
/*                                                                           */
/*  print_help()   Print out the help for usage.                             */
/*                                                                           */
/*****************************************************************************/

static void print_help(char *programName){
  fprintf(stdout, "\nDESCRIPTION: 2D Euler/Navier-Stokes Spacetime Solver (parallel, unstructured)\n");
  fprintf(stdout, "AUTHOR: imanol.flamarique@bristol.ac.uk (University of Bristol, United Kingdom)\n\n");
  fprintf(stdout, "USAGE: %s STENCIL PHYSICAL_MODEL [OPTION(s)] SOURCE [-o DEST]\n\n", programName);
  fprintf(stdout, "STENCIL(s):\n");
  fprintf(stdout, "  JST         Central-Difference (or Jameson-Schmidt-Turkel)\n");
  fprintf(stdout, "  UW_VL       Upwind via Van Leer's flux-vector splitting\n");
  fprintf(stdout, "  UW_Roe      Upwind via Roe flux matrix\n\n");
  fprintf(stdout, "PHYSICAL_MODEL(s):\n");
  fprintf(stdout, "  Euler       Euler equations of motion for INVISCID (compressible) flows.\n");
  fprintf(stdout, "  RANS        Reynolds-Averaged-Navier-Stokes equations of motion for\n");
  fprintf(stdout, "              VISCOUS (compressible) flows.\n\n");
  fprintf(stdout, "OPTION(s):\n");
  fprintf(stdout, "  -a          Angle of attack (in deg) [default = %lg]\n", aoa);
  fprintf(stdout, "  -B          Do NOT solve initial solution [solved by default]\n");
  fprintf(stdout, "  -CFL        Courant-Friedrichs-Lewy condition [default = %lg]\n", CFL);
  fprintf(stdout, "  -d          Freestream density [default = %lg]\n", refFlow.rho);
  fprintf(stdout, "  -dim        Use dimensional values for simulation [default = non-dimensional]\n");
  fprintf(stdout, "  -i          Maximum number of iterations in pseudo-time [default = %d]\n", maxiter);
  fprintf(stdout, "  -i0         Maximum number of iterations in pseudo-time for initial solver [default = %d]\n", maxiter0);
  fprintf(stdout, "  -k2         Constant k2 of the JST dissipation model [default = %lg]\n", k2);
  fprintf(stdout, "  -k4         Constant k4 of the JST dissipation model [default = %lg]\n", k4);
  fprintf(stdout, "  -kt         Stretching of the grid in the time direction [default = %lg]\n", tstretch);
  fprintf(stdout, "  -L          Characteristic length [default = %lg]\n", charlen);
  fprintf(stdout, "  -M          Freestream Mach number [default = %lg]\n", Minf);
  fprintf(stdout, "  -p          Freestream pressure [default = %lg]\n", refFlow.p);
  fprintf(stdout, "  -P          Periodic problem [default = non-periodic]\n");
  fprintf(stdout, "  -Pr         Freestream Prandtl number [default = %lg]\n", Prinf);
  fprintf(stdout, "  -PrT        Freestream turbulent Prandtl number [default = %lg]\n", saConst.Prturb);
  fprintf(stdout, "  -Re         Freestream Reynolds number [default = %lg]\n", Reinf);
  fprintf(stdout, "  -res        Residuals criterion of convergence. The simulation will finish as soon as the residuals meet\n");
  fprintf(stdout, "              this criterion of convergence [default = 1.0e%+lg]\n", eps_res);
  fprintf(stdout, "  -res0       Residuals criterion of convergence for initial 2D solution. The simulation will finish as soon\n");
  fprintf(stdout, "              as the residuals meet this criterion of convergence [default = 1.0e%+lg]\n", eps_res0);
  fprintf(stdout, "  -seed       Seeding spacetime solution filename\n");
  fprintf(stdout, "  -seed0      Seeding initial solution filename or \"inviscid_vortex\" for 2D isentropic inviscid vortex\n");
  fprintf(stdout, "              or \"sod_problem\" for Sod / shock tube problem\n");
  fprintf(stdout, "  -S          Save results interval (in minutes) [default = %d]\n", iSave);
  fprintf(stdout, "  -h          Display this help\n\n");
  fprintf(stdout, "Examples:  %s JST -q -M 0.85 naca0012 -o transonic_case\n", programName);
  fprintf(stdout, "           %s UW_VL -P -d 1.225 mesh\n", programName);
  fprintf(stdout, "           %s UW_Roe mesh\n\n", programName);

  exit(EXIT_FAILURE);
}


/*****************************************************************************/
/*                                                                           */
/*  parse_args()              Parses the arguments given as inputs           */
/*                                                                           */
/*****************************************************************************/

static void parse_args(int argc, char **argv){
  int k, flag, finish;
  double chi;
  char logfile[FILENAME_MAX];
  FILE *fm;

  refFlow.p = 101325.0;
  refFlow.rho = 1.225;
  Minf = 0.5;
  Prinf = 0.72;
  charlen = 1.0;
  Reinf = 1.0e+6;
  eps_res = -5.0;
  eps_res0 = -5.0;
  aoa = 0.0;
  CFL = 2.0;
  k2 = 1.0;
  k4 = 1 / 20.0;
  maxiter = 100000;
  maxiter0 = 50000;
  iSave = 60;
  strcpy(out_file, "solution");
  periodic = NO;
  tstretch = 1.0;
  charlen = 1.0;
  flag_dim = 0;
  numberOfTimeSteps = 0;
  stencil = CENTRAL_DIFFERENCE;

  /* define turbulence model constants */
  saConst.Prturb = 0.9;
  saConst.cb1 = 0.1355;
  saConst.cb2 = 0.622;
  saConst.sigma = 2.0 / 3.0;
  saConst.k = 0.41;
  saConst.k_2 = pow(saConst.k, 2.0);
  saConst.cw1 = (saConst.cb1 / saConst.k_2) + ((1.0 + saConst.cb2) / saConst.sigma);
  saConst.cw2 = 0.3;
  saConst.cw3 = 2.0;
  saConst.cw3_6 = pow(saConst.cw3, 6.0);
  saConst.cv1 = 7.1;
  saConst.cv1_3 = pow(saConst.cv1, 3.0);
  saConst.cv2 = 0.7;
  saConst.cv3 = 0.9;
  saConst.ct3 = 1.2;
  saConst.ct4 = 0.5;
  saConst.cn1 = 16.0;
  /* end of turbulence model constant definition */

  seed = seed0 = seedInviscidVortex = seedSodProblem = NO;
  calculate_initial = YES;
  flag = finish = 0;

  if (argc == 1){
    print_help(*argv);
  }
  else if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0){
    print_help(*argv);
  }

  if (strcmp(argv[1], "JST") == 0){
    stencil = CENTRAL_DIFFERENCE;
  }
  else if (strcmp(argv[1], "UW_VL") == 0){
    stencil = UPWIND;
    uw_option = VAN_LEER;
  }
  else if (strcmp(argv[1], "UW_Roe") == 0){
    stencil = UPWIND;
    uw_option = ROE;
  }
  else {
    err_init("unknown STENCIL. For more info on available stencils type '%s -h'", argv[0]);
  }

  if (strcmp(argv[2], "Euler") == 0){
    physModel = EULER;
  }
  else if (strcmp(argv[2], "RANS") == 0){
    physModel = RANS;
  }
  else {
    err_init("unknown PHYSICAL_MODEL. For more info on available physical models type '%s -h'", argv[0]);
  }

  for (k = 3; k<argc; k++){
    if ((flag && strcmp(argv[k], "-o") != 0) || finish){
      err_init("Too many arguments");
    }
    else if (strcmp(argv[k], "-a") == 0){
      k++;
      aoa = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-B") == 0){
      calculate_initial = NO;
    }
    else if (strcmp(argv[k], "-CFL") == 0){
      k++;
      CFL = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-d") == 0){
      k++;
      refFlow.rho = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-dim") == 0){
      flag_dim = 1;
    }
    else if (strcmp(argv[k], "-i") == 0){
      k++;
      maxiter = atoi(argv[k]);
    }
    else if (strcmp(argv[k], "-i0") == 0){
      k++;
      maxiter0 = atoi(argv[k]);
    }
    else if (strcmp(argv[k], "-k2") == 0){
      k++;
      k2 = atof(argv[k]);;
    }
    else if (strcmp(argv[k], "-k4") == 0){
      k++;
      k4 = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-kt") == 0){
      k++;
      tstretch = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-L") == 0){
      k++;
      charlen = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-M") == 0){
      k++;
      Minf = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-Re") == 0){
      k++;
      Reinf = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-p") == 0){
      k++;
      refFlow.p = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-P") == 0){
      periodic = 1;
    }
    else if (strcmp(argv[k], "-Pr") == 0){
      k++;
      Prinf = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-PrT") == 0){
      k++;
      saConst.Prturb = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-res") == 0){
      k++;
      eps_res = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-res0") == 0){
      k++;
      eps_res0 = atof(argv[k]);
    }
    else if (strcmp(argv[k], "-seed") == 0){
      seed = YES;
      k++;
      strcpy(seed_file, argv[k]);
    }
    else if (strcmp(argv[k], "-seed0") == 0){
      k++;
      strcpy(seed0_file, argv[k]);
      if (strcmp(seed0_file, "inviscid_vortex") == 0)
      {
        seedInviscidVortex = YES;
      }
      else if (strcmp(seed0_file, "sod_problem") == 0)
      {
        seedSodProblem = YES;
      }
      else
      {
        seed0 = YES;
      }
    }
    else if (strcmp(argv[k], "-S") == 0){
      k++;
      iSave = atoi(argv[k]);
    }
    else if (strcmp(argv[k], "-ts") == 0){
      k++;
      numberOfTimeSteps = atoi(argv[k]);
    }
    else if (!flag && strcmp(argv[k], "-o") == 0){
      err_init("Mesh file not specified");
    }
    else if (strcmp(argv[k], "-o") == 0){
      k++;
      strcpy(out_file, argv[k]);
      finish = 1;
    }
    else if (argv[k][0] == '-') {
      err_init("option \"%s\" not supported", argv[k]);
    }
    else if (argv[k][0] != '-') {
      strcpy(mesh_file, argv[k]);
      fm = fopen(mesh_file, "r");
      if (!fm){
        err_init("Failed to open file \"%s\"", mesh_file);
      }
      fclose(fm);
      flag = 1;
    }
  }

  strcpy(logfile, out_file);
  strcat(logfile, ".log");
  flog = fopen(logfile, "w");

  if (fabs(charlen) < 1.0e-2){
    warning("the characteristic length used for non-dimensionalisation is too small. Using L = 1.0 instead.");
    charlen = 1.0;
  }

  if (fabs(Minf) < EPSILON){
    warning("non-dimensionalization cannot be applied for a null freestream velocity (U_freestream = 0.0); hence using dimensional values");
    flag_dim = 1;
  }

  if (seed0 || seedInviscidVortex || seedSodProblem){
    warning("initial solution will not be calculated since initial seed file was detected (%s)", seed0_file);
    calculate_initial = NO;
  }
  if (!flag){
    error("Mesh file not specified");
  }

  if (physModel == RANS)
  {
    k2 = 1.0 / 2.0;
    k4 = 1.0 / 32.0;
  }

  if (seedSodProblem)
    flag_dim = 1;

  if (!calculate_initial && !seed0 && !seedInviscidVortex && !seedSodProblem && !periodic)
    error("For non-periodic problems an initial solution is necessary (use either '-P' or 'seed0')");

  if (seedInviscidVortex)
  {
    flag_dim = 1;
    refFlow.rho = 1.0;
    refFlow.p = 1.0;
    aoa = 0.0;
    Minf = 1.0/sound_speed(refFlow.p, refFlow.rho);
  }

  sosref = sound_speed(refFlow.p, refFlow.rho);
  refFlow.u = sosref*Minf*cos(aoa*PI / 180.0);
  refFlow.v = sosref*Minf*sin(aoa*PI / 180.0);
  refFlow.e = energy(refFlow.rho, refFlow.u, refFlow.v, refFlow.p);
  refFlow.T = refFlow.p / (refFlow.rho*R_GAS);
  refFlow.mu = refFlow.rho*sosref*Minf*charlen / Reinf;
  refFlow.mubar = 5.0*refFlow.mu;
  chi = refFlow.mubar / refFlow.mu;
  refFlow.mut = refFlow.mubar * (pow(chi, 3.0) / (pow(chi, 3.0) + saConst.cv1_3));

  if (seedInviscidVortex)
    refFlow.T = 1.0;

  if (flag_dim){
    freeFlow.rho = refFlow.rho;
    freeFlow.u = refFlow.u;
    freeFlow.v = refFlow.v;
    freeFlow.p = refFlow.p;
    freeFlow.e = refFlow.e;
    freeFlow.T = refFlow.T;
    freeFlow.mu = refFlow.mu;
    freeFlow.mubar = refFlow.mubar;
    freeFlow.mut = refFlow.mut;
    sosinf = sosref;
  }
  else {
    freeFlow.rho = refFlow.rho / refFlow.rho;
    freeFlow.p = refFlow.p / (refFlow.rho*pow(sosref*Minf, 2.0));
    sosinf = sound_speed(freeFlow.p, freeFlow.rho);
    freeFlow.u = Minf*sosinf*cos(aoa*PI / 180.0);
    freeFlow.v = Minf*sosinf*sin(aoa*PI / 180.0);
    freeFlow.e = energy(freeFlow.rho, freeFlow.u, freeFlow.v, freeFlow.p);
    freeFlow.T = temperature(freeFlow.rho, freeFlow.p, Minf);
    freeFlow.mu = refFlow.mu / refFlow.mu;
    freeFlow.mubar = 5.0 * freeFlow.mu;
    chi = freeFlow.mubar / freeFlow.mu;
    freeFlow.mut = freeFlow.mubar *(pow(chi, 3.0) / (pow(chi, 3.0) + saConst.cv1_3));
  }

  saConst.far_mubar = 5.0 * freeFlow.mu;

  fprintf(stdout, "command: \"");
  fprintf(flog, "command: \"");
  for (k = 0; k < argc - 1; k++){
    fprintf(stdout, "%s ", argv[k]);
    fprintf(flog, "%s ", argv[k]);
  }
  fprintf(stdout, "%s\"\n", argv[k]);
  fprintf(flog, "%s\"\n", argv[k]);


}


/*****************************************************************************/
/*                                                                           */
/*  print_header()     Prints out the header of the simulation               */
/*                                                                           */
/*****************************************************************************/

static void print_header(FILE *fp){
  log_blank_line(fp);
  log_box_line(fp);
  if (physModel == RANS)
  {
    log_box_text(fp, "SPACETIME 2D NAVIER-STOKES SOLVER");
  }
  else if (physModel == EULER)
  {
    log_box_text(fp, "SPACETIME 2D EULER SOLVER")
  }
  log_box_text(fp, "(UNSTRUCTURED, PARALLEL)");
  log_box_empty(fp);
  if (stencil == CENTRAL_DIFFERENCE){
    log_box_text(fp, "Central-Difference (JST)");
  }
  else if (stencil == UPWIND){
    log_box_text(fp, "Upwind (UW)");
    if (uw_option == VAN_LEER)
    {
      log_box_text(fp, "(Van Leer flux-vector splitting)");
    }
    else if (uw_option == ROE)
    {
      log_box_text(fp, "(Roe matrix)");
    }
  }
  log_box_empty(fp);
  log_box_text(fp, "BETA Version 0.8");
  log_box_text(fp, "(9th October 2018)");
  log_box_empty(fp);
  log_box_text(fp, "imanol.flamarique@bristol.ac.uk");
  log_box_line(fp);
  log_blank_line(fp);
  log_box_line(fp);
  log_box_text(fp, "*** VERY IMPORTANT ***");
  log_box_line(fp);
  log_box_text(fp, "PLEASE BE AWARE THAT THIS CODE IS STILL UNDER DEVELOPMENT (BETA VERSION)");
  log_box_text(fp, "AND IT MAY THEREFORE LEAD TO INACCURATE SOLUTIONS.");
  log_box_empty(fp);
  log_box_text(fp, "*** USE AT YOUR OWN RISK ***");
  log_box_line(fp);
  log_blank_line(fp);
  log_box_line(fp);
  if (flag_dim){
    log_box_text(fp, "FREESTREAM CONDITIONS");
  }
  else {
    log_box_text(fp, "FREESTREAM CONDITIONS (*)");
  }
  log_box_line(fp);
  log_setup_float(fp, "Density", freeFlow.rho);
  log_setup_float(fp, "Pressure", freeFlow.p);
  log_setup_float(fp, "Temperature", freeFlow.T);
  log_setup_float(fp, "Mach number", Minf);
  if (physModel == RANS)
  {
    log_setup_float(fp, "Reynolds number", Reinf);
    log_setup_float(fp, "Prandtl number", Prinf);
    log_setup_float(fp, "Turbulent Prandtl number", saConst.Prturb);
    log_setup_float(fp, "Dynamic viscosity", freeFlow.mu);
  }
  log_setup_float(fp, "Heat capacity ratio GAMMA", GAMMA);
  log_setup_float(fp, "Specific gas constant R", R_GAS);
  log_setup_float(fp, "Angle of attack [deg]", aoa);
  log_setup_float(fp, "Velocity magnitude", Minf*sosinf);
  log_setup_float(fp, "Velocity U (horizontal)", freeFlow.u);
  log_setup_float(fp, "Velocity V (vertical)", freeFlow.v);
  log_blank_line(fp);
  if (!flag_dim){
    fprintf(fp, " (*) dimensionless quantities unless stated\n\n");
  }
  log_box_line(fp);
  if (flag_dim){
    log_box_text(fp, "SOLVER SETTINGS");
  }
  else {
    log_box_text(fp, "SOLVER SETTINGS (*)");
  }
  log_box_line(fp);
  log_setup_float(fp, "CFL condition", CFL);
  if (stencil == CENTRAL_DIFFERENCE){
    log_setup_float(fp, "k2 constant (num. diss)", k2);
    log_setup_float(fp, "k4 constant (num. diss)", k4);
  }
  log_setup_text(fp, "Time direction", "Z-axis");
  log_setup_float(fp, "Stretching in time", tstretch);
  if (periodic){
    log_setup_text(fp, "Periodic problem", "YES");
  }
  else {
    log_setup_text(fp, "Periodic problem", "NO");
  }
  log_setup_int(fp, "Max number of iterations", maxiter);
  log_setup_int(fp, "Max number of iterations initial", maxiter0);
  log_setup_int(fp, "Save interval [min]", iSave);
  log_setup_exp(fp, "Convergence criterion of unsteady solution", eps_res);
  if (calculate_initial){
    log_setup_text(fp, "Calculate initial solution", "YES");
    log_setup_exp(fp, "Convergence criterion of initial solution", eps_res0);
  }
  else {
    log_setup_text(fp, "Calculate initial solution", "NO");
  }

  if (seed0 || seedInviscidVortex || seedSodProblem)
  {
    log_setup_text(fp, "Seeding initial solution", "YES");
    if (seed0)
    {
      log_setup_text(fp, "Initial seed file", seed0_file);
    }
    else if (seedInviscidVortex)
    {
      log_setup_text(fp, "Initial seed", "2D isentropic inviscid vortex");
    }
    else if (seedSodProblem)
    {
      log_setup_text(fp, "Initial seed", "Sod / shock tube problem");
    }
  }
  else {
    log_setup_text(fp, "Seeding initial solution", "NO");
  }
  if (seed){
    log_setup_text(fp, "Seeding spacetime solution", "YES");
    log_setup_text(fp, "Spacetime seed file", seed_file);
  }
  else {
    log_setup_text(fp, "Seeding spacetime solution", "NO");
  }

  log_blank_line(fp);
  if (!flag_dim){
    fprintf(fp, " (*) dimensionless quantities unless stated\n\n");
  }
  if (!flag_dim){
    log_box_line(fp);
    log_box_text(fp, "CHARACTERISTIC VALUES FOR NON-DIMENSIONALISATION");
    log_box_line(fp);
    log_setup_float(fp, "Density [kg/m3]", refFlow.rho);
    log_setup_float(fp, "Pressure [Pa]", refFlow.p);
    log_setup_float(fp, "Temperature [K]", refFlow.T);
    log_setup_float(fp, "Length [m]", charlen);
    log_setup_float(fp, "Velocity [m/s]", Minf*sosref);
    if (physModel == RANS)
    {
      log_setup_float(fp, "Dynamic viscosity [m2/s]", refFlow.mu);
      log_setup_float(fp, "Kinematic viscosity [Pa.s]", refFlow.mu / refFlow.rho);
    }
    log_blank_line(fp);
  }
  
  log_blank_line(fp);
}


static void initialize_sim(
  const face_map *_indx_f_,
  const int _numberOfCells_,
  int **_neighbourCell_,
  flow **_cellFlow_,
  double **_normalVec_,
  double **_solidFaceVel_
  )
{
  int i, m;

  totalNumCells = _numberOfCells_ + _indx_f_->numberOfFaces - _indx_f_->initial;
  m = _numberOfCells_;
  for (i = _indx_f_->initial; i < _indx_f_->numberOfFaces; i++){
    _neighbourCell_[i][LEFT] = m++;
  }
  if (m != totalNumCells){
    error("Wrong number of halo cells");
  }

  verbose("Initialising spacetime fluid variables from freestream values");
  (*_cellFlow_) = (flow*)malloc(sizeof(flow)*totalNumCells);
  for (i = 0; i < totalNumCells; i++){
    (*_cellFlow_)[i].rho = freeFlow.rho;
    (*_cellFlow_)[i].u = freeFlow.u;
    (*_cellFlow_)[i].v = freeFlow.v;
    (*_cellFlow_)[i].p = freeFlow.p;
    /* just to avoid memcheck error (valgrind) */
    (*_cellFlow_)[i].e = 0.0;
    (*_cellFlow_)[i].T = 0.0;
    (*_cellFlow_)[i].mu = 0.0;
    (*_cellFlow_)[i].mubar = 0.0;
    (*_cellFlow_)[i].mut = 0.0;
    /* end of section (valgrind) */
  }

  if (physModel == EULER)
  {
    calculate_halo_cells_Euler(sosinf, Minf, _normalVec_, _neighbourCell_, _indx_f_, &freeFlow, (*_cellFlow_));
    construct_augmented_state_Euler(totalNumCells, (*_cellFlow_));
  }
  else if (physModel == RANS)
  {
    for (i = 0; i < totalNumCells; i++)
      (*_cellFlow_)[i].mubar = freeFlow.mubar;
    calculate_halo_cells_RANS(sosinf, Minf, _normalVec_, _solidFaceVel_, _neighbourCell_, _indx_f_,
        &freeFlow, &saConst, (*_cellFlow_));
    construct_augmented_state_RANS(totalNumCells, Minf, refFlow.T, &saConst, (*_cellFlow_));
  }
}

static void transfer_initial_condition_to_spacetime_solution(const int _numberOfCells_,
  const face_map *_indx_f_, const flow *_iniFlow_, flow *_stFlow_)
{
  int i, ii, m;

  verbose("Transferring initial solution to spacetime solution");
  for (i = _indx_f_->initial; i < _indx_f_->solid; i++)
  {
    ii = i - _indx_f_->initial;
    m = _numberOfCells_ + ii;
    _stFlow_[m].rho = _iniFlow_[ii].rho;
    _stFlow_[m].u = _iniFlow_[ii].u;
    _stFlow_[m].v = _iniFlow_[ii].v;
    _stFlow_[m].p = _iniFlow_[ii].p;
    _stFlow_[m].e = _iniFlow_[ii].e;
    _stFlow_[m].T = _iniFlow_[ii].T;
    _stFlow_[m].mu = _iniFlow_[ii].mu;
    _stFlow_[m].mut = _iniFlow_[ii].mut;
    _stFlow_[m].mubar = _iniFlow_[ii].mubar;
  }
}

static void print_elapsed_time(time_t t0)
{
  char *message;

  message = get_time_message(t0);
  fprintf(flog, "[+] %s\n", message);
  fprintf(stdout, "[+] %s\n", message);
  free(message);
}


static int compute_solution(const char *output, const face_map *_indx_f_, const face_map *_indx_f_plot_,
    const int _maxiter_, const int _numberOfNodes_, const int _numberOfCells_,
    const int *_numberOfFaceVert_, int **_faceVert_, int **_neighbourCell_,
    const double _eps_res_, const double *_Psi_, const double *_area_,
    const double *_volume_, const double *_wallDist_, const double *_x_,
    const double *_y_, const double *_t_, double **_normalVec_,
    double **_solidFaceVel_, double ***_connecVec_, flow *_cellFlow_)
{
  int state = EXIT_SUCCESS, keep_running, k = 1;
  char fname[FILENAME_MAX], numstr[FILENAME_MAX], fresidual[FILENAME_MAX];
  FILE *fres;

  strcpy(fresidual, output);
  strcat(fresidual, ".res.plt");
  fres = fopen(fresidual, "w");
  fprintf(fres, "VARIABLES = \"n\", \"R<sup>n</sup>\"\n");
  keep_running = YES;

  while (keep_running){
    if (stencil == CENTRAL_DIFFERENCE)
    {
      if (physModel == RANS)
        state = solver_RANS_jst(iSave, _maxiter_, _numberOfCells_, _indx_f_, _neighbourCell_, _eps_res_, refFlow.T,
          Reinf, Prinf, CFL, k2, k4, _Psi_, _area_, _volume_, _wallDist_, _normalVec_, _solidFaceVel_,
          _connecVec_, &freeFlow, _cellFlow_, &saConst, fres);
      else if (physModel == EULER)
        state = solver_Euler_jst(iSave, _maxiter_, _numberOfCells_, _indx_f_, _neighbourCell_,
            _eps_res_, CFL, k2, k4, _area_, _volume_, _normalVec_, &freeFlow, _cellFlow_, fres);
    }
    else if (stencil == UPWIND)
    {
      if (physModel == RANS)
        state = solver_RANS_uw(uw_option, iSave, _maxiter_, _numberOfCells_, _indx_f_, _neighbourCell_,
          _eps_res_, refFlow.T, Reinf, Prinf, CFL, _Psi_, _area_, _volume_, _wallDist_,
          _normalVec_, _solidFaceVel_, _connecVec_, &freeFlow, _cellFlow_, &saConst, fres);
      else if (physModel == EULER)
        state = solver_Euler_uw(uw_option, iSave, _maxiter_, _numberOfCells_, _indx_f_, _neighbourCell_, _eps_res_,
            CFL, _area_, _volume_, _normalVec_, _connecVec_, &freeFlow, _cellFlow_, fres);
    }
    if (state != ERR_RESIDUAL){
      strcpy(fname, output);
      strcat(fname, ".seed");
      save_seed(_numberOfCells_, fname, _cellFlow_);
    }
    strcpy(fname, output);
    switch (state)
    {
    case CONVERGED_SOLUTION:
      verbose("Successfully converged solution");
      keep_running = NO;
      break;
    case NEED_TO_SAVE:
      break;
    case ERR_MAXITER:
      warning("Exiting simulation because number of iterations exceeded the maximum value");
      keep_running = NO;
      break;
    case ERR_RESIDUAL:
      warning("Simulation crashed :(");
      keep_running = NO;
      break;
    default:
      break;
    }
    if (state == NEED_TO_SAVE){
      strcat(fname, ".temp.");
      sprintf(numstr, "%d", k++);
      strcat(fname, numstr);
    }
    else if (state == ERR_RESIDUAL){
      strcat(fname, ".err.");
      sprintf(numstr, "%d", k++);
      strcat(fname, numstr);
    }
    else if (state == ERR_MAXITER){
          strcat(fname, ".best.");
          sprintf(numstr, "%d", k++);
          strcat(fname, numstr);
        }
    if (physModel == RANS)
    {
      save_3d_solution(_numberOfNodes_, _numberOfCells_, _numberOfFaceVert_, _faceVert_,
          fname, _x_, _y_, _t_, _wallDist_, _cellFlow_, _indx_f_plot_, _neighbourCell_);
      save_y_plus_QUAD(_numberOfNodes_, _faceVert_, _neighbourCell_, output, Reinf,
          _x_, _y_, _t_, _wallDist_, _normalVec_, _cellFlow_, _indx_f_plot_);
    }
    else if (physModel == EULER)
    {
      save_3d_Euler(_numberOfNodes_, _numberOfCells_, _numberOfFaceVert_, _faceVert_, fname,
          _x_, _y_, _t_, _cellFlow_, _indx_f_plot_, _neighbourCell_);
      save_solid_surfaces_Euler(_numberOfNodes_, _numberOfFaceVert_, _faceVert_, fname,
          _x_, _y_, _t_, _cellFlow_, _indx_f_plot_, _neighbourCell_);
    }
    if (k > MAX_NUM_SOLUTION_FILES){
      k = 1;
    }
  }

  return state;
}



/*****************************************************************************/
/*                                                                           */
/*  main()                                                                   */
/*                                                                           */
/*****************************************************************************/

int main(int argc, char **argv)
{
  time_t t0;
  int state, j, jj, totNumCell;
  face_map *indx_f_plot, *indx_f0, *indx_f, *_indx_f_plot, *_indx_f;
  char out_file0[FILENAME_MAX];

  t0 = time(NULL);

  parse_args(argc, argv);

  indx_f_plot = (face_map*)malloc(sizeof(face_map));
  read_mesh_3d_ASCII(mesh_file, &numberOfNodes, &numberOfFaces, &numberOfCells,
      &numberOfSolidTypes, &solidTypes, &numberOfFaceVert, &faceVert, &neighbourCell,
      &normalVec, &area, &xdim, &ydim, &tdim, 3, indx_f_plot);

  print_header(flog);
  print_header(stdout);

  if (tstretch != 1.0){
    stretch_grid(numberOfNodes, tstretch, tdim);
  }

  if (flag_dim){
    x = xdim;
    y = ydim;
    t = tdim;
  }
  else {
    x = non_dim_x(numberOfNodes, xdim, charlen);
    y = non_dim_x(numberOfNodes, ydim, charlen);
    t = non_dim_x(numberOfNodes, tdim, charlen / (sosref*Minf));
  }

  verbose("Calculating faces (normal vector, area and centre)");
  calculate_faces_3d(numberOfFaces, numberOfFaceVert, faceVert, normalVec, &faceCentre, area, x, y, t);

  verbose("Calculating cells (volume and centre)");
  cellCentre = get_cell_centres_3d (indx_f_plot, numberOfNodes, numberOfCells, numberOfFaceVert, neighbourCell,
    faceVert, x, y, t);
  volume = get_cell_volumes_3d (indx_f_plot, numberOfCells, neighbourCell, &connecVec, faceCentre, cellCentre,
    normalVec, area);

  verbose("Checking normal vectors");
  orient_normal_vectors_3d(indx_f_plot, neighbourCell, cellCentre, faceCentre, normalVec);

  print_mesh_statistics(flog, numberOfNodes, numberOfCells, indx_f_plot);
  print_mesh_statistics(stdout, numberOfNodes, numberOfCells, indx_f_plot);

  //########################## INITIAL SOLUTION ################################
  indx_f0 = (face_map*)malloc(sizeof(face_map));

  if (calculate_initial){
    strcpy(out_file0, out_file);
    strcat(out_file0, ".0");

    verbose("Getting initial 2D geometry");

    get_initial_2d_geometry(indx_f_plot, indx_f0, numberOfNodes, &numberOfInitialEdges, numberOfFaceVert,
        faceVert, &neighbourFace, &edgeVert);

    verbose("Obtaining a spacetime geometry from the initial one");
    get_st_initial(numberOfNodes, indx_f0, indx_f_plot, numberOfFaceVert, edgeVert, faceVert,
        neighbourFace, x, y, &_numberOfNodes, &_numberOfFaces, &_numberOfCells, &_indx_f_plot,
        &_numberOfFaceVert, &_faceVert, &_neighbourCell, &_x, &_y, &_t);

    verbose("Initial geometry: calculating faces (normal vector, area and centre)");
    _normalVec = (double**)malloc(sizeof(double*)*_numberOfFaces);
    for (j = 0; j < _numberOfFaces; j++)
      _normalVec[j] = (double*)malloc(sizeof(double)* 3);
    _area = (double*)malloc(sizeof(double)*_numberOfFaces);
    calculate_faces_3d(_numberOfFaces, _numberOfFaceVert, _faceVert, _normalVec,
        &_faceCentre, _area, _x, _y, _t);

    verbose("Initial geometry: calculating cells (volume and centre)");
    _cellCentre = get_cell_centres_3d(_indx_f_plot, _numberOfNodes, _numberOfCells,
        _numberOfFaceVert, _neighbourCell, _faceVert, _x, _y, _t);
    _volume = get_cell_volumes_3d(_indx_f_plot, _numberOfCells, _neighbourCell,
        &_connecVec, _faceCentre, _cellCentre, _normalVec, _area);

    verbose("Initial geometry: checking normal vectors");
    orient_normal_vectors_3d(_indx_f_plot, _neighbourCell, _cellCentre, _faceCentre, _normalVec);

    print_mesh_statistics(flog, _numberOfNodes, _numberOfCells, indx_f0);
    print_mesh_statistics(stdout, _numberOfNodes, _numberOfCells, indx_f0);

    if (physModel == RANS)
      _Psi = get_viscous_psi_HEXA(_indx_f_plot, _numberOfCells, _numberOfFaceVert, _neighbourCell, _faceVert, _x, _y,
        _t);

    verbose("Initial geometry: get periodic connectivity");
    _indx_f = get_periodic_connectivity(_indx_f_plot, _neighbourCell, _connecVec);

    for (j = _indx_f_plot->final; j < _indx_f_plot->numberOfFaces; j++)
      free(_faceCentre[j]);
    _faceCentre = (double**)realloc(_faceCentre, sizeof(double*)*_indx_f->numberOfFaces);

    if (physModel == RANS)
    {
      _wallDist = get_distance_to_wall(_indx_f, _numberOfCells, _area, _faceCentre, _cellCentre);
      _solidFaceVel = (double**)malloc(sizeof(double*)*(_indx_f->farfield - _indx_f->solid));
      for (j = _indx_f->solid; j < _indx_f->farfield; j++){
        jj = j - _indx_f->solid;
        _solidFaceVel[jj] = (double*)malloc(sizeof(double)* 2);
        _solidFaceVel[jj][0] = _solidFaceVel[jj][1] = 0.0;
      }
    }

    initialize_sim(_indx_f, _numberOfCells, _neighbourCell, &_cellFlow, _normalVec, _solidFaceVel);

    verbose("Initial solution: starting spacetime simulation");
    state = compute_solution(out_file0, _indx_f, _indx_f_plot, maxiter0, _numberOfNodes, _numberOfCells,
        _numberOfFaceVert, _faceVert, _neighbourCell, eps_res0, _Psi, _area, _volume, _wallDist,
        _x, _y, _t, _normalVec, _solidFaceVel, _connecVec, _cellFlow);

    verbose("Successfully calculated initial solution");
  }
  else if (seed0 || seedSodProblem)
  {
    get_initial_2d_geometry(indx_f_plot, indx_f0, numberOfNodes, &numberOfInitialEdges, numberOfFaceVert,
        faceVert, &neighbourFace, &edgeVert);
    _numberOfCells = indx_f_plot->solid - indx_f_plot->initial;

    if (seed0)
    {
      verbose("Initial solution: seeding spacetime fluid variables from file");
      _cellFlow = (flow*)read_seed(_numberOfCells, _numberOfCells, seed0_file);
    }
    else if (seedSodProblem)
    {
      verbose("Initial solution: seeding Sod / shock tube problem");
      flow leftState, rightState;
      leftState.rho = 1.0;
      leftState.p = 1.0;
      leftState.u = leftState.v = 0.0;
      leftState.e = energy(leftState.rho, leftState.u, leftState.v, leftState.p);
      rightState.rho = 0.125;
      rightState.p = 0.1;
      rightState.u = rightState.v = 0.0;
      rightState.e = energy(rightState.rho, rightState.u, rightState.v, rightState.p);
      _cellFlow = (flow*) seed_Sod_problem(_numberOfCells, _numberOfCells, indx_f_plot->initial, numberOfFaceVert,
        faceVert, x, y, &leftState, &rightState);
    }
  }
  else if (seedInviscidVortex)
  {
    _numberOfCells = indx_f_plot->solid - indx_f_plot->initial;
    verbose("Initial solution: seeding 2D isentropic inviscid vortex");
    _cellFlow = (flow*) seed_inviscid_vortex(_numberOfCells, _numberOfCells, indx_f_plot->initial, numberOfFaceVert,
      faceVert, x, y, &freeFlow);
  }

  //####################### END OF INITIAL SOLUTION ############################

  if (physModel == RANS)
    Psi = get_viscous_psi_HEXA(indx_f_plot, numberOfCells, numberOfFaceVert, neighbourCell, faceVert, x, y, t);


  if (periodic){
    indx_f = get_periodic_connectivity(indx_f_plot, neighbourCell, connecVec);
    for (j = indx_f_plot->final; j < indx_f_plot->numberOfFaces; j++){
      free(faceCentre[j]);
    }
    faceCentre = (double**)realloc(faceCentre, sizeof(double*)*indx_f->numberOfFaces);
  }
  else {
    indx_f = indx_f_plot;
  }

  if (physModel == RANS)
  {
    warning("Wall distances calculation may be inaccurate due to unstructured in time!!!");
    wallDist = get_distance_to_wall(indx_f, numberOfCells, area, faceCentre, cellCentre);
    solidFaceVel = get_solid_face_velocities(mesh_file, indx_f_plot,
        numberOfNodes, numberOfFaceVert, faceVert);
    if (fabs(Minf) >= EPSILON)
    {
      for (j = 0; j < indx_f->farfield - indx_f->solid; j++)
        solidFaceVel[j] = non_dim_x(2, solidFaceVel[j], (sosref*Minf));
    }
  }

  if (seed)
  {
    verbose("Seeding spacetime fluid variables from file");
    totNumCell = numberOfCells + indx_f->numberOfFaces - indx_f->initial;
    cellFlow = (flow*)read_seed(numberOfCells, totNumCell, seed_file);

    if (physModel == EULER)
    {
      calculate_halo_cells_Euler(sosinf, Minf, normalVec, neighbourCell, indx_f, &freeFlow, cellFlow);
      construct_augmented_state_Euler(totNumCell, cellFlow);
    }
    else if (physModel == RANS)
    {
      calculate_halo_cells_RANS(sosinf, Minf, normalVec, solidFaceVel, neighbourCell, indx_f,
          &freeFlow, &saConst, cellFlow);
      construct_augmented_state_RANS(totNumCell, Minf, refFlow.T, &saConst, cellFlow);
    }
  }
  else
  {
    initialize_sim(indx_f, numberOfCells, neighbourCell, &cellFlow, normalVec, solidFaceVel);
  }

  transfer_initial_condition_to_spacetime_solution(numberOfCells, indx_f, _cellFlow, cellFlow);

  verbose("Starting spacetime simulation");
  state = compute_solution(out_file, indx_f, indx_f_plot, maxiter, numberOfNodes, numberOfCells,
      numberOfFaceVert, faceVert, neighbourCell, eps_res, Psi, area, volume, wallDist,
      x, y, t, normalVec, solidFaceVel, connecVec, cellFlow);

  if (periodic)
    free(indx_f_plot);
  free(indx_f);
  free(indx_f0);

  print_elapsed_time(t0);

  return state;
}
