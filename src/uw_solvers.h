/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef SOLVER_UW_HEADERS_H
#define SOLVER_UW_HEADERS_H

#include "main_headers.h"


/*****************************************************************************/
/*                                                                           */
/*  solver_uw()                       Gets spacetime solution with Upwind    */
/*                                                                           */
/*****************************************************************************/

int solver_RANS_uw(
    const int option,
    const int isave,
    const int maxiter,
    const int numberOfCells,
    const face_map *indx_f,
    int **neighbourCell,
    const double eps_res,
    const double Tref,
    const double Reinf,
    const double Prinf,
    const double CFL,
    const double *Psi,
    const double *area,
    const double *volume,
    const double *wallDist,
    double **normalVec,
    double **solidFaceVel,
    double ***connecVec,
    const flow *freeFlow,
    flow *cellFlow,
    const turb *saConst,
    FILE *fres
  );


/**
 * Solver Euler-UW.
 */
int solver_Euler_uw(
  const int option,
  const int iSave,
  const int maxiter,
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  const double eps_res,
  const double CFL,
  const double *area,
  const double *volume,
  double **normalVec,
  double ***connecVec,
  const flow *freeFlow,
  flow *cellFlow,
  FILE *fres
  );


#endif
