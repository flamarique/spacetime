/*
    SpacetimeCFD is a 2D unstructured finite-volume CFD solver which uses
    a 3D spacetime formulation (2D+time) to solve unsteady problems with
    complex motions.

    Copyright (C) 2017  Imanol Flamarique Ederra

    This file is part of SpacetimeCFD.

    SpacetimeCFD is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "uw.h"
#include "common.h"

#define temperature(rho,p,Minf) (GAMMA*pow((Minf), 2.0)*(p) / (rho))
#define pressure(rho,u,v,E) ((rho)*(GAMMA - 1.0)*((E) - (pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define energy(rho,u,v,p) (((p) / ((GAMMA - 1.0)*(rho))) + ((pow((u), 2.0) + pow((v), 2.0)) / 2.0))
#define suderland(Tref) (110.0/(Tref))
#define viscosity(Tref,T) (pow((T), 1.5)*(1.0 + suderland(Tref)) / ((T) + suderland(Tref)))
#define sound_speed(p,rho) sqrt(GAMMA*(p)/(rho))

/* Global variables definition */
static int num_eqs = 4;   /* continuity (1), momentum (2) and energy (1) */
static int num_dims = 3;  /* x, y and t */
static const double RK_coeff[4] = {1.0, 2.0, 2.0, 1.0};

typedef struct ROE_AVG{
  double rho;
  double u;
  double v;
  double H; // enthalpy
  double a; // speed of sound
  double halfq; // half of q = u^2 + v^2
} Roe_avg;

/* end of global variables definition */

/*****************************************************************************/
/*                                                                           */
/*  allocate_memory()                 Allocates memory for JST solver        */
/*                                                                           */
/*****************************************************************************/

static void allocate_memory_Euler(
  const int numberOfCells,
  const face_map *indx_f,
  double **rho0,
  double **dt,
  double **specRad,
  double **specRadFace,
  double ***invflux,
  double ***w0,
  flow **faceL,
  flow **faceR
  )
{
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;

  allocate_common_memory_Euler(numberOfCells, indx_f, rho0, dt, specRad, specRadFace, w0, invflux);

  (*faceL) = (flow*)malloc(sizeof(flow)*numberOfFaces);
  (*faceR) = (flow*)malloc(sizeof(flow)*numberOfFaces);
}

/**
 * Allocates heap memory for RANS solver.
 */
static void allocate_memory_RANS(
    const int numberOfCells,
    const face_map *indx_f,
    double **rho0,
    double **dt,
    double **specRad,
    double **specRadFace,
    double **mubar0,
    double **tauxx,
    double **tauxy,
    double **tauyy,
    double **qx,
    double **qy,
    double **production,
    double **destruction,
    double **diffusion_s,
    double **diffusion_v,
    double **convection,
    double **SAflux,
    double ***w0,
    double ***invflux,
    double ***viscflux,
    flow **faceL,
    flow **faceR
  )
{
  allocate_memory_Euler(numberOfCells, indx_f, rho0, dt, specRad, specRadFace, invflux, w0, faceL, faceR);
  allocate_common_memory_RANS(numberOfCells, indx_f, mubar0, tauxx, tauxy, tauyy, qx, qy,
      production, destruction, diffusion_s, diffusion_v, convection, SAflux, viscflux);
}


/**
 * Releases heap memory for Euler solver.
 */
static void free_memory_Euler(
    const int numberOfCells,
    const int numberOfFaces,
    double *rho0,
    double *dt,
    double *specRad,
    double *specRadFace,
    double **w0,
    double **invflux,
    flow *faceL,
    flow *faceR
  )
{
  free_common_memory_Euler(numberOfCells, numberOfFaces, rho0, dt, specRad, specRadFace, w0, invflux);

  free(faceL);
  free(faceR);
}


/**
 * Releases heap memory for RANS solver.
 */
static void free_memory_RANS(
    const int numberOfCells,
    const int numberOfFaces,
    const face_map *indx_f,
    double *rho0,
    double *dt,
    double *specRad,
    double *specRadFace,
    double *mubar0,
    double *tauxx,
    double *tauxy,
    double *tauyy,
    double *qx,
    double *qy,
    double *production,
    double *destruction,
    double *diffusion_s,
    double *diffusion_v,
    double *convection,
    double *SAflux,
    double **w0,
    double **invflux,
    double **viscflux,
    flow *faceL,
    flow *faceR
  )
{
  free_memory_Euler(numberOfCells, numberOfFaces, rho0, dt, specRad, specRadFace, w0, invflux, faceL, faceR);
  free_common_memory_RANS(numberOfCells, numberOfFaces, mubar0, tauxx, tauxy, tauyy, qx, qy, production,
      destruction, diffusion_s, diffusion_v, convection, SAflux, viscflux);
}


/**
 * Helper function to work out monitors at cells.
 */
static double monitor(
  const double gradient_0,
  const double gradient_1,
  const double monitor_0
  )
{
  double mon_temp;

  if (fabs(gradient_0) > EPSILON){
    mon_temp = gradient_1 / gradient_0;
  }
  else {
    mon_temp = (gradient_1 + EPSILON) / (gradient_0 + EPSILON);
  }
  return min(mon_temp, monitor_0);
}

/**
 * Calculates monitors at cells (rho, u, v, p).
 *
 * @warning This function allocates dynamic memory for monitors. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
static void calculate_monitors(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double **grad_rho,
  double **grad_u,
  double **grad_v,
  double **grad_p,
  double ***monitor_rho,
  double ***monitor_u,
  double ***monitor_v,
  double ***monitor_p
  )
{
  int i, j, left, right;
  double **mon_rho, **mon_u, **mon_v, **mon_p;

  // allocate dynamic memory for monitors and initialize to 100.0
  mon_rho = (double**)malloc(sizeof(double*)*numberOfCells);
  mon_u = (double**)malloc(sizeof(double*)*numberOfCells);
  mon_v = (double**)malloc(sizeof(double*)*numberOfCells);
  mon_p = (double**)malloc(sizeof(double*)*numberOfCells);

#pragma omp parallel for
  for (i = 0; i < numberOfCells; i++)
  {
    mon_rho[i] = (double*) malloc(sizeof(double) * num_dims);
    mon_u[i] = (double*) malloc(sizeof(double) * num_dims);
    mon_v[i] = (double*) malloc(sizeof(double) * num_dims);
    mon_p[i] = (double*) malloc(sizeof(double) * num_dims);
    for (j = 0; j < num_dims; j++)
    {
      mon_rho[i][j] = mon_u[i][j] = mon_v[i][j] = mon_p[i][j] = 100.0;
    }
  }

  /* iterate over inner faces ONLY */
  for (i = indx_f->inner; i < indx_f->initial; i++){
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    for (j = 0; j < num_dims; j++){
      mon_rho[right][j] = monitor(grad_rho[right][j] , grad_rho[left][j]  , mon_rho[right][j]);
      mon_rho[left][j]  = monitor(grad_rho[left][j]  , grad_rho[right][j] , mon_rho[left][j]);
      mon_u[right][j]   = monitor(grad_u[right][j]   , grad_u[left][j]    , mon_u[right][j]);
      mon_u[left][j]    = monitor(grad_u[left][j]    , grad_u[right][j]   , mon_u[left][j]);
      mon_v[right][j]   = monitor(grad_v[right][j]   , grad_v[left][j]    , mon_v[right][j]);
      mon_v[left][j]    = monitor(grad_v[left][j]    , grad_v[right][j]   , mon_v[left][j]);
      mon_p[right][j]   = monitor(grad_p[right][j]   , grad_p[left][j]    , mon_p[right][j]);
      mon_p[left][j]    = monitor(grad_p[left][j]    , grad_p[right][j]   , mon_p[left][j]);
    }
  }

  /* copy pointers to monitors before exiting the function */
  (*monitor_rho) = mon_rho;
  (*monitor_u) = mon_u;
  (*monitor_v) = mon_v;
  (*monitor_p) = mon_p;
}

/**
 * Calculate limiters at cells (rho, u, v, p).
 *
 * @warning This function allocates dynamic memory for limiters. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
static void calculate_limiters(
    const int numberOfCells,
    double **mon_rho,
    double **mon_u,
    double **mon_v,
    double **mon_p,
    double ***limiter_rho,
    double ***limiter_u,
    double ***limiter_v,
    double ***limiter_p
    )
{
  int i, j;
  double **lim_rho, **lim_u, **lim_v, **lim_p;

  // allocate dynamic memory for limiters
  lim_rho = (double**)malloc(sizeof(double*)*numberOfCells);
  lim_u = (double**)malloc(sizeof(double*)*numberOfCells);
  lim_v = (double**)malloc(sizeof(double*)*numberOfCells);
  lim_p = (double**)malloc(sizeof(double*)*numberOfCells);

#pragma omp parallel for
  for (i = 0; i < numberOfCells; i++)
  {
    lim_rho[i] = (double*) malloc(sizeof(double) * num_dims);
    lim_u[i] = (double*) malloc(sizeof(double) * num_dims);
    lim_v[i] = (double*) malloc(sizeof(double) * num_dims);
    lim_p[i] = (double*) malloc(sizeof(double) * num_dims);
  }

  /* iterate over cells */
#pragma omp parallel for private(j)
  for (i = 0; i < numberOfCells; i++)
  {
    for (j = 0; j < num_dims; j++)
    {
      lim_rho[i][j] = (mon_rho[i][j] + fabs(mon_rho[i][j])) / (1.0 + fabs(mon_rho[i][j]));
      lim_u[i][j] = (mon_u[i][j] + fabs(mon_u[i][j])) / (1.0 + fabs(mon_u[i][j]));
      lim_v[i][j] = (mon_v[i][j] + fabs(mon_v[i][j])) / (1.0 + fabs(mon_v[i][j]));
      lim_p[i][j] = (mon_p[i][j] + fabs(mon_p[i][j])) / (1.0 + fabs(mon_p[i][j]));
    }
  }

  /* copy pointers to limiters before exiting the function */
  (*limiter_rho) = lim_rho;
  (*limiter_u) = lim_u;
  (*limiter_v) = lim_v;
  (*limiter_p) = lim_p;
}

/**
 * Work out face corrections from cell centred values (rho, u, v, p).
 *
 * @warning This function allocates dynamic memory for corrections. It is responsibility of the caller
 *   to release this memory when not needed anymore.
 */
static void calculate_face_corrections(
  const int numberOfCells,
  const face_map *indx_f,
  int **neighbourCell,
  double ***connecVec,
  double **grad_rho,
  double **grad_u,
  double **grad_v,
  double **grad_p,
  double ***correction_rho,
  double ***correction_u,
  double ***correction_v,
  double ***correction_p
  )
{
  int i;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  double **lim_rho = NULL, **lim_u = NULL, **lim_v = NULL, **lim_p = NULL;
  double **mon_rho = NULL, **mon_u = NULL, **mon_v = NULL, **mon_p = NULL;
  double **corr_rho = NULL, **corr_u = NULL, **corr_v = NULL, **corr_p = NULL;

  // allocate dynamic memory for corrections and initialize to 0.0
  corr_rho = (double**)malloc(sizeof(double*)*numberOfFaces);
  corr_u = (double**)malloc(sizeof(double*)*numberOfFaces);
  corr_v = (double**)malloc(sizeof(double*)*numberOfFaces);
  corr_p = (double**)malloc(sizeof(double*)*numberOfFaces);
#pragma omp parallel
  {
#pragma omp for
    for (i = indx_f->inner; i < indx_f->initial; i++){
      corr_rho[i] = (double*)calloc(2, sizeof(double));
      corr_u[i] = (double*)calloc(2, sizeof(double));
      corr_v[i] = (double*)calloc(2, sizeof(double));
      corr_p[i] = (double*)calloc(2, sizeof(double));
    }
#pragma omp for
    for (i = indx_f->initial; i < indx_f->numberOfFaces; i++){
      corr_rho[i] = (double*)calloc(1, sizeof(double));
      corr_u[i] = (double*)calloc(1, sizeof(double));
      corr_v[i] = (double*)calloc(1, sizeof(double));
      corr_p[i] = (double*)calloc(1, sizeof(double));
    }
  }

  calculate_monitors(numberOfCells, indx_f, neighbourCell, grad_rho, grad_u,
      grad_v, grad_p, &mon_rho, &mon_u, &mon_v, &mon_p);
  calculate_limiters(numberOfCells, mon_rho, mon_u, mon_v, mon_p,
      &lim_rho, &lim_u, &lim_v, &lim_p);

  /* iterate over faces */
#pragma omp parallel for
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      for (int k = 0; k < num_dims; k++)
      {
        corr_rho[i][j] += connecVec[i][j][k] * lim_rho[neighbourCell[i][j]][k] * grad_rho[neighbourCell[i][j]][k];
        corr_u[i][j] += connecVec[i][j][k] * lim_u[neighbourCell[i][j]][k] * grad_u[neighbourCell[i][j]][k];
        corr_v[i][j] += connecVec[i][j][k] * lim_v[neighbourCell[i][j]][k] * grad_v[neighbourCell[i][j]][k];
        corr_p[i][j] += connecVec[i][j][k] * lim_p[neighbourCell[i][j]][k] * grad_p[neighbourCell[i][j]][k];
      }
    }
  }

#pragma omp parallel for
  for (i = indx_f->initial; i < indx_f->numberOfFaces; i++)
  {
    for (int k = 0; k < num_dims; k++)
    {
      corr_rho[i][RIGHT] += connecVec[i][RIGHT][k] * lim_rho[neighbourCell[i][RIGHT]][k]
          * grad_rho[neighbourCell[i][RIGHT]][k];
      corr_u[i][RIGHT] += connecVec[i][RIGHT][k] * lim_u[neighbourCell[i][RIGHT]][k]
          * grad_u[neighbourCell[i][RIGHT]][k];
      corr_v[i][RIGHT] += connecVec[i][RIGHT][k] * lim_v[neighbourCell[i][RIGHT]][k]
          * grad_v[neighbourCell[i][RIGHT]][k];
      corr_p[i][RIGHT] += connecVec[i][RIGHT][k] * lim_p[neighbourCell[i][RIGHT]][k]
          * grad_p[neighbourCell[i][RIGHT]][k];
    }
  }

  /* copy corrections pointers before exiting */
  (*correction_rho) = corr_rho;
  (*correction_u) = corr_u;
  (*correction_v) = corr_v;
  (*correction_p) = corr_p;

  /* free dynamic memory for limiters and monitors*/
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfCells; i++){
      free(mon_rho[i]);
      free(mon_u[i]);
      free(mon_v[i]);
      free(mon_p[i]);
      free(lim_rho[i]);
      free(lim_u[i]);
      free(lim_v[i]);
      free(lim_p[i]);
    }
  }
  free(mon_rho);
  free(mon_u);
  free(mon_v);
  free(mon_p);
  free(lim_rho);
  free(lim_u);
  free(lim_v);
  free(lim_p);
}


/**
 * Reconstruct inner face values from cell values (rho, u, v, p).
 *
 * @warning Must call `calculate_gradients()` beforehand.
 */
static void reconstruct_inner_face_values_Euler(
    const int numberOfCells,
    const face_map *indx_f,
    int **neighbourCell,
    double ***connecVec,
    const flow *cellFlow,
    double **grad_rho,
    double **grad_u,
    double **grad_v,
    double **grad_p,
    flow *faceL,
    flow *faceR
)
{
  int i, left, right;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  double **corr_rho = NULL, **corr_u = NULL, **corr_v = NULL, **corr_p = NULL;

  // calculate face_corrections
  calculate_face_corrections(numberOfCells, indx_f, neighbourCell, connecVec, grad_rho, grad_u,
      grad_v, grad_p, &corr_rho, &corr_u, &corr_v, &corr_p);

#pragma omp parallel for private(left, right)
  for (i = indx_f->inner; i < indx_f->initial; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    /* RIGHT side (inner faces only) */
    faceR[i].rho = cellFlow[right].rho + corr_rho[i][RIGHT];
    faceR[i].u = cellFlow[right].u + corr_u[i][RIGHT];
    faceR[i].v = cellFlow[right].v + corr_v[i][RIGHT];
    faceR[i].p = cellFlow[right].p + corr_p[i][RIGHT];
    /* LEFT side (inner faces only) */
    faceL[i].rho = cellFlow[left].rho + corr_rho[i][LEFT];
    faceL[i].u = cellFlow[left].u + corr_u[i][LEFT];
    faceL[i].v = cellFlow[left].v + corr_v[i][LEFT];
    faceL[i].p = cellFlow[left].p + corr_p[i][LEFT];
  }

  // release heap memory for corrections
#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < numberOfFaces; i++){
      free(corr_rho[i]);
      free(corr_u[i]);
      free(corr_v[i]);
      free(corr_p[i]);
    }
  }
  free(corr_rho);
  free(corr_u);
  free(corr_v);
  free(corr_p);
}


/**
 * Reconstruct inner face values from cell values (rho, u, v, p, mubar).
 *
 * @warning Must call `calculate_gradients()` beforehand.
 */
static void reconstruct_inner_face_values_RANS(
    const int numberOfCells,
    const face_map *indx_f,
    int **neighbourCell,
    double ***connecVec,
    const flow *cellFlow,
    double **grad_rho,
    double **grad_u,
    double **grad_v,
    double **grad_p,
    flow *faceL,
    flow *faceR
)
{
  int left, right;

  reconstruct_inner_face_values_Euler(numberOfCells, indx_f, neighbourCell, connecVec, cellFlow,
        grad_rho, grad_u, grad_v, grad_p, faceL, faceR);

#pragma omp parallel for private(left, right)
  for (int i = indx_f->inner; i < indx_f->initial; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    faceL[i].mubar = faceR[i].mubar = 0.5 * (cellFlow[right].mubar + cellFlow[left].mubar);
  }
}


/**
 * Reconstruct boundary face values from cell values (rho, u, v, p).
 *
 * @warning Must call `calculate_hallo_cells_Euler()` beforehand.
 */
static void reconstruct_boundary_face_values_Euler(
    const face_map *indx_f,
    int **neighbourCell,
    const flow *cellFlow,
    flow *faceL,
    flow *faceR
)
{
  int left, right;

#pragma omp parallel for private(left, right)
  for (int i = indx_f->initial; i < indx_f->numberOfFaces; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    faceL[i].rho = faceR[i].rho = 0.5 * (cellFlow[right].rho + cellFlow[left].rho);
    faceL[i].u = faceR[i].u = 0.5 * (cellFlow[right].u + cellFlow[left].u);
    faceL[i].v = faceR[i].v = 0.5 * (cellFlow[right].v + cellFlow[left].v);
    faceL[i].p = faceR[i].p = 0.5 * (cellFlow[right].p + cellFlow[left].p);
  }

}


/**
 * Reconstruct boundary face values from cell values (rho, u, v, p, mubar).
 *
 * @warning Must call `calculate_hallo_cells_RANS()` beforehand.
 */
static void reconstruct_boundary_face_values_RANS(
    const face_map *indx_f,
    int **neighbourCell,
    const flow *cellFlow,
    flow *faceL,
    flow *faceR
)
{
  int left, right;
  
  reconstruct_boundary_face_values_Euler(indx_f, neighbourCell, cellFlow, faceL, faceR);

#pragma omp parallel for private(left, right)
  for (int i = indx_f->solid; i < indx_f->numberOfFaces; i++)
  {
    left = neighbourCell[i][LEFT];
    right = neighbourCell[i][RIGHT];
    faceL[i].mubar = faceR[i].mubar = 0.5 * (cellFlow[right].mubar + cellFlow[left].mubar);
  }
}

/**
 * Transforms fluxes from local to global csys.
 */
static void local_to_global_csys(
  const int faceId,
  const double snorm_x,
  const double snorm,
  const double vanLeerLocalFlux[5],
  double **normalVec,
  double vanLeerGlobalFlux[4]
  )
{
  int i, j, row, col;
  double Qmatrix[4][5];

  /* define the transformation matrix */
  for (i = 0; i < 4; i++){
    for (j = 0; j < 5; j++){
      Qmatrix[i][j] = 0.0;
    }
  }
  Qmatrix[0][0] = Qmatrix[3][4] = 1.0;
  Qmatrix[1][1] = normalVec[faceId][0];
  Qmatrix[2][1] = normalVec[faceId][1];
  if (snorm > 1.0e-3){
    Qmatrix[1][2] = normalVec[faceId][1] / snorm;
    Qmatrix[1][3] = normalVec[faceId][0] * normalVec[faceId][2] / snorm;
    Qmatrix[2][2] = -normalVec[faceId][0] / snorm;
    Qmatrix[2][3] = normalVec[faceId][1] * normalVec[faceId][2] / snorm;
  }
  else {  /* e1 = et */
    Qmatrix[1][2] = 0.0;
    Qmatrix[1][3] = -snorm_x;
    Qmatrix[2][2] = normalVec[faceId][2] / snorm_x;
    Qmatrix[2][3] = normalVec[faceId][0] * normalVec[faceId][1] / snorm_x;
  }

  /* perform usual matrix multiplication */
  for (row = 0; row < 4; row++){
    vanLeerGlobalFlux[row] = 0.0;
    for (col = 0; col < 5; col++){
      vanLeerGlobalFlux[row] += Qmatrix[row][col] * vanLeerLocalFlux[col];
    }
  }

}


/**
 * Works out Van Leer flux at inner face.
 */
static void get_van_leer_flux(
  const int faceId,
  const double snorm,
  double **normalVec,
  const flow *faceL,
  const flow *faceR,
  double vanLeerFlux[4]
  )
{
  double fmass, sosL, sosR, machnL, machnR;
  double unL, unR, ut1L, ut1R, ut2L, ut2R, snorm_x;
  double localFlux[5];
  int i;

  /* work out the speed of sound at both sides */
  sosL = sound_speed(faceL[faceId].p, faceL[faceId].rho);
  sosR = sound_speed(faceR[faceId].p, faceR[faceId].rho);

  /* work out normal and tangential velocities */
  unL = faceL[faceId].u * normalVec[faceId][0] + faceL[faceId].v * normalVec[faceId][1];
  unR = faceR[faceId].u * normalVec[faceId][0] + faceR[faceId].v * normalVec[faceId][1];
  snorm_x = sqrt(pow(normalVec[faceId][1], 2.0) + pow(normalVec[faceId][2], 2.0));
  if (snorm <= 1.0e-3){  /* if e1 = et */
    ut1L = faceL[faceId].v*normalVec[faceId][2] / snorm_x;
    ut1R = faceR[faceId].v*normalVec[faceId][2] / snorm_x;
    ut2L = -faceL[faceId].u*snorm_x + faceL[faceId].v*normalVec[faceId][0] * normalVec[faceId][1] / snorm_x;
    ut2R = -faceR[faceId].u*snorm_x + faceR[faceId].v*normalVec[faceId][0] * normalVec[faceId][1] / snorm_x;
  }
  else {
    ut1L = (faceL[faceId].u * normalVec[faceId][1] - faceL[faceId].v * normalVec[faceId][0]) / snorm;
    ut1R = (faceR[faceId].u * normalVec[faceId][1] - faceR[faceId].v * normalVec[faceId][0]) / snorm;
    ut2L = normalVec[faceId][2] * unL / snorm;
    ut2R = normalVec[faceId][2] * unR / snorm;
  }

  /* work out normal Mach numbers */
  machnL = unL / sosL;
  machnR = unR / sosR;

  /* calculate fluxes in local coordinate system (normal and tangentials) */
  for (i = 0; i < 5; i++){
    localFlux[i] = 0.0;
  }
  if (machnL <= -1.0){
    localFlux[0] += faceL[faceId].rho * unL;
    localFlux[1] += faceL[faceId].rho * pow(unL, 2.0) + faceL[faceId].p;
    localFlux[2] += faceL[faceId].rho * unL*ut1L;
    localFlux[3] += faceL[faceId].rho * unL*ut2L;
    localFlux[4] += (faceL[faceId].rho * faceL[faceId].e + faceL[faceId].p)*unL;
  }
  else if (machnL < 1.0){
    fmass = -0.25*faceL[faceId].rho * sosL*pow(machnL - 1.0, 2.0);
    localFlux[0] += fmass;
    localFlux[1] += fmass*((GAMMA - 1.0)*unL - 2.0*sosL) / GAMMA;
    localFlux[2] += fmass*ut1L;
    localFlux[3] += fmass*ut2L;
    localFlux[4] += 0.5*fmass*((pow((GAMMA - 1.0)*unL - 2.0*sosL, 2.0) / (pow(GAMMA, 2.0) - 1.0)) + pow(ut1L, 2.0) + pow(ut2L, 2.0));
  }

  if (machnR >= 1.0){
    localFlux[0] += faceR[faceId].rho * unR;
    localFlux[1] += faceR[faceId].rho * pow(unR, 2.0) + faceR[faceId].p;
    localFlux[2] += faceR[faceId].rho * unR*ut1R;
    localFlux[3] += faceR[faceId].rho * unR*ut2R;
    localFlux[4] += (faceR[faceId].rho * faceR[faceId].e + faceR[faceId].p)*unR;
  }
  else if (machnR > -1.0){
    fmass = 0.25*faceR[faceId].rho * sosR*pow(machnR + 1.0, 2.0);
    localFlux[0] += fmass;
    localFlux[1] += fmass*((GAMMA - 1.0)*unR + 2.0*sosR) / GAMMA;
    localFlux[2] += fmass*ut1R;
    localFlux[3] += fmass*ut2R;
    localFlux[4] += 0.5*fmass*((pow((GAMMA - 1.0)*unR + 2.0*sosR, 2.0) / (pow(GAMMA, 2.0) - 1.0)) + pow(ut1R, 2.0) + pow(ut2R, 2.0));
  }

  local_to_global_csys(faceId, snorm_x, snorm, localFlux, normalVec, vanLeerFlux);

}


/**
 * Calculates inviscid fluxes at faces (through Van Leer).
 */
static void calculate_inviscid_face_fluxes_VanLeer(
  double **normalVec,
  const double *area,
  const double *faceFactor,
  const face_map *indx_f,
  const flow *faceL,
  const flow *faceR,
  double **invflux
  )
{
#pragma omp parallel for
  for (int i = indx_f->inner; i < indx_f->numberOfFaces; i++)
  {
    /* spatial fluxes (through Van Leer) */
    get_van_leer_flux(i, faceFactor[i], normalVec, faceL, faceR, invflux[i]);
    /* temporal fluxes */
    if (normalVec[i][2] > 0.0)
    {
      invflux[i][0] += faceR[i].rho * normalVec[i][2];
      invflux[i][1] += faceR[i].rho * faceR[i].u * normalVec[i][2];
      invflux[i][2] += faceR[i].rho * faceR[i].v * normalVec[i][2];
      invflux[i][3] += faceR[i].rho * faceR[i].e * normalVec[i][2];
    }
    else
    {
      invflux[i][0] += faceL[i].rho * normalVec[i][2];
      invflux[i][1] += faceL[i].rho * faceL[i].u * normalVec[i][2];
      invflux[i][2] += faceL[i].rho * faceL[i].v * normalVec[i][2];
      invflux[i][3] += faceL[i].rho * faceL[i].e * normalVec[i][2];
    }
  }

#pragma omp parallel for
  for (int i = indx_f->inner; i < indx_f->numberOfFaces; i++)
  {
    for (int j = 0; j < num_eqs; j++)
      invflux[i][j] *= area[i];
  }
}


/// Calculates Roe averages at face.
/**
 * @warning Caller is responsible to release memory of returned struct 'Roe_avg' after using it.
 * @param[in] faceL Array of structs 'flow' with left face values.
 * @param[in] faceR Array of structs 'flow' with right face values.
 * @return Struct 'Roe_avg' with Roe face values.
 */
static Roe_avg *get_Roe_averages(const flow *faceL, const flow *faceR)
{
  const double HL = faceL->e + faceL->p / faceL->rho;
  const double HR = faceR->e + faceR->p / faceR->rho;

  Roe_avg *Roe = (Roe_avg*)malloc(sizeof(Roe_avg));

  Roe->rho = sqrt(faceL->rho * faceR->rho);
  Roe->u = (sqrt(faceL->rho) * faceL->u + sqrt(faceR->rho) * faceR->u) / (sqrt(faceL->rho) + sqrt(faceR->rho));
  Roe->v = (sqrt(faceL->rho) * faceL->v + sqrt(faceR->rho) * faceR->v) / (sqrt(faceL->rho) + sqrt(faceR->rho));
  Roe->H = (sqrt(faceL->rho) * HL + sqrt(faceR->rho) * HR) / (sqrt(faceL->rho) + sqrt(faceR->rho));
  Roe->halfq = (pow(Roe->u, 2.0) + pow(Roe->v, 2.0)) / 2.0;
  Roe->a = sqrt((GAMMA - 1.0)*(Roe->H - Roe->halfq));

  return Roe;
}

/// Calculates Roe jumps at face.
/**
 * @warning Caller is responsible to release memory of returned struct 'flow' after using it.
 * @param[in] faceL Array of structs 'flow' with left face values.
 * @param[in] faceR Array of structs 'flow' with right face values.
 * @return Array of doubles with Roe jumps at the face.
 */
static double *get_Roe_jumps(const flow *faceL, const flow *faceR)
{
  double *du = (double*)malloc(sizeof(double)*4);

  du[0] = faceR->rho - faceL->rho;
  du[1] = faceR->rho*faceR->u - faceL->rho*faceL->u;
  du[2] = faceR->rho*faceR->v - faceL->rho*faceL->v;
  du[3] = faceR->rho*faceR->e - faceL->rho*faceL->e;

  return du;
}


/// Calculates alpha coefficients for Roe fluxes at face.
/**
 * @warning Caller is responsible to release memory of returned array after using it.
 * @param[in] faceL Struct 'flow' with left values at required face.
 * @param[in] faceR Struct 'flow' with right values at required face.
 * @param[in] Roe Struct 'flow' with Roe average values at required face.
 * @param[in] nx Component of the normal vector in the x-direction.
 * @param[in] ny Component of the normal vector in the y-direction.
 * @return Array of size 4 with the coefficients 'alpha' for Roe fluxes in normal direction at the face.
 */
static double *calculate_Roe_coeffs_n(const flow *faceL, const flow *faceR, const Roe_avg *Roe, const double nx,
  const double ny)
{
  double *du = (double*)get_Roe_jumps(faceL, faceR);
  const double gm2a2 = (GAMMA - 1.0) / (2.0*pow(Roe->a, 2.0));
  const double ut = Roe->u*ny - Roe->v*nx;
  const double un = Roe->u*nx + Roe->v*ny;

  double *coeffs = (double*)calloc(4, sizeof(double));

  coeffs[0] = 2.0*gm2a2*((Roe->H - 2.0*Roe->halfq)*du[0] + Roe->u*du[1] + Roe->v*du[2] - du[3]);
  coeffs[1] = -ut*du[0] + ny*du[1] - nx*du[2];
  coeffs[2] = gm2a2 * ((Roe->halfq*(1.0+un/Roe->a) - Roe->H*un/Roe->a)*du[0]
                      + (Roe->H*nx/Roe->a - Roe->halfq*nx/Roe->a - Roe->u)*du[1]
                      + (Roe->H*ny/Roe->a - Roe->halfq*ny/Roe->a - Roe->v)*du[2] + du[3]);
  coeffs[3] = gm2a2 * ((Roe->halfq*(1.0-un/Roe->a) + Roe->H*un/Roe->a)*du[0]
                      - (Roe->H*nx/Roe->a - Roe->halfq*nx/Roe->a + Roe->u)*du[1]
                      - (Roe->H*ny/Roe->a - Roe->halfq*ny/Roe->a + Roe->v)*du[2] + du[3]);

  free(du);

  return coeffs;
}



/// Calculates the jumps of the fluxes for Roe solver at face and subtracts them from flux.
/**
 * @warning Caller is responsible to release memory of returned array after using it.
 * @param[in] faceL Struct 'flow' with left values at required face.
 * @param[in] faceR Struct 'flow' with right values at required face.
 * @param[inout] result Array of double with Roe fluxes.
 */
static void add_Roe_flux_jumps_n(const flow *faceL, const flow *faceR, const Roe_avg *Roe, const double nx,
  const double ny, double *result)
{
  double *coeffs = (double*)calculate_Roe_coeffs_n(faceL, faceR, Roe, nx, ny);

  const double ut = Roe->u*ny - Roe->v*nx;
  const double un = Roe->u*nx + Roe->v*ny;
  double eig[4] = {fabs(un), fabs(un), fabs(un + Roe->a), fabs(un - Roe->a)};
//  double eig[4] = {un, un, un + Roe->a, un - Roe->a};
  const double maxEig = max(fabs(un + Roe->a), fabs(un - Roe->a));
  const double fractionOfMaxEigenvalueForEntropyFix = 0.1; // hard-coded for now
  const double limit = fractionOfMaxEigenvalueForEntropyFix*maxEig;

  const double eigvec[16] = {1.0,              Roe->u,              Roe->v,          Roe->halfq,
                             0.0,                  ny,                 -nx,                  ut,
                             1.0,  Roe->u + Roe->a*nx,  Roe->v + Roe->a*ny,  Roe->H + Roe->a*un,
                             1.0,  Roe->u - Roe->a*nx,  Roe->v - Roe->a*ny,  Roe->H - Roe->a*un};

  // apply second Harten-Hyman entropy fix for Roe flux
  for (int i = 0; i < 4; i++)
  {
    if (eig[i] < limit)
      eig[i] = 0.5*(eig[i]*eig[i]/limit + limit);
  }

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
      result[i] += coeffs[j]*eig[j]*eigvec[4*j+i];
  }

  free(coeffs);
}


static void add_centered_flux_via_Roe_matrix_n(const Roe_avg *Roe, const flow *state, const double nx,
  const double ny, double *result)
{
  const double u1 = state->rho;
  const double u2 = state->rho*state->u;
  const double u3 = state->rho*state->v;
  const double u4 = state->rho*state->e;
  const double un = Roe->u*nx + Roe->v*ny;
  const double gm = GAMMA - 1.0;

  result[0] += nx*u2 + ny*u3;
  result[1] += ((gm*Roe->halfq - pow(Roe->u, 2.0))*nx - Roe->u*Roe->v*ny)*u1 + ((3.0-GAMMA)*Roe->u*nx + Roe->v*ny)*u2
             + (Roe->u*ny - gm*Roe->v*nx)*u3 + gm*nx*u4;
  result[2] += ((gm*Roe->halfq - pow(Roe->v, 2.0))*ny - Roe->u*Roe->v*nx)*u1 + (Roe->v*nx - gm*Roe->u*ny)*u2
             + ((3.0-GAMMA)*Roe->v*ny + Roe->u*nx)*u3 + gm*ny*u4;
  result[3] += (gm*Roe->halfq - Roe->H)*un*u1 + (Roe->H*nx - gm*Roe->u*un)*u2 + (Roe->H*ny - gm*Roe->v*un)*u3
             + GAMMA*un*u4;
}


static void get_Roe_flux(const flow *faceL, const flow *faceR, const double *normalVec, double *invflux)
{
  Roe_avg *Roe = (Roe_avg*)get_Roe_averages(faceL, faceR);

  for (int i = 0; i < 4; i++)
    invflux[i] = 0.0;

  add_centered_flux_via_Roe_matrix_n(Roe, faceL, normalVec[0], normalVec[1], invflux);
  add_centered_flux_via_Roe_matrix_n(Roe, faceR, normalVec[0], normalVec[1], invflux);
  add_Roe_flux_jumps_n(faceL, faceR, Roe, normalVec[0], normalVec[1], invflux);

  for (int i = 0; i < 4; i++)
    invflux[i] *= 0.5;

  free(Roe);
}


/**
 * Calculates inviscid fluxes at faces (through Van Leer).
 */
static void calculate_inviscid_face_fluxes_Roe(
  double **normalVec,
  const double *area,
  const face_map *indx_f,
  const flow *faceL,
  const flow *faceR,
  double **invflux
  )
{
#pragma omp parallel for
  for (int i = indx_f->inner; i < indx_f->numberOfFaces; i++)
  {
    /* spatial fluxes (through Roe) */
    get_Roe_flux(&faceL[i], &faceR[i], normalVec[i], invflux[i]);
    /* temporal fluxes */
    if (normalVec[i][2] > 0.0)
    {
      invflux[i][0] += faceR[i].rho * normalVec[i][2];
      invflux[i][1] += faceR[i].rho * faceR[i].u * normalVec[i][2];
      invflux[i][2] += faceR[i].rho * faceR[i].v * normalVec[i][2];
      invflux[i][3] += faceR[i].rho * faceR[i].e * normalVec[i][2];
    }
    else
    {
      invflux[i][0] += faceL[i].rho * normalVec[i][2];
      invflux[i][1] += faceL[i].rho * faceL[i].u * normalVec[i][2];
      invflux[i][2] += faceL[i].rho * faceL[i].v * normalVec[i][2];
      invflux[i][3] += faceL[i].rho * faceL[i].e * normalVec[i][2];
    }
  }

#pragma omp parallel for
  for (int i = indx_f->inner; i < indx_f->numberOfFaces; i++)
  {
    for (int j = 0; j < num_eqs; j++)
      invflux[i][j] *= area[i];
  }
}



/**
 * Updates the solution one time-step in pseudo-time for RANS eqs. of motion.
 */
double perform_update_RANS_uw(
  const int option,
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double totalVolume,
  const double Tref,
  const double Reinf,
  const double Prinf,
  const double CFL,
  const double *Psi,
  const double *area,
  const double *volume,
  const double *wallDist,
  const double *faceFactor,
  double **normalVec,
  double **solidFaceVel,
  double **W,
  double ***connecVec,
  flow *cellFlow,
  const flow *freeFlow,
  const turb *saConst
  )
{
  int i, j, stage;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  const int totalNumCells = numberOfCells + indx_f->numberOfFaces - indx_f->initial;
  double residual, sosinf, Minf;
  double **w0, **invflux, **viscflux, **RKflux, **RKRes, *SAflux, *SARes;
  double *rho0, *mubar0, *specRad, *specRadFace, *dt, *lap_nub;
  double **grad_rho, **grad_u, **grad_v, **grad_p, **grad_T, **grad_nub;
  double *tauxx, *tauxy, *tauyy, *qx, *qy;
  double *production, *destruction, *diffusion_v, *diffusion_s, *convection;
  flow *faceL, *faceR;

  sosinf = sound_speed(freeFlow->p, freeFlow->rho);
  Minf = sqrt(pow(freeFlow->u, 2.0) + pow(freeFlow->v, 2.0)) / sosinf;

  allocate_memory_RANS(numberOfCells, indx_f, &rho0, &dt, &specRad, &specRadFace, &mubar0,
      &tauxx, &tauxy, &tauyy, &qx, &qy, &production, &destruction, &diffusion_s, &diffusion_v, &convection,
      &SAflux, &w0, &invflux, &viscflux, &faceL, &faceR);

#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      rho0[i] = cellFlow[i].rho;
      mubar0[i] = cellFlow[i].mubar;
      for (j = 0; j < num_eqs; j++){
        w0[i][j] = W[i][j];
      }
    }
  }

  RKRes = allocate_memory_RKRes(numberOfCells);
  SARes = allocate_memory_SARes(numberOfCells);

/* RUNGE-KUTTA */
  for (stage = 0; stage < 4; stage++){
    calculate_gradients_Euler(numberOfCells, indx_f, neighbourCell, normalVec, area, volume,
        cellFlow, &grad_rho, &grad_u, &grad_v, &grad_p);

    reconstruct_inner_face_values_RANS(numberOfCells, indx_f, neighbourCell, connecVec, cellFlow,
        grad_rho, grad_u, grad_v, grad_p, faceL, faceR);
    reconstruct_boundary_face_values_RANS(indx_f, neighbourCell, cellFlow, faceL, faceR);

    construct_augmented_state_RANS(indx_f->numberOfFaces, Minf, Tref, saConst, faceL);
    construct_augmented_state_RANS(indx_f->numberOfFaces, Minf, Tref, saConst, faceR);

    if (option == VAN_LEER)
    {
      calculate_inviscid_face_fluxes_VanLeer(normalVec, area, faceFactor, indx_f, faceL, faceR, invflux);
    }
    else if (option == ROE)
    {
      calculate_inviscid_face_fluxes_Roe(normalVec, area, indx_f, faceL, faceR, invflux);
    }
    else
    {
      error("wrong type of Riemann solver chosen");
    }

    if (stage == 0)
    {
      calculate_gradient_T(numberOfCells, indx_f, neighbourCell, normalVec, area, volume, cellFlow, &grad_T);
      calculate_viscous_face_fluxes(indx_f, neighbourCell, connecVec, normalVec,
        grad_u, grad_v, grad_T, area, Reinf, Prinf, Minf, cellFlow, saConst, viscflux);
    }

    RKflux = calculate_runge_kutta_residuals(numberOfCells, indx_f, neighbourCell, 2, invflux, viscflux);

    if (stage == 0)
    {
      calculate_spectral_radii(numberOfCells, indx_f, neighbourCell, area, normalVec,
          cellFlow, specRad, specRadFace);
      calculate_timestep_RANS(numberOfCells, neighbourCell, CFL, Reinf, Prinf, Psi, volume, area,
          specRad, indx_f, cellFlow, saConst, dt);

      calculate_undivided_laplacians_nub(numberOfCells, indx_f, neighbourCell, cellFlow, &lap_nub);

      calculate_gradient_nub(numberOfCells, indx_f, neighbourCell, normalVec, area, volume,
          cellFlow, &grad_nub);
      calculate_volume_contrib_SA(numberOfCells, Reinf, volume, wallDist, lap_nub, grad_rho, grad_u, grad_v,
          grad_nub, cellFlow, saConst, production, destruction, diffusion_v);
      calculate_surface_contrib_SA(indx_f, neighbourCell, normalVec, area, grad_nub, Reinf, cellFlow, saConst, diffusion_s);
      free(lap_nub);
    }

    calculate_convective_SA(indx_f, neighbourCell, normalVec, area, cellFlow, convection);

    calculate_SA_residuals(numberOfCells, indx_f, neighbourCell, convection, production, destruction, diffusion_v, diffusion_s, SAflux);

    if (stage == 0)
      free_partial_memory_RANS(numberOfCells, grad_T, grad_nub);

    if (stage != 3)
    {
#pragma omp parallel
      {
#pragma omp for private(j)
        for (i = 0; i < numberOfCells; i++)
        {
          cellFlow[i].mubar = mubar0[i] - (dt[i] * SAflux[i] / (RK_coeff[stage + 1] * volume[i]));
          if (cellFlow[i].mubar <= 0.0)
          {
            cellFlow[i].mubar = 1.0e-6;
          }
          for (j = 0; j < num_eqs; j++)
          {
            W[i][j] = w0[i][j] - (dt[i] * RKflux[i][j] / (RK_coeff[stage + 1] * volume[i]));
          }
        }
      }
    }

    add_stage_RKRes_contribution_to_update(numberOfCells, RK_coeff[stage], RKflux, RKRes);
    add_stage_SARes_contribution_to_update(numberOfCells, RK_coeff[stage], SAflux, SARes);

    if (stage != 3)
    {
      calculate_primitive_vars(numberOfCells, W, cellFlow);
      calculate_halo_cells_RANS(sosinf, Minf, normalVec, solidFaceVel, neighbourCell, indx_f, freeFlow, saConst,
        cellFlow);
      construct_augmented_state_RANS(totalNumCells, Minf, Tref, saConst, cellFlow);
    }
    free_partial_memory_Euler(numberOfCells, RKflux, grad_rho, grad_u, grad_v, grad_p);
  }

  // update solution at next timestep
#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      cellFlow[i].mubar = mubar0[i] - (dt[i] * SARes[i] / volume[i]);
      if (cellFlow[i].mubar <= 0.0){
        cellFlow[i].mubar = 1.0e-6;
      }
      for (j = 0; j < num_eqs; j++){
        W[i][j] = w0[i][j] - (dt[i] * RKRes[i][j] / volume[i]);
      }
    }
  }
  free_memory_RKRes(numberOfCells, RKRes);
  free_memory_SARes(SARes);

  calculate_primitive_vars(numberOfCells, W, cellFlow);
  calculate_halo_cells_RANS(sosinf, Minf, normalVec, solidFaceVel, neighbourCell, indx_f, freeFlow, saConst,
    cellFlow);
  construct_augmented_state_RANS(totalNumCells, Minf, Tref, saConst, cellFlow);

  residual = calculate_convergence_residual(numberOfCells, totalVolume, volume, dt, rho0, cellFlow);

  free_memory_RANS(numberOfCells, numberOfFaces, indx_f, rho0, dt, specRad, specRadFace,
      mubar0, tauxx, tauxy, tauyy, qx, qy, production, destruction, diffusion_s,
      diffusion_v, convection, SAflux, w0, invflux, viscflux, faceL, faceR);

  return residual;
}

/**
 * Updates the solution one time-step in pseudo-time for Euler eqs. of motion.
 */
double perform_update_Euler_uw(
  const int option,
  const int numberOfCells,
  int **neighbourCell,
  const face_map *indx_f,
  const double totalVolume,
  const double CFL,
  const double *area,
  const double *volume,
  const double *faceFactor,
  double **normalVec,
  double **W,
  double ***connecVec,
  flow *cellFlow,
  const flow *freeFlow
  )
{
  int i, j, stage;
  const int numberOfFaces = indx_f->numberOfFaces - indx_f->inner;
  const int totalNumCells = numberOfCells + indx_f->numberOfFaces - indx_f->initial;
  double residual, sosinf, Minf;
  double **w0, **invflux, **RKflux, **RKRes;
  double *rho0, *specRad, *specRadFace, *dt;
  double **grad_rho, **grad_u, **grad_v, **grad_p;
  flow *faceL, *faceR;

  sosinf = sound_speed(freeFlow->p, freeFlow->rho);
  Minf = sqrt(pow(freeFlow->u, 2.0) + pow(freeFlow->v, 2.0)) / sosinf;

  allocate_memory_Euler(numberOfCells, indx_f, &rho0, &dt, &specRad, &specRadFace,
      &invflux, &w0, &faceL, &faceR);

#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      rho0[i] = cellFlow[i].rho;
      for (j = 0; j < num_eqs; j++){
        w0[i][j] = W[i][j];
      }
    }
  }

  RKRes = allocate_memory_RKRes(numberOfCells);

/* RUNGE-KUTTA */
  for (stage = 0; stage < 4; stage++){
    calculate_gradients_Euler(numberOfCells, indx_f, neighbourCell, normalVec, area, volume, cellFlow,
            &grad_rho, &grad_u, &grad_v, &grad_p);

    reconstruct_inner_face_values_Euler(numberOfCells, indx_f, neighbourCell, connecVec, cellFlow,
        grad_rho, grad_u, grad_v, grad_p, faceL, faceR);
    reconstruct_boundary_face_values_Euler(indx_f, neighbourCell, cellFlow, faceL, faceR);
    construct_augmented_state_Euler(indx_f->numberOfFaces, faceL);
    construct_augmented_state_Euler(indx_f->numberOfFaces, faceR);

    if (option == VAN_LEER)
    {
      calculate_inviscid_face_fluxes_VanLeer(normalVec, area, faceFactor, indx_f, faceL, faceR, invflux);
    }
    else if (option == ROE)
    {
      calculate_inviscid_face_fluxes_Roe(normalVec, area, indx_f, faceL, faceR, invflux);
    }
    else
    {
      error("wrong type of Riemann solver chosen");
    }

    RKflux = calculate_runge_kutta_residuals(numberOfCells, indx_f, neighbourCell, 1, invflux);

    if (stage == 0)
    {
      calculate_spectral_radii(numberOfCells, indx_f, neighbourCell, area, normalVec,
          cellFlow, specRad, specRadFace);
      calculate_timestep_Euler(numberOfCells, neighbourCell, CFL, volume, specRad, indx_f, dt);
    }

    if (stage != 3)
    {
#pragma omp parallel
      {
#pragma omp for private(j)
        for (i = 0; i < numberOfCells; i++)
        {
          for (j = 0; j < num_eqs; j++)
          {
            W[i][j] = w0[i][j] - (dt[i] * RKflux[i][j] / (RK_coeff[stage + 1] * volume[i]));
          }
        }
      }
    }

    add_stage_RKRes_contribution_to_update(numberOfCells, RK_coeff[stage], RKflux, RKRes);

    if (stage != 3)
    {
      calculate_primitive_vars(numberOfCells, W, cellFlow);
      calculate_halo_cells_Euler(sosinf, Minf, normalVec, neighbourCell, indx_f, freeFlow, cellFlow);
      construct_augmented_state_Euler(totalNumCells, cellFlow);
    }
    free_partial_memory_Euler(numberOfCells, RKflux, grad_rho, grad_u, grad_v, grad_p);
  }

  // update solution at next timestep
#pragma omp parallel
  {
#pragma omp for private(j)
    for (i = 0; i < numberOfCells; i++){
      for (j = 0; j < num_eqs; j++){
        W[i][j] = w0[i][j] - (dt[i] * RKRes[i][j] / volume[i]);
      }
    }
  }
  free_memory_RKRes(numberOfCells, RKRes);

  calculate_primitive_vars(numberOfCells, W, cellFlow);
  calculate_halo_cells_Euler(sosinf, Minf, normalVec, neighbourCell, indx_f, freeFlow, cellFlow);
  construct_augmented_state_Euler(totalNumCells, cellFlow);

  residual = calculate_convergence_residual(numberOfCells, totalVolume, volume, dt, rho0, cellFlow);

  free_memory_Euler(numberOfCells, numberOfFaces, rho0, dt, specRad, specRadFace,
      w0, invflux, faceL, faceR);

  return residual;
}
