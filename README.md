# SpacetimeCFD

## Overview
This repository is the result of a Ph.D. thesis at the University of Bristol (UK). It constitutes a computational fluid dynamics (CFD) solver and it is written in C. Due to the limited time and scope of this project (~ 3 years) it has not been thoroughly test. Hence, **there is no guarantee that it will produce accurate solutions for all use cases**. **Use at your own risk!**

For more information read [this](https://research-information.bris.ac.uk/files/190246091/ImanolFlamariqueEderra_PhD_thesis.pdf).

## Requirements
To compile and use the code you need:

- C compiler which supports standard C99
- OpenMP
- GNU Make
- Git

## Download
To download the code:

```bash
git clone https://bitbucket.org/flamarique/spacetime.git
```

## Compile
If your system meets all the requirements outlined above:

```bash
cd spacetime && make
```

## Usage
The solver can be launched from the bash terminal as follows:

```bash
./SpacetimeCFD <stencil> <physical_model> [option(s)] <mesh> [-o <result>]
```

where `<stencil>` is given by **one** of the following:

|`<stencil>`| Description|
|:---------:|:----------:|
| `JST`     | *central-difference* scheme which uses JST dissipation model |
| `UW_VL`   | *upwind* scheme which uses Van Leer's flux-vector splitting |
| `UW_Roe`  | *upwind* scheme which uses Roe's dissipation matrix |


and `<physical_model>` is given by **one** of the following:

|`<physical_model>`| Description|
|:----------------:|:----------:|
| `Euler`          | Euler equations of fluids motion for *inviscid* compressible flows|
| `RANS`           | RANS equations of motion for *viscous* compressible flows|


The available options are:

| Option | Value required? | Type of value | Default value | Description|
|:------:|:---------------:|:-------------:|:-------------:|:----------:|
| `-a` | Yes | `float` | 0 | angle of attack (in deg) |
| `-B` | No | - | - | do **not** solve initial *spatial* problem (an initial solution must be provided) |
| `-CFL` | Yes | `float` | 2 | *Courant-Friedrichs-Lewy* condition for the pseudo-time step size calculation (must be CFL < 2.5 for stability) |
| `-d` | Yes | `float` | 1.225 | density of the freestream flow (in kg/m3) |
| `-dim` | No | - | - | use dimensional values for quantities (e.g. density, pressure, length, time, etc.) |
| `-i0` | Yes | `int` | 50000 | maximum number of iterations in pseudo-time for the initial solution |
| `-i` | Yes | `int` | 100000 | maximum number of iterations in pseudo-time for the spacetime solution |
| `-k2` | Yes | `float` | 1 | value of constant *k2* of the JST dissipation model |
| `-k4` | Yes | `float` | 0.05 | value of constant *k4* in the JST dissipation model |
| `-kt` | Yes | `float` | 1 | stretching of the grid in the time direction (note that *time* goes along the *z*-axis) |
| `-L` | Yes | `float` | 1 | characteristic length for viscosity calculation (in m) |
| `-M` | Yes | `float` | 0.5 | Mach number of the freestream flow |
| `-p` | Yes | `float` | 101325 | pressure of the freestream flow (in Pa) |
| `-P` | No | - | - | solve a *periodic* problem (initial and final planes must be *identical*) |
| `-Pr` | Yes | `float` | 0.72 | Prandtl number of the freestream flow |
| `-PrT` | Yes | `float` | 0.9 | turbulent Prandtl number of the freestream flow |
| `-Re` | Yes | `float` | 1.0e+6 | Reynolds number of the freestream flow |
| `-res0` | Yes | `float` | -5 | exponent for criterion of convergence based on density residuals for initial solution (e.g. use `-3` for 1.0e-3) |
| `-res` | Yes | `float` | -5 | exponent for criterion of convergence based on density residuals for spacetime solution (e.g. use `-3` for 1.0e-3) |
| `-seed0` | Yes | `string` | - | seeding file containing the initial solution to be used; alternatively use directly the strings `inviscid_vortex` or `sod_problem` for the initialization of any of these well-known problems |
| `-seed` | Yes | `string` | - | seeding file containing the spacetime solution from which a simulation may be re-started |
| `-S` | Yes | `int` | 60 | frequency used to save the solution (in minutes) |
| `-h` | No | - | - | print help menu


The input `<mesh>` must comply with the format described in the following section. Finally, `<output>` is a string used to name the solution and logging files that the solver yields.

### Examples
```bash
./SpacetimeCFD JST Euler -a 3 -B -P -res -8 naca0012 -o pitching_naca0012
```
```bash
./SpacetimeCFD UW_VL RANS -a 1.3 -res0 -7 -res -6 -M 0.3 -Re 5.5e+6 viscous_mesh -o viscous_case
```
```bash
./SpacetimeCFD UW_Roe Euler -a 2.5 -dim -seed0 spoiler.0.seed -res -3 -M 0.15 spoiler_mesh -o spoiler_deployment
```

## Mesh format
The input mesh to the spacetime solver is an ASCII file which contains a *face-based* description (i.e. only information regarding points and faces is provided) of the spacetime finite-volume mesh. The format is as follows:

```
numPoints    numFaces
x_1    y_1    t_1
x_2    y_2    t_2
...    ...    ...
...    ...    ...
x_P    y_P    t_P
N_1    v1_1    v2_1    ...    vN_1_1    leftCell_1    rightCell_1    boundMarker_1
N_2    v1_2    v2_2    ...    vN_2_2    leftCell_2    rightCell_2    boundMarker_2
...    ...     ...     ...    ...       ...           ...            ...
...    ...     ...     ...    ...       ...           ...            ...
N_F    v1_F    v2_F    ...    vN_F_F    leftCell_F    rightCell_F    boundMarker_F
```

where

- `numPoints` is the total number of points in the spacetime mesh (also referred to as `P` in nodal data subscripts)
- `numFaces` are the total number of faces in the spacetime mesh (also referred to as `F` in face data subscripts)
- `x_j`, `y_j` and `t_j` are the spacetime coordinates of point `j`
- `N_i` is the number of verices in face `i`
- `v_1_i`, `v_2_i`, ..., `v_N_i_i` are the `N_i` vertex indices of face `i`; they must be a sequence of *contiguous* vertices
- `leftCell_i` and `rightCell_i` are the left and right neighbouring cell indices at face `i`
- `boundMarker_i` is the boundary marker at face `i`

Indices denoting points/vertices and (neighbour) cells start with 0. Boundary markers are used to define the face type and can be any of the following:

| Type of face | Boundary marker(s) |
|:------------:|:------------------:|
| inner (or fluid) face | `0` |
| initial/final boundary | `1` |
| solid boundary | `2, 200, 201, 202, ..., 299` |
| farfield boundary | `3` |


If a face is part of the fluid domain boundary, i.e. it is not an inner/fluid face, then one
of its neighbouring cells (`leftCell` or `rightCell`) will be assigned index `-1`. In other words, if no
cell exists to the left of a certain face then `leftCell = -1`. And in case no cell exists to the right of a
certain face then `rightCell = -1`.

The solver accepts any of the following [NASTRAN cell types](https://docs.plm.automation.siemens.com/data_services/resources/nxnastran/10/help/en_US/tdocExt/pdf/element.pdf): CTETRA (tetrahedron), CPENTA (wedge/prism formed by two triangles and four quadrilaterals), CPYRAM (pyramid with a quadrilateral base and four triangles as sides) and CHEXA (hexahedron). In regard to [face types](https://docs.plm.automation.siemens.com/data_services/resources/nxnastran/10/help/en_US/tdocExt/pdf/element.pdf) it accepts any of the
following: CTRIA3 (triangle defined by three points) and CQUAD4 (quadrilateral defined by a sequence of four *contiguous* points).

### Mesh for RANS simulations
In the case of RANS simulations, information regarding the velocity of grid nodes which belong to solid boundaries must be provided to the solver along with the mesh file described above. These grid velocities must be provided in a separate ASCII file with the following format:

```
node_1    Vx_1    Vy_1
node_2    Vx_2    Vy_2
...       ...     ...
...       ...     ...
node_SP   Vx_SP   Vy_SP
```

where:

- `node_j` is the index of a grid point which belongs to a solid boundary
- `Vx_j` and `Vy_j` are the components of node `node_j` velocities in the *x* and *y* directions, respectively
- `SP` is a subscript representing the total number of *solid* points, i.e. points which belong to a solid boundary

The name of this file containing *solid* points velocities must be `<mesh_filename>.vel`, where `<mesh_filename>` is the mesh filename including the extension (if any). For example, if the command used to launch the simulation is

```bash
./SpacetimeCFD UW_VL RANS -a 1.3 -res0 -7 -res -6 -M 0.3 -Re 5.5e+6 viscous_mesh -o viscous_case
```

then there should be at least two files in the directory from which the simulation is launched:

- `viscous_mesh`: this is the mesh file containing a *face-based* representation
- `viscous_mesh.vel`: this is the file containing grid points velocities for all points which belong to a *solid* boundary
